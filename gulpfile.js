var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */
require('laravel-elixir-angular');

//elixir(function(mix) {
//    mix.less('app.less');
//});


elixir(function(mix) {
 mix.angular("resources/assets/angular/", "public/assets/scripts/plugins/angular/", "application.js");
});
