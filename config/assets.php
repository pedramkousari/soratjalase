<?php

/*
|---------------------------------------------------------------------------
| Here are SOME of the available configuration options with suitable values.
| Uncomment and customize those you want to override or remove them to
| use their default values. For a FULL list of options please visit
| https://github.com/Stolz/Assets/blob/master/API.md#assets
|---------------------------------------------------------------------------
*/

return [

    /**
     * Regex to match against a filename/url to determine if it is an asset.
     *
     * @var string
     */
    'asset_regex' => '/.\.(css|js)$/i',

    /**
     * Regex to match against a filename/url to determine if it is a CSS asset.
     *
     * @var string
     */
    'css_regex' => '/.\.css$/i',

    /**
     * Regex to match against a filename/url to determine if it is a JavaScript asset.
     *
     * @var string
     */
    'js_regex' => '/.\.js$/i',

    /**
     * Absolute path to the public directory of your App (WEBROOT).
     * Required if you enable the pipeline.
     * No trailing slash!.
     *
     * @var string
     */
    'public_dir' => (function_exists('public_path')) ? public_path() : '/var/www/localhost/htdocs',

    /**
     * Directory for local CSS assets.
     * Relative to your public directory ('public_dir').
     * No trailing slash!.
     *
     * @var string
     */
    'css_dir' => '/assets',

    /**
     * Directory for local JavaScript assets.
     * Relative to your public directory ('public_dir').
     * No trailing slash!.
     *
     * @var string
     */
    'js_dir' => '/assets',

    /**
     * Directory for local package assets.
     * Relative to your public directory ('public_dir').
     * No trailing slash!.
     *
     * @var string
     */
    //'packages_dir' => 'packages',

    /**
     * Enable assets pipeline (concatenation and minification).
     * If you set an integer value greather than 1 it will be used
     * as a timestamp that will be added to the pipelined assets name.
     *
     * @var bool|integer
     */
    'pipeline' => env('ASSETS_COMPRESS'),

    /**
     * Directory for storing pipelined assets.
     * Relative to your assets directories ('css_dir' and 'js_dir').
     * No trailing slash!.
     *
     * @var string
     */
    'pipeline_dir' => 'cache',

    /**
     * Enable pipelined assets compression with Gzip.
     * Use only if your webserver supports Gzip HTTP_ACCEPT_ENCODING.
     * Set to true to use the default compression level.
     * Set an integer between 0 (no compression) and 9 (maximum compression) to choose compression level.
     *
     * @var bool|integer
     */
    'pipeline_gzip' => 9,

    /**
     * Closure used by the pipeline to fetch assets.
     *
     * Useful when file_get_contents() function is not available in your PHP
     * instalation or when you want to apply any kind of preprocessing to
     * your assets before they get pipelined.
     *
     * The closure will receive as the only parameter a string with the path/URL of the asset and
     * it should return the content of the asset file as a string.
     *
     * @var Closure
     */
    // 'fetch_command' => function ($asset) {return preprocess(file_get_contents($asset));},

    /**
     * Available collections.
     * Each collection is an array of assets.
     * Collections may also contain other collections.
     *
     * @var array
     */
    'collections' => [
        'fontface' => [
            'plugins/font-awesome/css/font-awesome.min.css',
        ],

        'core-css' => [
            'plugins/bootstrap/css/bootstrap.min.css',
            'plugins/uniform/css/uniform.default.css',
            'plugins/bootstrap-switch/css/bootstrap-switch.min.css',
            'plugins/bootstrap-fileinput/bootstrap-fileinput.css',
            'plugins/jquery-ui/jquery-ui-1.10.3.custom.min.css',                                // jquery ui
        ],

        'core-css-rtl' => [
            'plugins/bootstrap/css/bootstrap-rtl.min.css',
            'plugins/uniform/css/uniform.default.css',
            'plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css',
            'plugins/bootstrap-fileinput/bootstrap-fileinput.css',
            'plugins/jquery-ui/jquery-ui-1.10.3.custom.min.css',                                // jquery ui
        ],

        'plugin-css' => [
            'plugins/select2/select2.css',                                                       // select 2
            'plugins/select2/select2-metronic.css',                                              // select 2 metronic
            'plugins/data-tables/DT_bootstrap.css',                                              // data-table
            'plugins/fancybox/source/jquery.fancybox.css'                                        // fancybox
        ],

        'plugin-css-rtl' => [
            'plugins/select2/select2-rtl.css',                                                   // select 2
            'plugins/select2/select2-metronic-rtl.css',                                          // select 2 metronic
            'plugins/data-tables/DT_bootstrap.css',                                              // data-table
            'plugins/fancybox/source/jquery.fancybox.css'                                        // fancybox
        ],

        'theme-css' => [
            'plugins/metronic/css/style-metronic.css',
            'plugins/metronic/css/style.css',
            'plugins/metronic/css/style-responsive.css',
            'plugins/metronic/css/plugins.css',
            'plugins/metronic/css/pages/error.css',
        ],

        'theme-css-rtl' => [
            'plugins/metronic/css/style-metronic-rtl.css',
            'plugins/metronic/css/style-rtl.css',
            'plugins/metronic/css/style-responsive-rtl.css',
            'plugins/metronic/css/plugins-rtl.css',
            'plugins/metronic/css/pages/error.css',
        ],

        'plugins-admin-css' => [
            'plugins/bootstrap-toastr/toastr.min.css',                                           // toaster
            'plugins/jquery-notific8/jquery.notific8.min.css',                                   // notification
            'plugins/bootstrap-toastr/toastr-rtl.min.css',                                       // toaster
            'plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css',                          // modal bt3
            'plugins/bootstrap-modal/css/bootstrap-modal.css',                                   // modal
            'plugins/bootstrap-multiselect-master/dist/css/bootstrap-multiselect.css',           // multi select
            'css/custom.css',
        ],

        'plugins-admin-rtl-css' => [
            'css/custom.css',
            'css/custom-rtl.css'
        ],

        'index-css' => [
            'plugins/ResponsiveSlides.js-master/responsiveslides.css',
            'plugins/owl-carousel/owl.carousel.css',
            'plugins/owl-carousel/owl.theme.css',
            'plugins/owl-carousel/owl.transitions.css',
            'css/index.css',
            'css/index/app.css',
        ],

        'customer-css' => [
            'css/index.css',
            'css/index/app.css',
            'css/customer/app.css',
            'plugins/pwt.datepicker-0.4.5/dist/css/persian-datepicker-0.4.5.min.css',
//            'plugins/pwt.datepicker-0.4.5/dist/css/theme/persian-datepicker-blue.css',
//            'plugins/pwt.datepicker-0.4.5/dist/css/theme/persian-datepicker-cheerup.css',
//            'plugins/pwt.datepicker-0.4.5/dist/css/theme/persian-datepicker-dark.css',
//            'plugins/pwt.datepicker-0.4.5/dist/css/theme/persian-datepicker-redblack.css',
        ],

        'index-rtl-css' => [
        ],

        'core-js' => [
            'plugins/jquery-1.11.0.js',
            'plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js',
            'plugins/jquery-migrate-1.2.1.min.js',
            'plugins/bootstrap/js/bootstrap.min.js',
            'plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
            'plugins/jquery-slimscroll/jquery.slimscroll.min.js',
            'plugins/jquery.blockui.min.js',
            'plugins/jquery.cokie.min.js',
            'plugins/uniform/jquery.uniform.min.js',
            'plugins/bootstrap-fileinput/bootstrap-fileinput.js',
            'plugins/bootstrap-switch/js/bootstrap-switch.min.js'
        ],
        'core-js-rtl' => [
            'plugins/bootstrap-switch/js/bootstrap-switch-rtl.min.js'
        ],

        'plugin-js' => [
            'plugins/lodash-2-4-1.js',
            'plugins/jquery.shiftcheckbox.js',                                              // jquery shift checkbox
            'plugins/select2/select2.min.js',                                               // select 2
            'plugins/bootbox/bootbox.min.js',                                               // bootbox
            'plugins/jquery-validation/dist/jquery.validate.min.js',                        // jquery validation
            'plugins/jquery-validation/dist/additional-methods.min.js',                     // jquery validation additional methods
            'plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
            'plugins/jquery-knob/js/jquery.knob.js',
            'plugins/fancybox/source/jquery.fancybox.pack.js'
        ],

        'plugin-admin-js' => [
            'plugins/jquery-slimscroll/jquery.slimscroll.min.js',
            'plugins/jquery.blockui.min.js',
            'plugins/jquery.cokie.min.js',
            'plugins/uniform/jquery.uniform.min.js',
            'plugins/bootstrap-modal/js/bootstrap-modalmanager.js',                         // modal manager
            'plugins/bootstrap-modal/js/bootstrap-modal.js',                                // modal
            'plugins/bootstrap-toastr/toastr.min.js',                                       // toaster
            'plugins/jquery-notific8/jquery.notific8.min.js',                               // notification
            'scripts/custom/ui-extended-modals.js',                                         // extended modal
        ],

        'plugin-index-js' => [
            'plugins/angular/angular.js',
            'plugins/angular/angular-route.min.js',
        ],

        'app-js' => [
            'scripts/core/app.js',
            'scripts/custom/trace.js',
            'scripts/custom/index.js',
            'scripts/custom/custom.js',
        ],

        'index-js' => [
            'plugins/ResponsiveSlides.js-master/responsiveslides.js',
            'plugins/owl-carousel/owl.carousel.min.js',
            'scripts/core/app.js',
            'scripts/custom/trace.js',
            'scripts/custom/index.js',
            'scripts/index/app.js',
            'plugins/jquery-validation/dist/messages_fa.js',
//            'plugins/bootstrap-toastr/toastr.min.js',
        ],

        'customer-js' => [
            'scripts/core/app.js',
            'scripts/custom/trace.js',
            'scripts/custom/index.js',
            'scripts/customer/app.js',
            'scripts/custom/custom.js',
            'plugins/jquery-validation/dist/messages_fa.js',
            'plugins/pwt.datepicker-0.4.5/lib/persian-date.js',
            'plugins/pwt.datepicker-0.4.5/dist/js/persian-datepicker-0.4.5.min.js',


        ],
        'ie9-js' => [
            'plugins/respond.min.js',
            'plugins/excanvas.min.js'
        ],
    ],

    /**
     * Preload assets.
     * Here you may set which assets (CSS files, JavaScript files or collections)
     * should be loaded by default even if you don't explicitly add them on run time.
     *
     * @var array
     */
    //'autoload' => array('jquery-cdn'),

];
