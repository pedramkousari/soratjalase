<?php

return [

    'default'   => [
        'length'    => 3,
        'width'     => 100,
        'height'    => 46,
        'quality'   => 90,
    ],

    'flat'   => [
        'length'    => 3,
        'width'     => 100,
        'height'    => 35,
        'quality'   => 2,
        'lines'     => 1,
        'bgImage'   => false,
        'bgColor'   => '#FFFFFF',
        'fontColors'=> ['#916B14', '#9B5015', '#842619', '#9B1E70', '#601491'],
        'characters'=> '0123456789',
        'contrast'  => -5,
        'blur'      => false,
        'angle'     => 1,
    ],

    'mini'   => [
        'length'    => 3,
        'width'     => 60,
        'height'    => 32,
    ],

    'inverse'   => [
        'length'    => 5,
        'width'     => 120,
        'height'    => 36,
        'quality'   => 90,
        'sensitive' => true,
        'angle'     => 12,
        'sharpen'   => 10,
        'blur'      => 2,
        'invert'    => true,
        'contrast'  => -5,
    ]

];
