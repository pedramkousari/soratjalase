<?php
	
return [
    'site' => [
        'title' => env('APP_TITLE', 'App title'),
        'lang'  => env('APP_LANG_PREFIX', 'language')
    ],
    'perPage' => env('APP_PERPAGE', 10),

    'view' => [ //## key == section / value == template
        'Admin'	=> ['admin'],
    ],
];