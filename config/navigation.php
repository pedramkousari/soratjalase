<?php
/**
 * Navigation
 * @author Salah
 * @param label
 * @param controller
 * @param method
 * @param url
 * @param fa -icon (font awesome)
 * @param children
 * @param route
 */
return [
    [
        'label' => 'language.home',
        'url' => '/Home',
        'fa-icon' => 'fa fa-home',
    ],
    [
        'label' => 'company info',
        'url' => '/Company',
        'fa-icon' => 'fa fa-info-circle',
        'permission' => 'Admin',
    ],
    [
        'label' => 'language.users section',
        'permission' => 'Admin, Programmer',
        'fa-icon' => 'fa fa-users',
        'children' => [
            [
                'label' => 'language.users group management',
                'url' => '/UserGroup',
                'permission' => 'Programmer',
                'fa-icon' => 'fa fa-users'
            ],
            [
                'label' => 'language.users management',
                'url' => '/User',
                'permission' => 'Admin, Programmer',
                'fa-icon' => 'fa fa-user'
            ],
        ]
    ],
    [
        'label' => 'language.facilities',
        'fa-icon' => 'fa fa-th',
        'children' => [
            [
                'label' => 'language.logout',
                'url' => '/logout',
                'fa-icon' => 'fa fa-sign-out'
            ],

        ]
    ],
 ];