<?php
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use App\Models\DocumentType;
use App\Models\UserGroup;
use App\Models\Activity;
use App\Models\User;


class DatabaseSeeder extends Seeder {

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        Model::unguard();

        $this->call('UsersGroupTableSeeder');
        $this->call('UsersTableSeeder');


        $this->call('DocumentTypeTableSeeder');
        $this->command->info('DocumentType table seeded!');


        $this->call('ActivityTableSeeder');
        $this->command->info('Activity table seeded!');
    }
}

class UsersGroupTableSeeder extends Seeder
{
    public function run()
    {
        $ugRow = UserGroup::where('xgroup', 'Admin')->first();
        if (!$ugRow) {
          UserGroup::store(['xgroup' => 'Admin']);
        }

        $ugRow = UserGroup::where('xgroup', 'Programmer')->first();
        if (!$ugRow) {
          UserGroup::store(['xgroup' => 'Programmer']);
        }

        $ugRow = UserGroup::where('xgroup', 'Customer')->first();
        if (!$ugRow) {
            UserGroup::store(['xgroup' => 'Customer']);
        }

    }
}


class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $userList = User::where('xusername', 'programmer')
            ->orWhere('xusername', 'sa')
            ->lists('xuserid');
        if ($userList) {
            $userListStr = implode(',', $userList);
            User::remove("xuserid IN ($userListStr)");
            DB::statement('ALTER TABLE xxuser_name AUTO_INCREMENT = 1');
        }
        
        User::store([
            'xxuser_name' => [
                'xusername' => 'sa', 
                'xpassword' => '-+',
                'email' => 'sales@aryaweb.com'
            ],
            'xxuser_info' => [
                'xname' => 'Sales',
                'xfamily' => 'aryaweb'
            ]
        ], ['Admin']);

        User::store([
            'xxuser_name' => [
                'xusername' => 'programmer', 
                'xpassword' => '1aryaweb39',
                'email' => 'programmer@aryaweb.com'
            ],
            'xxuser_info' => [
                'xname' => 'برنامه نویس'
            ]
        ], ['Admin', 'Programmer']);

        User::store([
            'xxuser_name' => [
                'xusername' => 'kosari@aryaweb.com',
                'xpassword' => '-+',
                'email' => 'kosari@aryaweb.com'
            ],
            'xxuser_info' => [
                'xname' => 'پدرام',
                'xfamily' => 'کوثری',
            ]
        ], ['Customer']);


        $this->command->info('User table seeded!');
    }
}

class DocumentTypeTableSeeder extends Seeder{

    public function run(){
        $arr = [
                                    'صورتجلسه انحلال',
                                    'صورتجلسه مجمع عادی سالیانه',
                                    'صورتجلسه انتخاب مدیران بازرسین',
                                    'صورتجلسه نقل و انتقال',
                                    'صورتجلسه کاهش سرمایه',
                                    'صورتجلسه تعیین حوزه',
                                    'صورتجلسه تغییر آدرس',
                                    'صورتجلسه الحاق موضوع',
                                    'صورتجلسه افزایش سرمایه',
                                    'صورتجلسه سرمایه تعهدی',
                                    'صورتجلسه تمدید بازرسین',
                                    'صورتجلسه تمدید مدیر تصفیه',
                                    'صورتجلسه اصلاح اساسنامه',
                                    'صورتجلسه تغییر حق امضا',
                                    'صورتجلسه تغییر نام',
                                    'صورتجلسه افزایش سرمایه',
                                    'صورتجلسه تغییر محل شعبه',
                                    'صورتجلسه تاسیس شعبه',
                                    'صورتجلسه افزایش و کاهش  تعداد اعضا',
                                    'صورتجلسه المثنی مدارک ثبتی',
                                    'اساسنامه شرکت سهامی خاص',
                                    'اظهارنامه ثبت شرکت سهامی خاص',
                                    'صورتجلسه هيئت مديره شرکت سهامی خاص',
                                    'صورتجلسه مجمع عمومي مؤسسين شرکت سهامی خاص',
                                    'اساسنامه شرکت مسئوليت محدود',
                                    'تقاضانامه ثبت شرکت مسئوليت محدود',
                                    'شرکتنامه شرکت مسئوليت محدود',
                                    'صورتجلسه مجمع عمومي مؤسسين شرکت مسئوليت محدود'
        ];
        foreach($arr as $key => $value){
            DocumentType::store([
                'xdocument_type' => $value
            ]);
        }
    }

}

class ActivityTableSeeder extends Seeder{

    public function run(){
        $arr = [
            'بازرگانی',
            'عمرانی',
            'راه سازی و ساختمان',
            'خدمات شهری',
            'تاسیساتی',
            'صنعتی',
            'معدنی',
            'نفتی',
            'کشت و صنعت',
            'نرم افزاری',
            'الکتریکال',
            'دکوراسیون',
            'تجهیزات پزشکی',
            'سرمایه گذاری',
            'حمل و نقل',
            'خدمات بانکی',
            'خودرو و وسایل نقلیه',
            'سنگ ها و فلزات قدیمی',
            'PVC (پی - وی - سی)',
        ];
        foreach($arr as $key => $value){
            Activity::store([
                'xactivity' => $value
            ]);
        }
    }

}