<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('xxcompany', function(Blueprint $table)
		{
			$table->increments('xcompanyid');
			$table->string('xfrm_type')->nullable()->nullable()->comment('ارتباط بین دو جدول');
			$table->integer('xcompany_activityid')->unsigned()->index()->nullable();
			$table->string('xcompany_name')->nullable();
			$table->string('xcompany_register_number',25)->nullable();
			$table->string('xcompany_national_code',13)->nullable();
			$table->string('xcompany_address')->nullable();
			$table->string('xcompany_postal_code')->nullable();
			$table->integer('xcompany_money')->nullable();

			$table->enum('xcompany_type',['مسئولیت محدود','سهامی خاص'])->nullable();

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('xxcompany');
	}

}
