<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrmRegisterCompanyLtdsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('xxfrm_register_company_ltd', function(Blueprint $table)
		{
			$table->increments('xfrm_register_company_ltdid');
			$table->integer('xdocumentid')->unsigned()->index();
			$table->integer('xcompanyid')->unsigned()->index();
			$table->boolean('xfrm_register_company_ltd_finished');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('xxfrm_register_company_ltd');
	}

}
