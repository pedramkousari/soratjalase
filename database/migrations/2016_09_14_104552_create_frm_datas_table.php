<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrmDatasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('xxfrm_data', function(Blueprint $table)
		{
			$table->increments('xfrm_dataid');
			$table->integer('xcompanyid')->unsigned()->index()->nullable();

			$table->integer('xfrm_id')->unsigned()->index()->nullable()->comment('ارتباط بین دو جدول');
			$table->string('xfrm_type')->nullable()->nullable()->comment('ارتباط بین دو جدول');


			$table->string('xfrm_data_chief')->nullable()->comment('رئیس')->nullable();
			$table->string('xfrm_data_supervisor_first')->nullable()->comment('ناظر')->nullable();
			$table->string('xfrm_data_supervisor_second')->nullable()->comment('ناظر')->nullable();
			$table->string('xfrm_data_clerk')->nullable()->comment('منشی')->nullable();
			$table->string('xfrm_data_agent')->nullable()->comment('وکیل')->nullable();

			$table->enum('xfrm_data_gender_chief',['male','female'])->nullable()->comment('جنسیت رئیس')->nullable();
			$table->enum('xfrm_data_gender_supervisor_first',['male','female'])->nullable()->comment('جنسیت ناظر')->nullable();
			$table->enum('xfrm_data_gender_supervisor_second',['male','female'])->nullable()->comment('جنسیت ناظر')->nullable();
			$table->enum('xfrm_data_gender_supervisor_clerk',['male','female'])->nullable()->comment('جنسیت منشی')->nullable();
			$table->enum('xfrm_data_gender_agent',['male','female'])->nullable()->comment('جنسیت وکیل')->nullable();

			$table->date('xfrm_data_date_of_meet')->nullable()->comment('تاریخ جلسه');
			$table->time('xfrm_data_time_of_meet')->nullable()->comment('ساعت جلسه');

			$table->softDeletes();
			$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('xxfrm_data');
	}

}
