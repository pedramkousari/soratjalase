<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrmLiquidationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('xxfrm_liquidation', function(Blueprint $table)
		{
			$table->increments('xfrm_liquidationid');
			$table->integer('xdocumentid')->unsigned()->index();
			$table->text('xfrm_liquidation_reason')->nullable();
			$table->string('xfrm_liquidation_location')->nullable();
			$table->string('xfrm_liquidation_postal_code')->nullable();
			$table->text('xfrm_liquidation_managers')->nullable();

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('xxfrm_liquidation');
	}

}
