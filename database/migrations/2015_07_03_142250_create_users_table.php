<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('xxuser_name', function(Blueprint $table)
		{
			$table->increments('xuserid');
			$table->string('xusername', 256);
			$table->string('xpassword', 60);
			$table->string('xpayload', 256);
			$table->string('email')->unique();
			$table->enum('xuser_status', ['active', 'inactive', 'deleted'])->default('active');
			$table->string('xremember_token', 256);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default('0000-00-00 00:00:00');
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('xxuser_name');
	}

}
