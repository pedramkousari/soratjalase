<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('xxsession', function(Blueprint $table)
		{
			$table->integer('id');
			$table->integer('userid');
			$table->mediumText('payload');
			$table->timestamp('last_activity');
			$table->timestamps();
			$table->index('userid');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('xxsession');
	}

}
