<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserssTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('xxuser', function(Blueprint $table)
		{
			$table->increments('xuser_id');
			$table->string('xuser_name')->nullable();
			$table->string('xuser_family')->nullable();
			$table->string('xuser_national_code')->nullable();
			$table->string('xuser_father_name')->nullable();
			$table->string('xuser_birth_date')->nullable();
			$table->string('xuser_id_number')->nullable();
			$table->string('xuser_address')->nullable();
			$table->string('xuser_post_code')->nullable();
			$table->string('xuser_gender')->nullable();

			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('xxuser');
	}

}
