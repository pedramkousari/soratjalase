<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('xxdocument', function(Blueprint $table)
		{
			$table->increments('xdocumentid');
			$table->integer('xuserid')->unsigned()->index();
			$table->integer('xdocument_typeid')->unsigned()->index();
			$table->string('xdocument_code',20)->nullable()->unique()->comment('year-documentTypeId-userId-formId');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('xxdocument');
	}

}
