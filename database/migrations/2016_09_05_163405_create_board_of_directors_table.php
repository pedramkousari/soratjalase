<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardOfDirectorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('xxboard_of_director', function(Blueprint $table)
		{
			$table->increments('xboard_of_directorid');
            $table->integer('xcompanyid')->unsingned()->index()->nullable();
            $table->integer('xuser_id')->unsingned()->index()->nullable();
            $table->boolean('xboard_of_director_signed_right')->comment('حق امضا');
            $table->boolean('xboard_of_director_non_partner')->comment('خارج از شرکا');
			$table->enum('xboard_of_director_type',['cherman','vice cherman','member','non-member'])->comment('رییس, نایب ری, عضو , خارج از عضو');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('xxboard_of_director');
	}

}
