<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyNamesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('xxcompany_name', function(Blueprint $table)
		{
			$table->increments('xcompany_nameid');
			$table->integer('xcompanyid')->unsingned()->index()->nullable();
			$table->string('xcompany_name')->nullable();
			$table->boolean('xcompany_name_main')->comment('نام اصلی');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('xxcompany_name');
	}

}
