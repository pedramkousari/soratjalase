<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('xxbranch', function(Blueprint $table)
		{
			$table->increments('xbranchid');
			$table->integer('xcompanyid')->unsingned()->index()->nullable();
			$table->integer('xuser_id')->unsingned()->index()->nullable();
			$table->string('xbranch_postal_code')->nullable();
			$table->string('xbranch_address')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('xxbranch');
	}

}
