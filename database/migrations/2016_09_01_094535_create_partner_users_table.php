<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('xxuser_partner', function(Blueprint $table)
		{
			$table->increments('xuser_partnerid');
			$table->integer('xuser_id')->unsingned()->index()->nullable();
			$table->integer('xcompanyid')->unsingned()->index()->nullable();
			$table->string('xuser_partner_type')->nullable();
			$table->integer('xuser_partner_stock')->unsingned()->nullable();
			$table->double('xuser_partner_money')->unsingned()->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('xxuser_partner');
	}

}
