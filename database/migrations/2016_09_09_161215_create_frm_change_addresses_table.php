<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrmChangeAddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('xxfrm_change_address', function(Blueprint $table)
		{
			$table->increments('xfrm_change_addressid');
            $table->integer('xdocumentid')->unsigned()->index();

			$table->string('xfrm_change_address_company_from_address')->nullable();
			$table->string('xfrm_change_address_company_to_address')->nullable();
			$table->string('xfrm_change_address_company_from_postal_code',20)->nullable();
			$table->string('xfrm_change_address_company_to_postal_code',20)->nullable();

            $table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('xxfrm_change_address');
	}

}
