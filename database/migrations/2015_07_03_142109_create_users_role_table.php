<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersRoleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('xxuser_role', function($table) {
			$table->increments('xroleid');
			$table->integer('xgroupid');			 
			$table->integer('xuserid');
			$table->unique(['xgroupid', 'xuserid']);
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('xxuser_role');
	}

}
