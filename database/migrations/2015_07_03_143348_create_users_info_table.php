<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersInfoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('xxuser_info', function(Blueprint $table) {
			$table->increments('xuserid');
			$table->string('xname', 128);
			$table->string('xfamily', 128);
			$table->enum('xgender', ['male', 'female'])->nullable();
			$table->date('xbirthday')->nullable();
			$table->string('xtel', 32)->nullable();
			$table->string('xfax', 32)->nullable();
			$table->string('xmobile', 32)->nullable();
			$table->string('xzipcode', 32)->nullable();
			$table->string('xcurrentip', 64)->nullable();
			$table->timestamp('xcreationdate');
			$table->mediumText('xaddress')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('xxuser_info');
	}

}
