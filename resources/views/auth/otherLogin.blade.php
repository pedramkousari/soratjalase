
<div class="login-options">
    <h4>Or login with</h4>
    <ul class="social-icons">
        <li>
            <a class="facebook" data-original-title="facebook" href="#">
            </a>
        </li>
        <li>
            <a class="twitter" data-original-title="Twitter" href="#">
            </a>
        </li>
        <li>
            <a class="googleplus" data-original-title="Goole Plus" href="#">
            </a>
        </li>
        <li>
            <a class="linkedin" data-original-title="Linkedin" href="#">
            </a>
        </li>
    </ul>
</div>