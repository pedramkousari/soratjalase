<!-- BEGIN FORGOT PASSWORD FORM -->
{{-- action('RemindersController@store') --}}
<form action="{{ url('/password/email') }}" class="forget-form" onsubmit="if ($('.forget-form').validate().form()) App.ajaxRequest(this, {'resultObject': 'result'});return false;" >
    <h3 style="margin-bottom: 30px;">@lang('language.forget password ?')</h3>
    
    <div class="result"></div> 
    
    <p>@lang('language.enter your e-mail address below to reset your password'):</p>

    <div class="form-group">
        <div class="input-icon">
            <i class="fa fa-envelope"></i>
            <input type="email" name="email" class="form-control placeholder-no-fix" placeholder="@lang('language.email')" autocomplete ="off" style="direction: ltr;" >
        </div>
    </div>
    <div class="form-actions">
        <button type="button" id="back-btn" class="btn">
            <i class="m-icon-black @if(config('app.dir') == 'ltr') m-icon-swapleft @else m-icon-swapright @endif"></i>
            @lang('language.back')
        </button>
        <button class="submit btn green pull-right">
            @lang('language.send') 
            <i class="m-icon-white @if(config('app.dir') == 'ltr') m-icon-swapright @else m-icon-swapleft @endif"></i>
        </button>
    </div>
</form>
<!-- END FORGOT PASSWORD FORM -->