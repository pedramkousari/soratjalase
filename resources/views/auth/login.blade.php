<!DOCTYPE html>

<!--[if IE 8]>
    <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
    <html lang="en" class="ie9 no-js"> <![endif]-->
    <!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    @include('backend._head')
    <link href="{{ asset('/assets/plugins/metronic/css/pages/login.css') }}" rel="stylesheet" type="text/css"/>
</head>

<body class="login">

    <!-- BEGIN LOGO -->
    <div class="logo">@lang(env('APP_LANG_PREFIX') . '.' . env('APP_TITLE'))</div>
    <!-- END LOGO -->

    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        
        @include('auth.loginForm')
        
        <!-- END LOGIN FORM -->
        @include('auth.forgetForm')
        {{-- @include('auth.registerForm') --}}
    </div>
    <!-- END LOGIN -->
    @include('backend.footer')

    @include('backend.assets')

    <script src="{{ asset('/assets/scripts/custom/login.js') }}" type="text/javascript"></script>
</body>

<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function () {
        App.init();
        Login.init();
    });
</script>
<!-- END JAVASCRIPTS -->