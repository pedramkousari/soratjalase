<!DOCTYPE html>

<!--[if IE 8]>
    <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
    <html lang="en" class="ie9 no-js"> <![endif]-->
    <!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    @include('backend._head')
</head>
<body class="login">

    <!-- BEGIN LOGO -->
    <div class="logo">@lang(env('APP_LANG_PREFIX') . '.' . env('APP_TITLE'))</div>
    <!-- END LOGO -->

    <!-- BEGIN LOGIN -->
    <div class="login">
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="reset-password-form" method="POST" action="{{ url('/password/reset') }}">                      
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="token" value="{{ $token }}">
                <h3 class="form-title">@lang('language.change password')</h3>
             
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span>@lang('language.enter email and password')</span>
                </div>

                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">@lang('language.email')</label>

                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input type="email" name="email" class="form-control placeholder-no-fix ltr" autocomplete="off" placeholder="@lang('language.email')" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">@lang('language.password')</label>

                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input type="password" id="password" name="password" class="form-control placeholder-no-fix ltr" autocomplete="off" placeholder="@lang('language.password')" min-length="6" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">@lang('language.confirm password')</label>

                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input type="password" id="password_confirmation" name="password_confirmation" class="form-control placeholder-no-fix ltr" autocomplete="off" placeholder="@lang('language.confirm password')" min-length="6" required>
                    </div>
                </div>  

                <div class="form-actions">
                    <button type="submit" class="btn green @if(config('app.dir') == 'ltr') pull-left @else pull-right @endif">
                        @lang('language.submit') 
                        <i class="m-icon-white @if(config('app.dir') == 'ltr') m-icon-swapright @else m-icon-swapleft @endif"></i>
                    </button>
                </div>          
            </form>
        </div>    
    </div>    
    @include('backend.assets')  

    <link href="{{ asset('/assets/plugins/metronic/css/pages/login.css') }}" rel="stylesheet" type="text/css"/>
    <script src="{{ asset('/assets/scripts/custom/login.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        jQuery(document).ready(function () {
            App.init();
            Login.init();
        });
    </script>
</body>

</html>