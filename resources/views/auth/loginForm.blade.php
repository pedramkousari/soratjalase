<form action="{{ url('/auth/login') }}" class="login-form" method="POST"> 
    <input type="hidden" name="_token" value="{{ csrf_token() }}"  >
    <h3 class="form-title">@lang('language.login to your account')</h3>
   
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(Session::get('success'))
        <div class="alert alert-success">
            <button class="close" data-close="alert"></button>
            <span>{!! Session::get('success') !!}</span>
        </div>
    @endif

    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button>
        <span>@lang('language.enter any username and password.')</span>
    </div>

    {{-- Username --}}
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">@lang('language.username')</label>

        <div class="input-icon">
            <i class="fa fa-user"></i>
            <input type="text" name='username' class="form-control placeholder-no-fix" placeholder="@lang('language.username')" autocomplete="off" style="direction: ltr" value="{{ old('username') }}">
        </div>
    </div>

    {{-- Password --}}
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">@lang('language.password')</label>

        <div class="input-icon">
            <i class="fa fa-lock"></i>
            <input type="password" name="password" class="form-control placeholder-no-fix" placeholder="@lang('language.password')" autocomplete="off" style="direction: ltr">
        </div>
    </div>
    
    {{-- Captcha --}}
    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <div class="input-group">
                    <input type="text" name="captcha" class="form-control placeholder-no-fix ltr" autocomplete="off" placeholder="@lang('language.security code')">
                    <span class="input-group-btn">
                        <button class="btn green" style="padding: 7px 14px;" type="button" onclick="App.reCaptcha(this)" title="@lang('language.recaptcha')">
                            <i class="fa fa-refresh"></i>
                        </button>
                    </span>
                </div>
            </div>
            <div class="col-md-6 login_captcha" style="text-align: left;">
                <img class="captcha" src="{{ captcha_src('flat') }}">
            </div>
        </div>
    </div>

    {{-- Remember and submit --}}
    <div class="form-actions">
        <input type="checkbox" name="remember" value="1" @if (old('remember')) checked="checked" @endif >
        <label name="remember" class="checkbox" style="padding: 0;">@lang('language.remember me')</label>
        <button type="submit" class="btn green pull-right">
            @lang('language.login')
            <i class="m-icon-white @if(config('app.dir') == 'ltr') m-icon-swapright @else m-icon-swapleft @endif"></i>
        </button>
    </div>

    {{-- @include('auth.otherLogin') --}}
    <div class="forget-password">
        <h4>@lang('language.forgot your password ?')</h4>

        <p>
            @lang('language.no worries, click')
            <a href="javascript:;" id="forget-password">@lang('language.here')</a>
            @lang('language.to reset your password.')
        </p>
    </div>
</form>