<!-- responsive -->
<div id="edit-{{ str_random() }}" class="@if(!Request::ajax() and @$viewEdit) @else modal fade @endif edit-responsive"  tabindex="-1" data-width="{{ @$windowWidth ? $windowWidth:  760 }}" @if (@$autoSave) data-autosave="{'periodTime': '{{ intval($autoSave) }}'}" @endif {{ @$disableESC ? 'data-keyboard="false"':  '' }}
    data-draggable="{{ @$draggable ? $draggable:  'true' }}" data-resizable="{{ @$resizable ? $resizable:  'false' }}">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">
            @if (@$title && @!$customTitle)
                @if (config('app.id')) @lang('language.edit') @else @lang('language.create') @endif
                @lang(preg_match('/^language\./', $title) ? $title:  (config('custom.site.lang', 'language') . '.' . $title))
            @elseif (@$customTitle)
                {{ $customTitle }}
            @else
                @if (config('app.id')) 
                    @lang('language.edit') 
                @else 
                    @lang('language.create')  
                @endif
                @lang(config('custom.site.lang', 'language') . '.' . strtolower(config('app.controller')))
            @endif
        </h4>
    </div>
    
    {!! Form::open(array('url' => getCurrentURL('controller') . '/' . (@$storeMethod ? $storeMethod:  'store') . '/' . config('app.id'), 'name' => 'frm_' . str_random(), 'id' => 'frm-' . str_random(), 'files' => true, 'target' => 'edit_frame', 'data-aftersend' => @$afterSendForm,
            'onsubmit' => (@$onSendForm ? $onSendForm:  "App.sendPopupForm(this, event, " . (@$sendOptions ? $sendOptions:  '{}'). ");") 
        )) !!}
        <div class="modal-body">
            <div class="row">
                @yield('content')
            </div>
        </div>
        @include('backend.editBtn')
    {!! Form::close() !!}

    <script type="text/javascript">
        if (typeof App != 'undefined') {
            App.validateForm($('#' + $('button.store-btn').closest('form').attr('id')));
        }
    </script>
</div>