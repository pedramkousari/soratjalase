
@if (strtolower(config('app.controller')) == 'auth')
    <!-- BEGIN COPYRIGHT  Login -->
    <div class="copyright">
        {{ date('Y') }} &copy; 
        <a href="http://aryaweb.com" target="_blank" title="Aryaweb">
            @lang('language.aryaweb')
        </a>
    </div>
@else
    <!-- BEGIN FOOTER -->
    <div class="footer">
        <a class="footer-inner" href="http://aryaweb.com" target="_blank" title="Aryaweb">
            {{ date('Y') }} &copy; @lang('language.aryaweb')
        </a>
        <div class="footer-tools">
            <span class="go-top">
                <i class="fa fa-angle-up"></i>
            </span>
        </div>
    </div>
    <!-- END FOOTER -->
@endif