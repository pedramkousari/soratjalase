<div class="modal-footer">
    @if (@$customEditBtn)
        @foreach($customEditBtn as $kb => $btn)
            <button class="tooltips" type="{{ @$btn['type'] ?: 'submit' }}" @if(@$btn['options']) @foreach($btn['options'] as $kop => $vop) {{ @$kop . '=' . $vop }} @endforeach @endif>
            	@if (@$btn['fa-icon'])
                	<i class="fa {{ @$btn['fa-icon'] }}"></i>
                @endif
                {{ @$btn['title'] }}
            </button>  
        @endforeach
    @endif

    @if (@!$editBtn['hideSubmit'])
    	<button type="submit" name="submitForm" class="btn btn-primary store-btn" onclick="return {!! (@$store ? $store : 'true') !!};">
    		<i class="fa fa-save"></i>
    		@lang('language.submit')
    	</button> 
    @endif

</div>