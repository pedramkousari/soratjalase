
@if(@$tplsearch)
    <div class="search-box table-responsive" style="display: none;">
        @include($tplsearch)
        <div class="search-btn-list pull-right">
            <input class="btn btn-default" type="reset" value="{{ trans('language.show all') }}" onclick="$('[data-advancedselect]').select2('val', '');$('.checker span').removeClass('checked');$('label.btn').removeClass('active');setTimeout(App.list, 10);" />

            <button type="submit" class="btn btn-default blue" type="button" onclick="App.list({search:  true});" >
                {{ trans('language.search') }}&nbsp;&nbsp;<i class="fa fa-search"></i>
            </button>

        </div>
        <div class="clearfix"></div>
    </div>
@endif