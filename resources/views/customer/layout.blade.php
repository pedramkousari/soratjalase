<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
	@include('customer._head')
</head>
<body class="{{ config('app.dir') }}">


    @include('index.header')

    <div class="container container-main">
        @yield('content')
    </div>

	@include('index.footer')
	@include('customer.assets')
</body>
</html>