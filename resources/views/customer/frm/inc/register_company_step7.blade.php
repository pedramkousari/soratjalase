<div class="row">
    <div class="col-md-12">
        <div class="title-inner">
            <div class="line"></div>
            <h4>زمینه ی فعالیت شرکت</h4>
            <h5></h5>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="text-left" for=""><span>نوع فعالیت شرکت</span></label>
            @unless(!@$activityList)
                <select name="frmCompany[xcompany_activityid]" required class="form-control">
                    <option value="">{{'اتخاب کنید'}}</option>
                    @foreach(@$activityList as $key => $value)
                        <option {{ @$activityId==$key ? 'selected' : null  }} value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                </select>
            @endunless
            <input class="invisible" type="text" value="{{ @$activityId }}" data-rule-selectcheck="نوع فعالیت را مشخص کنید." />

            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="text-left" for=""><span>توکیل به غیر</span></label>
            @unless(!@$userList)
                <select name="frmRegister[xuser_id]" required class="form-control">
                    <option value="">{{'اتخاب کنید'}}</option>
                    @foreach(@$userList as $key => $value)
                        <option {{ @$xuser_id==$key ? 'selected' : null  }} value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                </select>
            @endunless
            <input class="invisible" type="text" value="{{ @$xuser_id }}" data-rule-selectcheck="شفرد مورد نظر را انتخاب کنید." />
        </div>
    </div>
    <div class="clearfix"></div>
</div>