<div class="row">
    <div class="col-md-12">
        <div class="title-inner">
            <div class="line"></div>
            <h4>لیست شرکای شرکت</h4>
            <h5></h5>
        </div>
        <div class="clearfix"></div>
    </div>
    {{--<div class="col-md-12">--}}
    {{--<div class="form-group">--}}
    {{--<label class="text-left" for=""><span>تعداد اعضای حقیقی شرکت</span></label>--}}
    {{--<div class="col-md-3">--}}
    {{--<input type="text" name="cnt_user_partner" placeholder=""--}}
    {{--data-rule-required="true"--}}
    {{--data-rule-min="2"--}}
    {{--style="width:100px;float:none;"--}}
    {{--class="form-control col-md-9 cnt-user-partner text-center" />--}}
    {{--</div>--}}
    {{--<div class="col-md-1">--}}
    {{--<button type="button" class="btn btn-diff" onclick="Form.addUserPartner('.cnt-user-partner')">افزودن</button>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <div class="clearfix"></div>

    <div class="row mt-20">
        <div class="col-md-12">
            <h4 class="seperator-title">تعداد اعضای حقیقی شرکت</h4>
            @var('cnt', 1)

            @unless(@$userPartner)
                @var('userPartner', [1,1])
            @endunless
            <div class="row row-multi">
                <div class="col-md-2 text-center">کد ملی</div>
                <div class="col-md-1 text-center">نام</div>
                <div class="col-md-2 text-center">نام خانوادگی</div>
                <div class="col-md-1 text-center">نام پدر</div>
                <div class="col-md-1 text-center">تاریخ تولد</div>
                <div class="col-md-1 text-center">شماره شناسنامه</div>
                <div class="col-md-1 text-center">سهم به درصد</div>
                <div class="col-md-2 text-center">میزان سهم به ریال</div>
                <div class="col-md-1 text-center">عملیات</div>
            </div>
            @foreach(@$userPartner as $up)
                <div class="user-partner reset mt-20 row-multi-no-padding partner">
                    <input type="hidden" class="reset" name="userPartner[row{{ $cnt }}][xuser_partnerid]" value="{{ @$up['xuser_partnerid'] }}">
                    <input type="hidden" class="reset" name="userPartner[row{{ $cnt }}][xuser_id]" value="{{ @$up['xuser_id'] }}">

                    <div class="form-group col-md-2">
                        <input type="text" name="userPartner[row{{  $cnt }}][xuser_national_code]" value="{{ @$up['xuser_national_code'] }}" class="form-control ltr" autocomplete="off" placeholder="کد ملی"
                               required
                               data-rule-length = "10"
                               data-rule-number="true"
                                />

                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-1">
                        <input type="text" name="userPartner[row{{  $cnt }}][xuser_name]" value="{{ @$up['xuser_name'] }}" class="form-control rtl" autocomplete="off"  placeholder="نام"
                               required

                                />

                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-2">
                        <input type="text" name="userPartner[row{{  $cnt }}][xuser_family]" value="{{ @$up['xuser_family'] }}" class="form-control rtl" autocomplete="off"  placeholder="نام خانوادگی"
                               required />

                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-1">
                        <input type="text" name="userPartner[row{{  $cnt }}][xuser_father_name]" value="{{ @$up['xuser_father_name'] }}" class="form-control rtl" autocomplete="off"  placeholder="نام پدر"   required>

                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-1">
                        <input type="text" name="userPartner[row{{  $cnt }}][xuser_birth_date]" value="{{ @$up['xuser_birth_date'] ? FarsiLib::g2jDate($up['xuser_birth_date']) : null }}" class="form-control ltr" autocomplete="off" data-datepicker="{'ifFormat': '%Y/%m/%d'}"  placeholder="تاریخ تولد"  >

                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-1">
                        <input type="text" name="userPartner[row{{  $cnt }}][xuser_id_number]" value="{{ @$up['xuser_id_number'] }}" class="form-control ltr" autocomplete="off"  placeholder="شماره شناسنامه"   required>

                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-1">
                        <input type="text" name="userPartner[row{{  $cnt }}][xuser_partner_stock]" value="{{ @$up['xuser_partner_stock'] }}" class="form-control ltr" autocomplete="off"  placeholder="سهم به درصد"
                               onblur="Form.stockToMoney(this)"
                               data-partner-stock
                               required
                               data-rule-min = "1"
                               data-rule-max = "99"
                                />

                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-2">
                        <input type="text" name="userPartner[row{{  $cnt }}][xuser_partner_money]" value="{{ @$up['xuser_partner_money'] }}" class="form-control ltr" readonly autocomplete="off"  placeholder="سهم به ریال"
                               data-set-money
                               data-inputmask
                                />

                        <div class="clearfix"></div>
                    </div>


                    <div class="form-group col-md-1 ltr">
                        <a class="addBut" href="javascript:void(0);" onclick="App.duplicateRow('user-partner');">
                            <i class="fa fa-plus-square"></i>
                        </a>
                        <a class="reset remBut reset invisible" target="edit_frame" onclick="App.removeRow(this, 'user-partner',{{ @$up['xuser_partnerid'] ? '"/Customer/RegisterCompany/deleteUserPartner/' . @$up['xuser_partnerid'] . '"' : 'null' }} ,{{ @$up['xuser_partnerid']?@$up['xuser_partnerid'] : 'null' }},2);" href="javascript:void(0);" >
                            <i class="fa fa-trash"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                @var('cnt', ++$cnt)
            @endforeach
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row mt-20">
        <div class="col-md-12">
            <h4 class="seperator-title">تعداد اعضای حقوقی شرکت</h4>
            @var('cnt', 1)
            @unless(@$companyPartner)
                @var('companyPartner',[1])
            @endunless
            <div class="row row-multi">
                <div class="col-md-3 text-center">کد ملی شرکت</div>
                <div class="col-md-3 text-center">نام شرکت</div>
                <div class="col-md-2 text-center">سهم به درصد</div>
                <div class="col-md-2 text-center">میزان سهم به ریال</div>
                <div class="col-md-2 text-center">عملیات</div>
            </div>
            @foreach(@$companyPartner as $cp)
                <div class="company-partner reset mt-20 row-multi-no-padding partner">
                    <input type="hidden" class="reset" name="companyPartner[row{{ $cnt }}][xuser_partnerid]" value="{{ @$cp['xuser_partnerid'] }}">
                    <input type="hidden" class="reset" name="companyPartner[row{{ $cnt }}][xcompanyid]" value="{{ @$cp['xcompanyid'] }}">

                    <div class="form-group col-md-3">
                        <input type="text" name="companyPartner[row{{  $cnt }}][xcompany_national_code]" value="{{ @$cp['xcompany_national_code'] }}" class="form-control ltr" autocomplete="off" placeholder="کد ملی شرکت"
                               data-rule-number="true"
                                />

                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-3">
                        <input type="text" name="companyPartner[row{{  $cnt }}][xcompany_name]" value="{{ @$cp['xcompany_name'] }}" class="form-control ltr" autocomplete="off" placeholder="نام شرکت"


                                />

                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-2">
                        <input type="text" name="companyPartner[row{{  $cnt }}][xuser_partner_stock]" value="{{ @$cp['xuser_partner_stock'] }}" class="form-control ltr" autocomplete="off"  placeholder="سهم به درصد"
                               onblur="Form.stockToMoney(this)"
                               data-partner-stock
                               data-rule-min = "1"
                               data-rule-max = "99"
                                />

                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-2">
                        <input type="text" name="companyPartner[row{{  $cnt }}][xuser_partner_money]" value="{{ @$cp['xuser_partner_money'] }}" class="form-control ltr" readonly autocomplete="off"  placeholder="سهم به ریال"

                               data-set-money
                               data-inputmask
                                />

                        <div class="clearfix"></div>
                    </div>


                    <div class="form-group col-md-1 ltr">
                        <a class="addBut" href="javascript:void(0);" onclick="App.duplicateRow('company-partner');">
                            <i class="fa fa-plus-square"></i>
                        </a>
                        <a class="reset remBut invisible" target="edit_frame" onclick="App.removeRow(this, 'user-partner',{{ @$cp['xuser_partnerid'] ? '"/Customer/RegisterCompany/deleteUserPartner/' . @$cp['xuser_partnerid'] . '"' : 'null' }} ,{{ @$cp['xuser_partnerid']?@$cp['xuser_partnerid'] : 'null' }});" href="javascript:void(0);" >
                            <i class="fa fa-trash"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                @var('cnt', ++$cnt)
            @endforeach
        </div>
    </div>
    <div class="clearfix"></div>
    <script>
        setTimeout(function(){ App.normalizeRows('user-partner',null,2) },200);
        setTimeout(function(){ App.normalizeRows('company-partner') },200);

    </script>
</div>