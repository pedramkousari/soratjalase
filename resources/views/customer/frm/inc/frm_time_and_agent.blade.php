<div class="col-md-2">
    <div class="form-group">
        <label class="text-left" for=""><span>جنسیت</span></label>
        <select name="data[xfrm_data_gender_chief]" class="form-control" required>
            <option value="">-- انتخاب کنید --</option>
            <option {{ @$data['xfrm_data_gender_chief']=='male' ? 'selected' : null }} value="male">مرد</option>
            <option {{ @$data['xfrm_data_gender_chief']=='female' ? 'selected' : null }} value="female">زن</option>
        </select>
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="text-left" for=""><span>رئیس جلسه</span></label>
        <div class="row">
            @if(@$userPartner)
                <div class="form-group col-md-5">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="" value="0" {{ @$ub['xboard_of_director_non_partner']== 1 ? 'checked' : null  }} id="" onchange="changeUserBoard(this);">
                            <span class="fs-13">انتخاب از لیست</span>
                        </label>
                    </div>
                    <div class="clearfix"></div>
                </div>
            @endif
            <div class="form-group {{ @$userPartner ? 'col-md-7' : 'col-md-12'}}" data-user>
                <div class="row hidden">
                    @unless(!@$userPartner)
                        <select name="data[xfrm_data_chief]" class="form-control" required>
                            <option value="">{{'انتخاب کنید'}}</option>
                            @foreach(@$userPartner as $key => $value)
                                <option value="{{ $value }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    @endunless
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input class="form-control rtl" name="data[xfrm_data_chief]" value="{{@$data['xfrm_data_chief']}}" id=""
                               data-rule-required="true"
                               data-persian  />
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-2">
    <div class="form-group">
        <label class="text-left" for=""><span>جنسیت</span></label>
        <select name="data[xfrm_data_gender_supervisor_first]" class="form-control" required>
            <option value="">-- انتخاب کنید --</option>
            <option {{ @$data['xfrm_data_gender_supervisor_first']=='male' ? 'selected' : null }} value="male">مرد</option>
            <option {{ @$data['xfrm_data_gender_supervisor_first']=='female' ? 'selected' : null }} value="female">زن</option>
        </select>
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="text-left" for=""><span>ناظر جلسه</span></label>
        <div class="row">
            @if(@$userPartner)
                <div class="form-group col-md-5">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="" value="0" {{ @$ub['xboard_of_director_non_partner']== 1 ? 'checked' : null  }} id="" onchange="changeUserBoard(this);">
                            <span class="fs-13">انتخاب از لیست</span>
                        </label>
                    </div>
                    <div class="clearfix"></div>
                </div>
            @endif
            <div class="form-group {{ @$userPartner ? 'col-md-7' : 'col-md-12'}}" data-user>
                <div class="row hidden">
                    @unless(!@$userPartner)
                        <select name="data[xfrm_data_supervisor_first]" required class="form-control">
                            <option value="">{{'انتخاب کنید'}}</option>
                            @foreach(@$userPartner as $key => $value)
                                <option value="{{ $value }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    @endunless
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input class="form-control rtl" name="data[xfrm_data_supervisor_first]" value="{{@$data['xfrm_data_supervisor_first']}}" id=""
                               data-rule-required="true"
                               data-persian  />
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-2">
    <div class="form-group">
        <label class="text-left" for=""><span>جنسیت</span></label>
        <select name="data[xfrm_data_gender_supervisor_second]" class="form-control" required>
            <option value="">-- انتخاب کنید --</option>
            <option {{ @$data['xfrm_data_gender_supervisor_second']=='male' ? 'selected' : null }} value="male">مرد</option>
            <option {{ @$data['xfrm_data_gender_supervisor_second']=='female' ? 'selected' : null }} value="female">زن</option>
        </select>
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="text-left" for=""><span>ناظر جلسه</span></label>
        <div class="row">
            @if(@$userPartner)
                <div class="form-group col-md-5">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="" value="0" {{ @$ub['xboard_of_director_non_partner']== 1 ? 'checked' : null  }} id="" onchange="changeUserBoard(this);">
                            <span class="fs-13">انتخاب از لیست</span>
                        </label>
                    </div>
                    <div class="clearfix"></div>
                </div>
            @endif
            <div class="form-group {{ @$userPartner ? 'col-md-7' : 'col-md-12'}}" data-user>
                <div class="row hidden">
                    @unless(!@$userPartner)
                        <select name="data[xfrm_data_supervisor_second]" required class="form-control">
                            <option value="">{{'انتخاب کنید'}}</option>
                            @foreach(@$userPartner as $key => $value)
                                <option value="{{ $value }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    @endunless
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input class="form-control rtl" name="data[xfrm_data_supervisor_second]" value="{{@$data['xfrm_data_supervisor_second']}}" id=""
                               data-rule-required="true"
                               data-persian  />
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-2">
    <div class="form-group">
        <label class="text-left" for=""><span>جنسیت</span></label>
        <select name="data[xfrm_data_gender_supervisor_clerk]" class="form-control" required>
            <option value="">-- انتخاب کنید --</option>
            <option {{ @$data['xfrm_data_gender_supervisor_clerk']=='male' ? 'selected' : null }} value="male">مرد</option>
            <option {{ @$data['xfrm_data_gender_supervisor_clerk']=='female' ? 'selected' : null }} value="female">زن</option>
        </select>
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="text-left" for=""><span>منشی جلسه</span></label>
        <div class="row">
            @if(@$userPartner)
                <div class="form-group col-md-5">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="" value="0" {{ @$ub['xboard_of_director_non_partner']== 1 ? 'checked' : null  }} id="" onchange="changeUserBoard(this);">
                            <span class="fs-13">انتخاب از لیست</span>
                        </label>
                    </div>
                    <div class="clearfix"></div>
                </div>
            @endif
            <div class="form-group {{ @$userPartner ? 'col-md-7' : 'col-md-12'}}" data-user>
                <div class="row hidden">
                    @unless(!@$userPartner)
                        <select name="data[xfrm_data_clerk]" required class="form-control">
                            <option value="">{{'انتخاب کنید'}}</option>
                            @foreach(@$userPartner as $key => $value)
                                <option value="{{ $value }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    @endunless
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input class="form-control rtl" name="data[xfrm_data_clerk]" value="{{@$data['xfrm_data_clerk']}}" id=""
                               data-rule-required="true"
                               data-persian  />
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="divide"></div>
<div class="col-md-2">
    <div class="form-group">
        <label class="text-left" for=""><span>جنسیت</span></label>
        <select name="data[xfrm_data_gender_agent]" class="form-control" required>
            <option value="">-- انتخاب کنید --</option>
            <option {{ @$data['xfrm_data_gender_agent']=='male' ? 'selected' : null }} value="male">مرد</option>
            <option {{ @$data['xfrm_data_gender_agent']=='female' ? 'selected' : null }} value="female">زن</option>
        </select>
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label class="text-left" for=""><span>موکل</span></label>
        <div class="row">
            @if(@$userPartner)
            <div class="form-group col-md-5">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="" value="0" {{ @$ub['xboard_of_director_non_partner']== 1 ? 'checked' : null  }} id="" onchange="changeUserBoard(this);">
                        <span class="fs-13">انتخاب از لیست</span>
                    </label>
                </div>
                <div class="clearfix"></div>
            </div>
            @endif
            <div class="form-group {{ @$userPartner ? 'col-md-7' : 'col-md-12'}}" data-user>
                <div class="row hidden">
                    @unless(!@$userPartner)
                        <select name="data[xfrm_data_agent]" required class="form-control">
                            <option value="">{{'انتخاب کنید'}}</option>
                            @foreach(@$userPartner as $key => $value)
                                <option value="{{ $value }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    @endunless
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input class="form-control rtl" name="data[xfrm_data_agent]" value="{{@$data['xfrm_data_agent']}}" id=""
                               data-rule-required="true"
                               data-persian  />
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="text-left" for=""><span>تاریخ جلسه</span></label>
        <input type="text" name="data[xfrm_data_date_of_meet]" value="{{ @$data['xfrm_data_date_of_meet'] }}"
               data-date
               data-rule-required="true"
               class="form-control ltr" />
    </div>
</div>
<div class="col-md-2">
    <div class="form-group">
        <label class="text-left" for=""><span>ساعت جلسه</span></label>
        <input data-time name="data[xfrm_data_time_of_meet]" type="text" value="{{ @$data['xfrm_data_time_of_meet'] }}" class="form-control text-center"></input>
        <span class="add-on">
          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
          </i>
        </span>
    </div>
</div>
<div class="clearfix"></div>

<script>
    setTimeout(function(){
        $("[data-time]").pDatepicker({
            persianDigit: true,
            format: 'HH:mm:ss',
            timePicker: {
                enabled: true,
                showSeconds: true,
                showMeridian: true,
                scrollEnabled: true,
            },
            onlyTimePicker: true,
        });
        $("[data-date]").pDatepicker({
            format: 'YYYY/MM/DD',
            persianDigit: true,
            onlyTimePicker: false,
            navigator: {
                text: {
                    btnNextText: ">",
                    btnPrevText: "<"
                },
            }
        });
    },200);

    function changeUserBoard(obj){
        obj = $(obj);
        console.log(obj);
        if (obj.is(':checked')) {
            obj.closest('.row').find('[data-user] > div.row').last().addClass('hidden');
            obj.closest('.row').find('[data-user] > div.row').first().removeClass('hidden');
        }else{
            obj.closest('.row').find('[data-user] > div.row').first().addClass('hidden');
            obj.closest('.row').find('[data-user] > div.row').last().removeClass('hidden');
        }
    }
</script>
