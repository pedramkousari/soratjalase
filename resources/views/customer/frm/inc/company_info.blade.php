<div class="col-md-6">
    <div class="form-group">
        <label class="text-left" for=""><span>نام شرکت</span></label>
        <input type="text" name="company[xcompany_name]" value="{{ @$data['xcompany_name'] }}"
               data-rule-required="true"
               class="form-control" />
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label class="text-left" for=""><span>شماره ثبت شرکت</span></label>
        <input type="text" class="form-control ltr" name="company[xcompany_register_number]" value="{{@$data['xcompany_register_number']}}"
               data-rule-required="true" />
    </div>
</div>
<div class="clearfix"></div>

<div class="col-md-6">
    <div class="form-group">
        <label class="text-left" for=""><span>سرمایه ثبت شده</span></label>
        <input type="text" class="form-control" name="company[xcompany_money]" value="{{@$data['xcompany_money']}}"
               data-inputmask
               data-rule-required="true"
               data-rule-mony-company = "true"
               data-rule-required="true" />
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label class="text-left" for=""><span>شناسه ملی</span></label>
        <input class="form-control ltr" name="company[xcompany_national_code]" value="{{@$data['xcompany_national_code']}}" id=""
               data-rule-required="true"
               data-rule-length="13"
               data-rule-number="true"
                />
    </div>
</div>
<div class="clearfix"></div>