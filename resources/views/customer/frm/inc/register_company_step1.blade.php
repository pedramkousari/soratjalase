
    <input type="hidden" name="documentId" value="{{$documentId}}">
    <input type="hidden" name="companyId" value="{{$companyId}}">
    <div class="row">
        <div class="col-md-12">
            <div class="title-inner">
                <div class="line"></div>
                <h4>اطلاعات شرکت</h4>
                <h5>وارد نمودن بخشهای ستاره دار الزامی می باشد</h5>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="text-left" for=""><span>نوع شرکت </span></label>
                <input type="text" name="company[xcompany_type]" placeholder="" value="{{ $type }}" disabled
                       data-rule-required="true"
                       class="form-control" />
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="text-left" for=""><span>سرمایه شرکت </span></label>
                <input type="text" class="form-control ltr" name="company[xcompany_money]" value="{{@$company['xcompany_money']}}" placeholder=""
                       onblur="Form.setMoney(this)"
                       data-inputmask
                       data-rule-required="true"
                       data-rule-mony-company = "true"
                        >
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="text-left" for=""><span>آدرس شرکت</span></label>
                <input type="text" class="form-control" name="company[xcompany_address]" value="{{@$company['xcompany_address']}}" placeholder=""
                       data-rule-required="true" />
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="text-left" for=""><span>کد پستی شرکت</span></label>
                <input class="form-control ltr" name="company[xcompany_postal_code]" value="{{@$company['xcompany_postal_code']}}" id="" placeholder=""
                       data-rule-required="true"
                       data-rule-length="13"
                       data-rule-number="true"
                        />
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
<script>
    var money = {{@$company['xcompany_money'] ? @$company['xcompany_money'] : 0}};
    setTimeout(function(){
        if(money){
            Form.setMoney('[data-rule-mony-company]');
        }
    },200);
//    $(document).ready(function(){
//
//    })

</script>