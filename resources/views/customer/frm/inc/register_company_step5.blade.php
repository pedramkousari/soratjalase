<div class="row">
    <div class="col-md-12">
        <div class="title-inner">
            <div class="line"></div>
            <h4>نام های پیشنهادی</h4>
            <h5>فیلد اول به عنوان نام اصلی در نظر گرفته می شود.</h5>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    <div class="row mt-20">
        <div class="col-md-4 col-md-offset-4">
            <h4 class="seperator-title">نام های پیشنهادی</h4>
            @var('cnt', 1)

            <div class="row row-multi">
                <div class="col-md-10 text-center">نام پیشنهادی</div>
                <div class="col-md-2 text-center">عملیات</div>
            </div>

            @foreach(@$companyName as $cp)
                <div class="company-name reset mt-20 row-multi-no-padding fs-13">
                    <input type="hidden" class="reset" name="companyName[row{{ $cnt }}][xcompany_nameid]" value="{{ @$cp['xcompany_nameid'] }}">

                    <div class="form-group col-md-10">
                        <input type="text" name="companyName[row{{  $cnt }}][xcompany_name]" value="{{ @$cp['xcompany_name'] }}" class="form-control rtl" autocomplete="off" placeholder="نام شرکت"
                               required
                                />

                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group col-md-2 ltr">
                        <a class="addBut" href="javascript:void(0);" onclick="App.duplicateRow('company-name');">
                            <i class="fa fa-plus-square"></i>
                        </a>
                        <a class="reset remBut reset invisible" target="edit_frame" onclick="App.removeRow(this, 'company-name',{{ @$cp['xcompany_nameid'] ? '"/Customer/RegisterCompany/deleteCompanyName/' . @$cp['xcompany_nameid'] . '"' : 'null' }} ,{{ @$cp['xcompany_nameid']?@$cp['xcompany_nameid'] : 'null' }});" href="javascript:void(0);" >
                            <i class="fa fa-trash"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                @var('cnt', ++$cnt)
            @endforeach
        </div>
    </div>
    <div class="clearfix"></div>
    <script>
        setTimeout(function(){ App.normalizeRows('company-name',null) },200);
    </script>
</div>