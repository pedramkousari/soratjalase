<div class="row">
    <div class="col-md-12">
        <div class="title-inner">
            <div class="line"></div>
            <h4>مشخص کردن آدرس یکی از شرکا</h4>
            <h5>&nbsp;</h5>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label class="text-left" for=""><span>فرد مورد نظر</span></label>
            @unless(!@$userPartner)
                <select name="frm[xuser_id]" required class="form-control user-address" onchange="Form.setAttr(this , 'address-user');">
                    <option value="">{{'اتخاب کنید'}}</option>
                    @foreach(@$userPartner as $key => $value)
                        <option value="{{ $value['xid'] }}">{{ $value['name'] }}</option>
                    @endforeach
                </select>
            @endunless
        </div>
    </div>
    <div class="col-md-9">
        <div class="form-group">
            <label class="text-left" for=""><span>آدرس</span></label>
            <input type="text" class="form-control rtl" name="frm[xuser_address]" value="{{@$company['xuser_address']}}" placeholder="آدرس"
                   data-rule-required="true"
                   data-address-user />
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<script>
    json_address = {!! @$json_address?$json_address: '{}' !!};
</script>