<div class="row">
    <div class="col-md-12">
        <div class="title-inner">
            <div class="line"></div>
            <h4>شعب</h4>
            <h5>در صورت وجود شعبای خود را وارد کنید.</h5>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    <div class="row mt-20">
        <div class="col-md-12">
            <h4 class="seperator-title">شعب</h4>
            @var('cnt', 1)

            <div class="row row-multi">
                <div class="col-md-2 text-center">آدرس شعبه</div>
                <div class="col-md-2 text-center">کد پستی شعیه</div>
                <div class="col-md-2 text-center">شماره ملی مدیر شعبه</div>
                <div class="col-md-1 text-center">نام</div>
                <div class="col-md-1 text-center">نام خانوادگی</div>
                <div class="col-md-1 text-center">نام پدر</div>
                <div class="col-md-1 text-center">تاریخ تولد</div>
                <div class="col-md-1 text-center">شماره شناسنامه</div>
                <div class="col-md-1 text-center">عملیات</div>
            </div>

            @foreach(@$branch as $b)
                <div class="branch-list reset mt-20 row-multi-no-padding fs-13">
                    <input type="hidden" class="reset" name="branch[row{{ $cnt }}][xbranchid]" value="{{ @$b['xbranchid'] }}">
                    <input type="hidden" class="reset" name="user[row{{ $cnt }}][xuser_id]" value="{{ @$b['xuser_id'] }}">

                    <div class="form-group col-md-2">
                        <input type="text" name="branch[row{{  $cnt }}][xbranch_address]" value="{{ @$b['xbranch_address'] }}" class="form-control rtl" autocomplete="off" placeholder="آدرس شعبه"
                               data-on-depend
                               data-off-validate

                                />

                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-2">
                        <input type="text" name="branch[row{{  $cnt }}][xbranch_postal_code]" value="{{ @$b['xbranch_postal_code'] }}" class="form-control rtl" autocomplete="off" placeholder="کد پستی شعبه"
                               data-on-depend
                               data-off-validate
                               required />

                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-2">
                        <input type="text" name="user[row{{  $cnt }}][xuser_national_code]" value="{{ @$b['xuser_national_code'] }}" class="form-control ltr" autocomplete="off" placeholder="کد ملی مدیر"
                               data-rule-length = "10"
                               data-rule-number="true"
                               data-on-depend
                               data-off-validate
                               required />

                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-1">
                        <input type="text" name="user[row{{  $cnt }}][xuser_name]" value="{{ @$b['xuser_name'] }}" class="form-control rtl" autocomplete="off"  placeholder="نام"

                               data-on-depend
                               data-off-validate
                               required />

                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-1">
                        <input type="text" name="user[row{{  $cnt }}][xuser_family]" value="{{ @$b['xuser_family'] }}" class="form-control rtl" autocomplete="off"  placeholder="نام خانوادگی"
                               data-on-depend
                               data-off-validate
                               required/>

                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-1">
                        <input type="text" name="user[row{{  $cnt }}][xuser_father_name]" value="{{ @$b['xuser_father_name'] }}" class="form-control rtl" autocomplete="off"  placeholder="نام پدر"
                               data-on-depend
                               data-off-validate
                               required />

                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-1">
                        <input type="text" name="user[row{{  $cnt }}][xuser_birth_date]" value="{{ @$b['xuser_birth_date'] ? FarsiLib::g2jDate($b['xuser_birth_date']) : null }}" class="form-control ltr" autocomplete="off" data-datepicker="{'ifFormat': '%Y/%m/%d'}"  placeholder="تاریخ تولد"
                               data-on-depend
                               data-off-validate
                               required>

                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-1">
                        <input type="text" name="user[row{{  $cnt }}][xuser_id_number]" value="{{ @$b['xuser_id_number'] }}" class="form-control ltr" autocomplete="off"  placeholder="شماره شناسنامه"
                               data-on-depend
                               data-off-validate
                               required>

                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-1 ltr">
                        <a class="addBut" href="javascript:void(0);" onclick="App.duplicateRow('branch-list');">
                            <i class="fa fa-plus-square"></i>
                        </a>
                        <a class="reset remBut reset invisible" target="edit_frame" onclick="App.removeRow(this, 'branch-list',{{ @$b['xbranchid'] ? '"/Customer/RegisterCompany/deleteBranch/' . @$b['xbranchid'] . '"' : 'null' }} ,{{ @$b['xbranchid']?@$b['xbranchid'] : 'null' }});" href="javascript:void(0);" >
                            <i class="fa fa-trash"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                @var('cnt', ++$cnt)
            @endforeach
        </div>
    </div>
    <div class="clearfix"></div>
    <script>
        setTimeout(function(){ App.normalizeRows('branch-list',null) },200);
//            $('[data-on-depend]').each(function(k,v){
//                $(v).bind('blur', function(){
//                    $('[data-on-depend]').each(function(key,obj){
//                        obj = $(obj);
//                        if(obj.val() != ''){
//                            obj.closest('.branch-list').find('[data-on-depend]').each(function(key2,obj2){
//                                obj2 = $(obj2);
//                                obj2.removeAttr('data-off-validate');
//                            });
//                        }else{
//                            obj.closest('.branch-list').find('[data-on-depend]').each(function(key2,obj2){
//                                obj2 = $(obj2);
//                                obj2.attr('data-off-validate','true');
//                            });
//                        }
//                    });
//                });
//            });

    </script>
</div>