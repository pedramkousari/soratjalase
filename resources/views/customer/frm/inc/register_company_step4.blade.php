<div class="row">
    <div class="col-md-12">
        <div class="title-inner">
            <div class="line"></div>
            <h4>لیست شرکای شرکت</h4>
            <h5></h5>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    <div class="row mt-20">
        <div class="col-md-12">
            <h4 class="seperator-title">اعضای هیئت مدیره و دارندگان حق امضاء</h4>
            @var('cnt', 1)

            <div class="row row-multi">
                <div class="col-md-4 text-center">نوع عضویت</div>
                <div class="col-md-2 text-center">خارج از شرکا</div>
                <div class="col-md-4 text-center">نام و نام خانوادگی</div>
                <div class="col-md-1 text-center">حق امضاء</div>
                <div class="col-md-1 text-center">عملیات</div>
            </div>

            @foreach(@$userBoard as $ub)
                <div class="user-board reset mt-20 row-multi-no-padding fs-13">
                    <input type="hidden" class="reset" name="userBoard[row{{ $cnt }}][xboard_of_directorid]" value="{{ @$ub['xboard_of_directorid'] }}">


                    <div class="form-group col-md-4">
                        @unless(!@$boardType)
                            <select name="userBoard[row{{  $cnt }}][xboard_of_director_type]" required class="form-control" >
                                <option value="">{{'اتخاب کنید'}}</option>
                                @foreach(@$boardType as $key => $value)
                                    <option {{ $ub['xboard_of_director_type']==$key ? 'selected' : null  }} value="{{ $key }}">@t($value)</option>
                                @endforeach
                            </select>
                        @endunless
                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="userBoard[row{{  $cnt }}][xboard_of_director_non_partner]" value="1" {{ @$ub['xboard_of_director_non_partner']== 1 ? 'checked' : null  }} id="" onchange="changeUserBoard(this);">
                                <span>خارج از شرکا</span>
                            </label>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-4" data-user-board>
                        @unless(!@$userPartner)
                            <div class="row {{ @$ub['xboard_of_director_non_partner'] == 1 ? 'hidden' : null  }}">
                                <select name="userBoard[row{{  $cnt }}][xuser_id]" required class="form-control" >
                                    <option value="">{{'اتخاب کنید'}}</option>
                                    @foreach(@$userPartner as $key => $value)
                                        <option {{ $ub['xuser_id'] == $key ? 'selected' : null  }} value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endunless
                            <div class="row {{ @$ub['xboard_of_director_non_partner']== 1 ? null : 'hidden'  }}">
                                <input type="hidden" class="reset" name="user[row{{  $cnt }}][xuser_id]" value="{{ @$ub['xuser_id'] }}">
                                <div class="col-md-6">
                                    <input type="text" class="form-control rtl" name="user[row{{  $cnt }}][xuser_name]" value="{{@$ub['xuser_name']}}" placeholder="نام"
                                           data-rule-required="true"/>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control rtl" name="user[row{{  $cnt }}][xuser_family]" value="{{@$ub['xuser_family']}}" placeholder="نام خانوادگی"
                                           data-rule-required="true"/>
                                </div>
                            </div>


                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-1">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="userBoard[row{{  $cnt }}][xboard_of_director_signed_right]" {{ @$ub['xboard_of_director_signed_right']== 1 ? 'checked' : null  }} value="1" id="">
                                <span>حق امضا</span>
                            </label>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="form-group col-md-1 ltr">
                        <a class="addBut" href="javascript:void(0);" onclick="App.duplicateRow('user-board');">
                            <i class="fa fa-plus-square"></i>
                        </a>
                        <a class="reset remBut reset invisible" target="edit_frame" onclick="App.removeRow(this, 'user-board',{{ @$ub['xboard_of_directorid'] ? '"/Customer/RegisterCompany/deleteUserBoard/' . @$ub['xboard_of_directorid'] . '"' : 'null' }} ,{{ @$ub['xboard_of_directorid']?@$ub['xboard_of_directorid'] : 'null' }});" href="javascript:void(0);" >
                            <i class="fa fa-trash"></i>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                @var('cnt', ++$cnt)
            @endforeach
        </div>
    </div>
    <div class="clearfix"></div>
    <script>
        setTimeout(function(){ App.normalizeRows('user-board',null) },200);

        function changeUserBoard(obj){
            obj = $(obj);
            console.log(obj);
            if (obj.is(':checked')) {
                obj.closest('.user-board').find('[data-user-board] > div.row').first().addClass('hidden');
                obj.closest('.user-board').find('[data-user-board] > div.row').last().removeClass('hidden');
            }else{
                obj.closest('.user-board').find('[data-user-board] > div.row').last().addClass('hidden');
                obj.closest('.user-board').find('[data-user-board] > div.row').first().removeClass('hidden');
            }
        }
    </script>
</div>