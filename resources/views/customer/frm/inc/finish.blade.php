<div class="col-md-12">
	<div class="alert alert-success rtl text-left fs-13">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<strong class="rtl pull-left" style="
    padding-left: 10px;
">ثبت پیش نویس! </strong> <span class="pull-left"> کاربر گرامی پیش نویس</span><strong class="pull-left" style="
    padding-left: 5px;
    padding-right: 5px;
">{{ @$title }}</strong><span class="pull-left"> با شماره پیگیری </span><strong class="pull-left" style="
    padding-left: 5px;
    padding-right: 5px;
">{{ @$documentCode }}</strong><span class="">  شما تنظیم شده است با ورود به پروفایل کاربری خود نسبت به دریافت آن اقدام کنید.</span>
	</div>
</div>