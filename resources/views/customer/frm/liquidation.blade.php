@section('form')
    <div class="row form-step-finish">
        <input type="hidden" name="companyId" value="{{@$companyId}}" >
        <input type="hidden" name="frm_dataId" value="{{@$frm_dataId}}" >
        <input type="hidden" name="frmId" value="{{@$frmId}}" >


        @include('customer.frm.inc.company_info')
        <div class="divide"></div>

        <div class="col-md-12">
            <div class="form-group">
                <label class="text-left" for=""><span>علت انحلال</span></label>
                <textarea type="text" class="form-control" name="frm[xfrm_liquidation_reason]" data-rule-required="true"  maxlength="768" data-rule-maxlength="768"/>{{@$data['xfrm_liquidation_reason']}}</textarea>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="text-left" for=""><span>محل تصویه</span></label>
                <input type="text" class="form-control" name="frm[xfrm_change_address_company_to_address]" value="{{@$data['xfrm_change_address_company_to_address']}}"
                       data-rule-required="true" />
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="text-left" for=""><span>کد پستی</span></label>
                <input class="form-control ltr" name="frm[xfrm_liquidation_postal_code]" value="{{@$data['xfrm_liquidation_postal_code']}}" id=""
                       data-rule-required="true"
                       data-rule-length="10"
                       data-rule-number="true" />
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="divide"></div>
        <div class="row mt-20">
            <div class="col-md-8 col-md-offset-2">
                <h4 class="seperator-title">مدیران تصفیه</h4>
                @var('cnt', 1)

                <div class="row row-multi">
                    <div class="col-md-3 text-center">نوع</div>
                    <div class="col-md-7 text-center">نام</div>
                    <div class="col-md-2 text-center">عملیات</div>
                </div>

                @foreach(@[1] as $cp)
                    <div class="managar-list reset mt-20 row-multi-no-padding fs-13">
                        <div class="form-group col-md-3">
                            <select name="frmArr[row{{ $cnt }}][personality_type]" required class="form-control">
                                <option value="">{{'انتخاب کنید'}}</option>
                                <option value="real">{{'حقیقی'}}</option>
                                <option value="legal">{{'حقوقی'}}</option>
                            </select>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group col-md-7">
                            <div class="row real">
                                <div class="form-group col-md-3 col-md-offset-1">
                                    <select name="frmArr[row{{ $cnt }}][personality_gender]" required class="form-control">
                                        <option value="">{{'انتخاب کنید'}}</option>
                                        <option value="male">{{'مرد'}}</option>
                                        <option value="female">{{'زن'}}</option>
                                    </select>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group col-md-6 col-md-offset-1">
                                    <input type="text" name="frmArr[row{{  $cnt }}][manager_name]" class="form-control rtl" autocomplete="off" placeholder="نام"
                                           required />
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="row legal hidden">
                                <div class="form-group col-md-6 col-md-offset-1">
                                    <input type="text" name="frmArr[row{{  $cnt }}][manager_name]" class="form-control rtl" autocomplete="off" placeholder="نام"
                                           required />
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2 ltr">
                            <a class="addBut" href="javascript:void(0);" onclick="App.duplicateRow('company-name');">
                                <i class="fa fa-plus-square"></i>
                            </a>
                            <a class="reset remBut reset invisible" target="edit_frame" onclick="App.removeRow(this, 'company-name',{{ @$cp['xcompany_nameid'] ? '"/Customer/RegisterCompany/deleteCompanyName/' . @$cp['xcompany_nameid'] . '"' : 'null' }} ,{{ @$cp['xcompany_nameid']?@$cp['xcompany_nameid'] : 'null' }});" href="javascript:void(0);" >
                                <i class="fa fa-trash"></i>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @var('cnt', ++$cnt)
                @endforeach
            </div>
        </div>
        <div class="clearfix"></div>


        <div class="divide"></div>
        @include('customer.frm.inc.frm_time_and_agent')
    </div>
    <script>
        setTimeout(function(){
            Wizard.init({
                urlSendData : '{{@$url}}'
            });
        },200);
    </script>
@endsection