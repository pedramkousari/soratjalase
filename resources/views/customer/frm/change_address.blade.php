@section('form')
    <div class="row form-step-finish">
        <input type="hidden" name="companyId" value="{{@$companyId}}" >
        <input type="hidden" name="frm_dataId" value="{{@$frm_dataId}}" >
        <input type="hidden" name="frmId" value="{{@$frmId}}" >


        @include('customer.frm.inc.company_info')
        <div class="divide"></div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="text-left" for=""><span>آدرس قدیم</span></label>
                <input type="text" class="form-control" name="frm[xfrm_change_address_company_from_address]" value="{{@$data['xfrm_change_address_company_from_address']}}"
                       data-rule-required="true" />
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="text-left" for=""><span>کد پستی قدیم</span></label>
                <input type="text" class="form-control ltr" name="frm[xfrm_change_address_company_from_postal_code]" value="{{@$data['xfrm_change_address_company_from_postal_code']}}"
                       data-rule-required="true"
                       data-rule-length="10"
                       data-rule-number="true" />
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="text-left" for=""><span>آدرس جدید</span></label>
                <input type="text" class="form-control" name="frm[xfrm_change_address_company_to_address]" value="{{@$data['xfrm_change_address_company_to_address']}}"
                       data-rule-required="true" />
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="text-left" for=""><span>کد پستی جدید</span></label>
                <input class="form-control ltr" name="frm[xfrm_change_address_company_to_postal_code]" value="{{@$data['xfrm_change_address_company_to_postal_code']}}" id=""
                       data-rule-required="true"
                       data-rule-length="10"
                       data-rule-number="true"
                />
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="divide"></div>
        @include('customer.frm.inc.frm_time_and_agent')
    </div>
    <script>
        setTimeout(function(){
            Wizard.init({
                urlSendData : '{{@$url}}'
            });
        },200);
    </script>
@endsection