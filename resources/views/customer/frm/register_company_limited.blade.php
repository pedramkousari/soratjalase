@section('form')
        <div class="form-step1">
            @include('customer.frm.inc.register_company_step1')
        </div>
        <div class="form-step2 display-none">
            {{--@include('customer.frm.inc.register_company_step2')--}}
        </div>
        <div class="form-step3 display-none">
            {{--@include('customer.frm.inc.register_company_step3')--}}
        </div>
        <div class="form-step4 display-none">
            {{--@include('customer.frm.inc.register_company_step4')--}}
        </div>
        <div class="form-step5 display-none">
            {{--@include('customer.frm.inc.register_company_step5')--}}
        </div>
        <div class="form-step6 display-none">
            {{--@include('customer.frm.inc.register_company_step5')--}}
        </div>
        <div class="form-step7 display-none">
            {{--@include('customer.frm.inc.register_company_step5')--}}
        </div>
<script>
    setTimeout("App.normalizeRows('user-partner',null,2)", 100);
    setTimeout("App.normalizeRows('company-partner')", 100);

    setTimeout(function(){
        Wizard.init({
            urlSendData : '{{@$url}}'
        });
    },200);

</script>
@endsection