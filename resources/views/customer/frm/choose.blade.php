@section('form')
    <div class="row">
        <div class="col-md-12">

            <form action="/{{@config('app.section')}}/{{@config('app.controller')}}/form" method="post" role="form">

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="text-left" for="documentType"><span>انتخاب نوع سند</span></label>
                        {!! Form::token()  !!}
                        @unless(!$documentTypeList)
                            <select name="frm[xdocument_typeid]" id="documentType" class="form-control">
                                <option value="">{{'انتخاب کنید'}}</option>
                                @foreach(@$documentTypeList as $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        @endunless
                    </div>
                </div>
                <div class="clearfix"></div>

            	<button type="submit" class="btn btn-primary pull-right">{{'ادامه'}}</button>
            </form>
        </div>
    </div>
@endsection