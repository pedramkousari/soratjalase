<meta charset="utf-8"/>
<title>
    {{ @$title ? (preg_match('/^language\./', $title) ? trans($title):  trans(config('custom.site.lang', 'language') . '.' . $title)):  '' }}  
    {{ @$title2 ? ' | ' . $title2:  ''}}
</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="target-densitydpi=device-dpi, initial-scale=1.0, user-scalable=no" />
<meta name="description" content="" />
<meta name="author" content="">

<!-- Favicons -->
<link rel="shortcut icon" href="">
<link rel="apple-touch-icon" href="" sizes="57x57">
<link rel="apple-touch-icon" href="" sizes="72x72">
<link rel="apple-touch-icon" href="" sizes="114x114">
<link rel="apple-touch-icon" href="" sizes="144x144">

@var('dir', config('app.dir', 'ltr'))
@assets('fontface')

@if ($dir == 'rtl') 
    @assets('core-css-rtl')
@else     
    @assets('core-css')
@endif

@if ($dir == 'rtl')  
    @assets('plugin-css')
    @assets('plugin-css-rtl')
    @assets('customer-css')
    {{--@assets('index-css-rtl')->config(array('pipeline' => compareAssets(Assets::getCss())))--}}
@else     
    @assets('plugin-css')
    @assets('index-css')->config(array('pipeline' => compareAssets(Assets::getCss())))
@endif

@if ($dir == 'rtl')
    @assets('core-js')
    @assets('core-js-rtl')->config(array('pipeline' => compareAssets(Assets::getJs())))
@else
    @assets('core-js')->config(array('pipeline' => compareAssets(Assets::getJs())))
@endif

{!! Assets::css() !!}
{!! Assets::js() !!}

<noscript>
    <link rel="stylesheet" href="{{ asset('/assets/plugins/jquery-file-upload/css/jquery.fileupload-noscript.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/jquery-file-upload/css/jquery.fileupload-ui-noscript.css') }}">
</noscript>

<script src="{{ asset('/assets/cache/' . jsLanguage()) }}"></script>  

<script type="text/javascript">
    var section = "{{ config('app.section') }}", controller = "{{ config('app.controller') }}", action = "{{ config('app.action') }}",  locale = "{{ config('app.localization') }}";
    var lang = "{{ App::getLocale() }}", environment = '{{ App::environment() }}', random = "{{ config('app.random') }}";
    window.dir =  "{{ config('app.dir') }}";

    jQuery(document).ready(function() {
        App.init(); // initlayout and core plugins  
        Index.init();
    });
</script>