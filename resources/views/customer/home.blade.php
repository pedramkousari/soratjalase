@section('content')
    <div class="row">
        <div class="col-md-12 main bg-withe">
            <div class="row">
                <div class="col-md-12 mb-30">
                    <div class="icon-title" >
                        <i class="fa {{@$title_icon}}"></i>
                    </div>
                    <h3 class="title text-left">{{@$title}}</h3>
                </div>
                <div class="col-md-12">
                    @if(@$step_cnt)
                    <div class="wizard">
                        @if(@$step_cnt-1)
                            <div class="progressBar">
                                <div class="progress-line"></div>
                            </div>
                            <div class="step step-{{@$step_cnt-1}}">
                                <div class="step-start active">
                                    <div class="circle-big"></div>
                                </div>
                                @for(@$i = 0 ; $i<@$step_cnt-1; $i++ )
                                    <div class="step-next">
                                        <div class="circle-big"></div>
                                        <div class="inner">
                                            <div class="circle-small hidden-xs"></div>
                                            <div class="circle-small hidden-xs"></div>
                                            <div class="circle-small hidden-xs"></div>
                                        </div>
                                    </div>
                                @endfor
                            </div>
                            <div class="clearfix"></div>
                        @endif

                        <div class="show-template">
                            <form class="form-step" novalidate>
                                @yield('form')

                                <div class="clearfix mb-30">
                                </div>
                                @if(@$step_cnt-1 && @$step_cnt)
                                    <button class="btn pull-left prev">مرحله قبلی</button>
                                @endif
                                <button type="submit" class="btn btn-primary pull-right @if((@$step_cnt-1) != 0) hidden @endif finish">{{@$btnFinishTitle ? $btnFinishTitle : 'پایان'}}</button>
                                @if(@$step_cnt-1 && @$step_cnt)
                                    <button class="btn btn-primary pull-right next">مرحله بعدی</button>
                                @endif
                            </form>
                        </div>
                            @if(@$step_cnt-1 && @$step_cnt)
                                <div class="btn-wizard">
                                    <a class="back"><i class="fa  fa-chevron-right fa-2x"></i></a>
                                    <a class="next"><i class="fa fa-chevron-left fa-2x"></i></a>
                                </div>
                            @endif
                    </div>
                    @else
                        @yield('form')
                    @endif
                </div>

            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            App.initAjax();
        });
    </script>
@endsection
