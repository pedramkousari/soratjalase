@section('content')

    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-md-9">
                <div class="row carousel-holder">

                    <div class="col-md-12 text-left">
                        {{--<img src="/images/bg/sor_03.jpg" style="position: absolute;z-index: -1;opacity: .2" class="img-responsive" alt="Image">--}}
                        <h5 class="fs-16 fb">معرفی سامانه ستاناد</h5>
                        <h4 class="fs-22 fb">سامانه ثبت و تنظیم صورتجلسات</h4>
                        <ul class="rslides">
                            <li><img src="/images/index.jpg" alt=""></li>
                            <li><img src="/assets/plugins/ResponsiveSlides.js-master/demo/images/1.jpg" alt=""></li>
                            <li><img src="/assets/plugins/ResponsiveSlides.js-master/demo/images/2.jpg" alt=""></li>
                        </ul>
                        {{--<img src="/images/index.jpg" class="img-responsive" alt="Image">--}}
                        <p class="fs-13">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و
                        </p>
                    </div>

                </div>
                <div class="row" >
                    <div class="btn-owl pull-right">
                        <div >
                            <div class="btn-owl-next-arrow"><i class="fa  fa-angle-right "></i></div>
                            <div class="btn-owl-prev-arrow"><i class="fa  fa-angle-left "></i></div>
                        </div>
                    </div>
                    <div class="content-owl">
                        <div id="owl-document" class="owl-carousel owl-theme ltr owl-document">
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-asasname.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/register-company.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-director.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-board.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-address.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-asasname.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-asasname.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-asasname.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-asasname.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-asasname.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-asasname.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-asasname.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-asasname.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-asasname.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-asasname.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-asasname.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-asasname.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-asasname.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-asasname.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <img src="/images/icon/owl-document/edit-asasname.png" class="img-responsive" alt="Image">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="divide hidden-md hidden-lg"></div>
            <div class="col-md-3 pull-right " >
                <div class="inner-banner-left-ctrl inner-banner-left hidden-xs hidden-sm">
                    <div class="row">
                        <div class="col-sm-6 col-md-4 col-lg-5">
                            <div class="row">
                                <div class="col-md-12">
                                    <img src="/images/icon/user-icon.png" class="img-responsive pull-left ml-10" alt="user">
                                    <h4 class="title-create-user color-rouged pull left text-left">ایجاد حساب کاربری</h4>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 register-holder">
                                    <div class="alert alert-danger {{ count($errors) <= 0 ? 'display-none' : '' }}">
                                        <ul>
                                            @if (count($errors) > 0)
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                    <form action="{{ config('app.controller') }}/register" method="post" role="form" onsubmit="return Index.send(this, {parent: $(this).closest('.register-holder'), captcha: $('.captcha')});" novalidate>
                                        <div class="form-group">
                                            <label for=""></label>
                                            <input type="text" class="form-control" name="xname" data-rule-persian required id="" placeholder="نام">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="xfamily" data-rule-persian required id="" placeholder="نام خانوادگی">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control ltr" name="email" required data-rule-email id="" autocomplete="off" placeholder="پست الکترونیکی">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control ltr" name="xmobile" required data-rule-pattern="09(1[0-9]|3[1-9]|2[1-9])[0-9]{3}[0-9]{4}" data-msg-pattern="لطفا شماره همراه صحیح وارد کنید." id="" autocomplete="off" placeholder="شماره تلفن همراه">
                                            <span class="help-block text-left fs-11">مثال: {{convertDigit('09372392767')}}</span>
                                        </div>
                                        {{-- Captcha --}}
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-xs-7 col-sm-6">
                                                    <div class="input-group">
                                                        <input type="text" name="captcha" class="form-control placeholder-no-fix ltr" autocomplete="off" placeholder="@lang('language.security code')" required>
                                                        <span class="input-group-btn">
                                                        <button class="btn green" style="padding: 6px 14px;top: 0; position: absolute;" type="button" onclick="App.reCaptcha(this)" title="@lang('language.recaptcha')">
                                                            <i class="fa fa-refresh"></i>
                                                        </button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-5 col-sm-6 login_captcha" style="text-align: left;">
                                                    <img class="captcha" src="{{ captcha_src('flat') }}">
                                                </div>
                                            </div>
                                        </div>

                                        <button type="submit" class="btn send-btn btn-primary bg-rouged form-control">ثبت نام حساب کاربری</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix visible-md visible-lg">
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-5">
                            <div class="row img-info">
                                <div class="col-xs-12">
                                    <p class="fs-13 text-justify mt-15">
                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه
                                    </p>
                                </div>
                                <div class="col-xs-4">
                                    <a href="javascript:vid(0)">
                                        <img src="public/images/icon/info.png" class="img-responsive" alt="Image">
                                        <span class="fs-11 fb color-grey">راهنمای کاربری</span>
                                    </a>
                                </div>
                                <div class="col-xs-4">
                                    <a href="javascript:vid(0)">
                                        <img src="public/images/icon/signin.png" class="img-responsive" alt="Image">
                                        <span class="fs-11 fb color-grey">مزایای عضویت</span>
                                    </a>
                                </div>
                                <div class="col-xs-4">
                                    <a href="javascript:vid(0)">
                                        <img src="public/images/icon/about.png" class="img-responsive" alt="Image">
                                        <span class="fs-11 fb color-grey">درباره ستاناد</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="inner-banner-right">
    </div>
@endsection