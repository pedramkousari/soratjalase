<!-- Footer -->
<div class="footer mt-20">
    <div class="container">
        <div class="row pt-5 pb-5">
            <div class="col-xs-12 col-sm-8 col-md-9 text-left hidden-xs hidden-md">
                <span>کلیه حقوق مادی و معنوی سامانه متعلق به موسسه تدبیرسازان آفریقا می باشد</span>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3 pull-right text-right">
                <span>طراحی و پیاده سازی: </span> <a href="javascript:void(0)" class="color-yellow">شرکت نارگان</a>
            </div>
        </div>
    </div>
</div>