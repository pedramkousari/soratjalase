@section('content')
        <!-- Top content -->
<div class="top-content text-left">

    <div class="inner-bg">
        <div class="container">
            <div class="row mb-30 mt-30 color-white">
                <div class="col-sm-8 col-sm-offset-2 text text-center">
                    <h1>@t('login or register forms proceedings') </h1>

                    <div class="description">
                    </div>
                </div>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row" data-ng-app="registerApp">
                <div class="col-sm-5">

                    <div class="form-box">
                        <div class="form-top color-white">
                            <div class="pull-left">
                                <h3 class="">@t('login to our site')</h3>

                                <p class="fs-14">@t('enter username and password to log on') :</p>
                            </div>
                            <div class="form-top-right pull-right text-right">
                                <i class="fa fa-lock"></i>
                            </div>
                        </div>
                        <div data-ng-controller="loginCtrl" class="form-bottom login-div">

                            <div class="alert alert-@{{alert}}" data-ng-show="showMessage">
                                <ul>
                                    <li data-ng-repeat="message in messageList">
                                        @{{message}}
                                    </li>
                                </ul>
                            </div>
                            <form name="loginForm" novalidate ng-submit="processForm({captcha:true})" >
                                <div class="form-group" ng-class="{ 'has-error' : loginForm.username.$invalid && !loginForm.username.$pristine }">
                                    <label class="sr-only" for="form-username">trans('language.name')</label>
                                    <input data-ng-model="frmData.username" name="username" type="text" placeholder="نام" class="form-username form-control" autocomplete="off" required>
                                    <span data-ng-show="loginForm.username.$touched  && loginForm.username.$invalid" class="help-block">
                                        نام کاربری را وارد کنید
                                    </span>
                                </div>
                                <div class="form-group" ng-class="{ 'has-error' : loginForm.password.$invalid && !loginForm.password.$pristine }">
                                    <label class="sr-only" for="form-password">trans('language.password')</label>
                                    <input ng-model="frmData.password" name="password" type="password" placeholder="کلمه عبور" class="form-password form-control ltr" autocomplete="off" required>
                                    <span data-ng-show="loginForm.password.$touched  && loginForm.password.$invalid" class="help-block">
                                        کلمه عبور خود را وارد کنید
                                    </span>
                                </div>
                                <div class="form-group" ng-class="{ 'has-error' : loginForm.captcha.$invalid && !loginForm.captcha.$pristine }">
                                    {{-- Captcha --}}
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group rtl">
                                                    <input type="text" data-ng-model="frmData.captcha" name="captcha" class="form-control placeholder-no-fix" autocomplete="off" placeholder="@lang('language.security code')" required>
                                                    <span class="input-group-btn">
                                                        <button class="btn green" type="button" onclick="App.reCaptcha(this)" title="@lang('language.recaptcha')">
                                                            <i class="fa fa-refresh"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6 login_captcha" style="text-align: left;">
                                                <img class="captcha" src="{{ preg_replace('/\?.*/', ('?' . config('app.random')), captcha_src('flat')) }}">
                                            </div>
                                        </div>
                                    </div>
                                    <span data-ng-show="loginForm.captcha.$touched  && loginForm.captcha.$invalid" class="help-block">
                                        کد امنیتی اشتباه است
                                    </span>
                                </div>
                                <button data-ng-disabled="loginForm.$invalid" type="submit" class="btn">@t('sign in') !</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-sm-1 middle-border"></div>
                <div class="col-sm-1"></div>

                <div class="col-sm-5">
                    <div class="form-box">
                        <div class="form-top color-white">
                            <div class="form-top-left pull-left">
                                <h3>@t('sign up')</h3>
                                <p class="fs-14">@t('fill in the form below') :</p>
                            </div>
                            <div class="form-top-right pull-right text-right">
                                <i class="fa fa-pencil"></i>
                            </div>
                        </div>
                        <div data-ng-controller="registerCtrl"  class="form-bottom register-div">

                            <div class="alert alert-@{{alert}}" data-ng-show="showMessage">
                                <ul>
                                    <li data-ng-repeat="message in messageList">
                                        @{{message}}
                                    </li>
                                </ul>
                            </div>

                            {{-- return onsubmit="Index.send(this, {parent: $(this).closest('.register-div')});" --}}
                            <form name="registerForm" class="registration-form" enctype="multipart/form-data" ng-submit="processForm({captcha:true})" novalidate>
                                <div class="form-group"  ng-class="{ 'has-error' : registerForm.name.$invalid && !registerForm.name.$pristine }">
                                    <label class="sr-only" for="form-first-name">trans('language.name')</label>
                                    <input data-ng-model="frmData.xname" placeholder="نام" class="form-first-name form-control" required="required" name="name" type="text" >
                                    <span data-ng-show="registerForm.name.$touched  && registerForm.name.$invalid" class="help-block">
                                        نام خو را وارد کنید
                                    </span>
                                </div>
                                <div class="form-group" ng-class="{ 'has-error' : registerForm.family.$invalid && !registerForm.family.$pristine }">
                                    <label class="sr-only" for="form-last-name">trans('language.family')</label>
                                    <input data-ng-model="frmData.xfamily" placeholder="نام خانوادگی" class="form-last-name form-control" required="required" name="family" type="text">
                                    <span data-ng-show="registerForm.family.$touched  && registerForm.family.$invalid" class="help-block">
                                        فامیلی خود را وارد کنید.
                                    </span>
                                </div>
                                <div class="form-group" ng-class="{ 'has-error' : registerForm.mobile.$invalid && !registerForm.mobile.$pristine }">
                                    <label class="sr-only" for="form-mobile">trans('language.mobile')</label>
                                    <input data-ng-model="frmData.xmobile" placeholder="موبایل" class="form-mobile form-control ltr" id="form-mobile" required="required" title="شماره تماس را وارد کنید" name="mobile" type="text">
                                    <span data-ng-show="registerForm.mobile.$touched  && registerForm.mobile.$invalid" class="help-block">
                                        شماره موبایل خود را وارد کنید.
                                    </span>
                                </div>
                                <div class="form-group" ng-class="{ 'has-error' : registerForm.email.$invalid && !registerForm.email.$pristine }">
                                    <label class="sr-only" for="form-email">trans('language.email')</label>
                                    <input data-ng-model="frmData.email" name="email" type="email" placeholder="پست الکترونیکی" class="form-email form-control ltr" required="required" title="پست الکترونیکی عبور را وارد کنید">
                                    <span data-ng-show="registerForm.email.$touched  && registerForm.email.$invalid" class="help-block">
                                        پست الکترونیکی خود را صحیح وارد کنید.
                                    </span>
                                </div>
                                <div class="form-group" ng-class="{ 'has-error' : registerForm.captcha.$invalid && !registerForm.captcha.$pristine }">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <input data-ng-model="frmData.captcha" placeholder="کد امنیتی" autocomplete="off" class="form-control placeholder-no-fix ltr" minlength="3" aria-describedby="captcha-error" required="required" title="کد امنیتی را وارد کنید" name="captcha" type="text">
                                                <span class="input-group-btn">
                                                    <button class="btn green" type="button" data-ng-click="App.reCaptcha(this)"  title="@lang('language.recaptcha')">
                                                        <i class="fa fa-refresh"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6" style="text-align: left;">
                                            <img class="captcha" src="{{ preg_replace('/\?.*/', ('?' . config('app.random')), captcha_src('flat')) }}">
                                        </div>
                                    </div>
                                    <span data-ng-show="registerForm.captcha.$touched  && registerForm.captcha.$invalid.required" class="help-block">
                                        کد امنیتی صحیح نیست
                                    </span>
                                </div>
                                <button data-ng-disabled="registerForm.$invalid" type="submit" class="btn">@t('sign me up') !</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
@endsection