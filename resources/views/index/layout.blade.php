<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
	@include('index._head')
</head>
<body class="{{ config('app.dir') }}">
	<div>
		@include('index.header')

		@yield('content')

		@include('index.footer')
		@include('index.assets')

		<script>
			if (typeof App != 'undefined') {
				$('.send-btn').each(function() {
					var frm = $(this).closest('form');
					if (frm.length) {
						var rand = Math.round(Math.random() * 10000);
						var formId = frm.attr('id') ? frm.attr('id') : ('frm-' + rand);
						frm.attr('id', formId);
						App.validateForm($('#' + formId),{errorElement: 'span'});
					}
				});
			}
		</script>
	</div>
</body>
</html>