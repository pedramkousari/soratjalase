<!-- Navigation -->
<nav class="navbar navbar-inverse bg-black" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <img src="/images/logo/logo.png" class="img-responsive logo" alt="ستاناد">
            <div class="col-xs-7 col-sm-9 pt-15">
                <a class="navbar-brand color-yellow title-header" href="#">ستـانـاد</a>
                <div class="clearfix">
                </div>
                <a class="navbar-brand color-grey title-header-desc" href="#">سامانه تنظیم صورت جلسات</a>
            </div>

        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
            <div class="header-login">
                <div class="row">
                    <div class="login-holder col-sm-12 col-md-9">
                        @if(!Auth::id())
                        <div class="alert alert-danger {{ count($errors) <= 0 ? 'display-none' : '' }}">
                            <ul>
                                @if (count($errors) > 0)
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                        <form action="{{ config('app.controller') }}/login" onsubmit="return Index.send(this, {parent: $(this).closest('.login-holder'),login: true , captcha : false});" method="post" class="form" role="form" novalidate>
                            <div class="row pt-15 no-padding-right">
                                <div class="col-xs-6 text-left">
                                    <span class="color-white fb">ورود به حساب کاربری</span>
                                </div>
                                <div class="col-xs-6 text-right no-padding-left">
                                    <a class="color-grey fs-13 fb" href="javascript:void(0)" >کلمه عبور را فراموش کرده اید؟</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="username" required id="" placeholder="نام کاربری">
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <input type="password" class="form-control" name="password" required id="" placeholder="کلمه عبور">
                                    </div>
                                </div>
                                <div class="col-sm-5 hidden-lg hidden-sm hidden-md">
                                    <button type="submit" class="btn btn-default form-control">ورود</button>
                                </div>

                                <button type="submit" class="btn btn-send btn-default hidden-xs">ورود</button>


                                <div class="clearfix"></div>
                            </div>
                        </form>
                        @else
                            <ul class="nav navbar-nav pull-right"><!-- BEGIN USER LOGIN DROPDOWN -->
                                <li class="dropdown user">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        @if (File::exists(config('upload.User') . Auth::id()))
                                            <img src="/pic/user/{{ Auth::id() . 'w29h29c1/' . str_random() }}.png?nocache=1" alt=""/>
                                        @else
                                            <img src="/assets/plugins/metronic/img/avatar.png" style="width: 29px; height: 29px;" alt=""/>
                                        @endif
                                        <span class="username">{{ auth()->user()->info()->xfullname }}</span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu text-left bg-black">
                                        <li>
                                            {{--<a href="{{ getCurrentURL('section') . '/User/profile' }}">--}}
                                            <a href="javascript:void(0)" class="color-white">
                                                <i class="fa fa-user"></i> @lang('language.my profile')
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="javascript:;" id="trigger_fullscreen" class="color-white">
                                                <i class="fa fa-arrows"></i> @lang('language.full screen')
                                            </a>
                                        </li>
                                        {{--<li>--}}
                                            {{--<a href="extra_lock.html">--}}
                                                {{--<i class="fa fa-lock"></i> @lang('language.lock screen')--}}
                                            {{--</a>--}}
                                        {{--</li>--}}
                                        <li>
                                            <a href="/auth/logout" class="color-white">
                                                <i class="fa fa-key"></i> @lang('language.logout')
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- END USER LOGIN DROPDOWN -->
                            </ul>
                            <!-- END TOP NAVIGATION MENU -->
                        @endif
                    </div>
                    <div class="col-md-3 hidden-sm">
                        <ul class="header-contact">
                            <li>خط ویژه</li>
                            <li class="color-yellow">{{ convertDigit('42595') }}</li>
                            <li>
                                <ul>
                                    <li><a href="javascript:void(0)"><img src="/images/icon/skype.png" alt="instagram"></a></li>
                                    <li><a href="javascript:void(0)"><img src="/images/icon/instagram.png" alt="instagram"></a></li>
                                    <li><a href="javascript:void(0)"><img src="/images/icon/telegram.png" alt="instagram"></a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>