<!-- Click here to reset your password: {{ url('password/reset/'.$token) }} -->
<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>@lang('language.to change your password, please click the following link'):</div>
		<div>
			{{ url('password/reset/'.$token) }}
		</div>
	</body>
</html>