<div class="row">
    {{-- Username --}}
    <div class="form-group col-md-3">
        <div class="input-icon">
            <i class="fa fa-user"></i>
            <input type="text" name="q[where][xusername][like][str]" class="form-control placeholder-no-fix ltr" placeholder="@lang('language.username')" />
        </div>
    </div>
    
    {{-- First name --}}           
    <div class="form-group col-md-3">
        <input type="text" name="q[where][xname][like][str]" class="form-control placeholder-no-fix" placeholder="@lang('language.first name')" />
    </div>  

    {{-- Last name --}}  
    <div class="form-group col-md-3">
        <input type="text" name="q[where][xfamily][like][str]" class="form-control placeholder-no-fix" placeholder="@lang('language.last name')" />
    </div>    
    
    {{-- Email --}}           
    <div class="form-group col-md-3">
        <div class="input-icon">
            <i class="fa fa-envelope"></i>
            <input type="text" name="q[where][email][like][str]" class="form-control placeholder-no-fix ltr" placeholder="@lang('language.email')" />
        </div>
    </div>
    <div class="clearfix"></div>
    
    {{-- Mobile --}}
    <div class="form-group col-md-3">
        <div class="input-icon">
            <i class="fa fa-mobile"></i>
            <input type="tel" name="q[where][xmobile][like][str]" class="form-control placeholder-no-fix ltr" placeholder="@lang('language.mobile')" />
        </div>
    </div>

    {{-- Phone --}}
    <div class="form-group col-md-3">
        <div class="input-icon">
            <i class="fa fa-phone"></i>
            <input type="tel" name="q[where][xtel][like][str]" class="form-control placeholder-no-fix ltr" placeholder="@lang('language.phone')" />
        </div>
    </div>

    {{-- Fax --}}
    <div class="form-group col-md-3">
        <div class="input-icon">
            <i class="fa fa-fax"></i>
            <input type="tel" name="q[where][xfax][like][str]" class="form-control placeholder-no-fix ltr" placeholder="@lang('language.fax')" />
        </div>
    </div>

    {{-- Group --}}
    <div class="form-group col-md-3">
        @if(@$groupList)
            <select name="q[where][ug.xgroupid][eq][int]" class="form-control" data-advancedselect="">
                <option value="">@lang('language.select')  @lang('language.group')...</option>
                @foreach ($groupList as $groupid => $group)
                    <option value="{{ $group['xgroupid'] }}">@lang('language.' . $group['xgroup'])</option>
                @endforeach 
            </select>
        @endif
    </div>   
    <div class="clearfix"></div>

    {{-- Gender --}}
    <div class="form-group col-md-3"> 
        <div class="row">
            <label class="control-label col-md-2" style="margin-top: 5px;">
                @lang('language.gender'):
            </label>
            <div class="col-md-10">
                @include('backend.partial.radioBox', ['lang' => 'language', 'list' => @$genderList, 'search' => true, 'name' => 'q[where][xgender][eq][str]'])
            </div>
        </div> 
    </div>  

    {{-- Status --}}
    <div class="form-group col-md-3"> 
        <div class="row">
            <label class="control-label col-md-2" style="margin-top: 5px;">
                @lang('language.status'):
            </label>
            <div class="col-md-10">
                @include('backend.partial.radioBox', ['lang' => 'language', 'list' => @$statusList, 'search' => true, 'name' => 'q[where][xuser_status][eq][str]'])
            </div>
        </div> 
    </div>  
</div>    
<script type="text/javascript" src="{{ asset('/assets/plugins/bootstrap-multiselect-master/dist/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript">
    $(window).load(function() {
        Custom.multiSelect($('select[multiple]'));      
    });
</script>