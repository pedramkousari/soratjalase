<div class="row">
    {{-- Company title --}}
    <div class="form-group col-md-3">
        <input class="form-control placeholder-no-fix" type="text" name="q[where][xcompany][like][str]" placeholder="@lang('language.name')" />
    </div> 

    {{-- Email --}}
    <div class="form-group col-md-3">
        <div class="input-icon">
            <i class="fa fa-envelope"></i>
            <input type="email" name="q[where][xcompany_email][like][str]" class="form-control placeholder-no-fix" style="direction: ltr;" placeholder="@lang('language.email')" />
        </div>
    </div>

    {{-- Phone --}}
    <div class="form-group col-md-3">
        <div class="input-icon">
            <i class="fa fa-phone"></i>
            <input type="tel" name="q[where][xcompany_tel][like][str]" class="form-control placeholder-no-fix" 
                style="direction: ltr;" placeholder="@lang('language.phone')" />
        </div>
    </div>

    {{-- Fax --}}
    <div class="form-group col-md-3">
        <div class="input-icon">
            <i class="fa fa-fax"></i>
            <input type="tel" name="q[where][xcompany_fax][like][str]" class="form-control placeholder-no-fix" 
                style="direction: ltr;" placeholder="@lang('language.fax')" />
        </div>
    </div>
    <div class="clearfix"></div> 

    {{-- Status --}}
    <div class="form-group col-md-3">
        <div class="row">
            <label class="control-label col-md-2" style="margin-top: 5px;">
                {{ trans('language.status') }}:
            </label>
            <div class="col-md-10">    
                @include('backend.partial.radioBox', ['lang' => 'language', 'list' => @$statusList, 'search' => true, 'name' => 'q[where][xcompany_status][eq][str]'])
            </div> 
        </div>       
    </div>    
</div>