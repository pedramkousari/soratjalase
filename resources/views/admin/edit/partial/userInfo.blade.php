{{-- First name --}}
<div class="form-group">
    <label class="control-label col-md-2">
       @lang('language.first name'):
        <span class="required">*</span>
    </label>
    <div class="col-md-10">
        <input type="text" name="frm[xxuser_info][xname]" value="{{ @$list['xname'] }}" class="form-control" autocomplete="off" required="required" title="{{ t('language.this field is required') }}">
        <span class="help-block">@lang("language.like"): @lang('language.john')</span>
    </div>
    <div class="clearfix"></div>
</div>

{{-- Last name --}}
<div class="form-group">
    <label class="control-label col-md-2">
       @lang('language.last name'):
        <span class="required">*</span>
    </label>
    <div class="col-md-10">
        <input type="text" name="frm[xxuser_info][xfamily]" value="{{ @$list['xfamily'] }}" class="form-control" autocomplete="off" required="required" title="{{ t('language.this field is required') }}">
        <span class="help-block">@lang("language.like"): @lang('language.smith')</span>
    </div>
    <div class="clearfix"></div>
</div>

{{-- Email --}}
<div class="form-group">
    <label class="control-label col-md-2">
       @lang('language.email'):
    </label>
    <div class="col-md-10" style="margin-bottom: 15px;"> 
        <div class="input-icon">
            <i class="fa fa-envelope"></i>
            <input type="email" name="frm[xxuser_name][email]" value="{{ @$list['email'] }}" class="form-control ltr" autocomplete="off">
        </div>    
    </div>
    <div class="clearfix"></div>
</div>

{{-- Gender --}}
<div class="form-group">
    <label class="control-label col-md-2">
       @lang('language.gender'):
    </label>
    <div class="col-md-10" style="margin-bottom: 15px;"> 
        @include('backend.partial.radioBox', ['lang' => 'language', 'list' => @$genderList, 'name' => 'frm[xxuser_info][xgender]', 'selected' => @$list['xgender']]) 
    </div>
    <div class="clearfix"></div>
</div>

{{-- Birthday --}}
<div class="form-group">
    <label class="control-label col-md-2">
       @lang('language.birthday'):
    </label>
    <div class="col-md-10" style="margin-bottom: 15px;"> 
      <input type="text" name="frm[xxuser_info][xbirthday]" value="{{ @$list['xbirthday'] }}" class="form-control" autocomplete="off" data-datepicker="">
    </div>
    <div class="clearfix"></div>
</div>

{{-- Mobile --}}
<div class="form-group">
    <label class="control-label col-md-2">
       @lang('language.mobile'):
    </label>
    <div class="col-md-10" style="margin-bottom: 15px;">
        <div class="input-icon">
            <i class="fa fa-mobile"></i>
            <input type="tel" name="frm[xxuser_info][xmobile]" value="{{ @$list['xmobile'] }}" class="form-control ltr" autocomplete="off">
        </div>    
    </div>
    <div class="clearfix"></div>
</div>

{{-- Phone --}}
<div class="form-group">
    <label class="control-label col-md-2">
       @lang('language.phone'):
    </label>
    <div class="col-md-10" style="margin-bottom: 15px;">
        <div class="input-icon">
            <i class="fa fa-phone"></i>
            <input type="tel" name="frm[xxuser_info][xtel]" value="{{ @$list['xtel'] }}" class="form-control ltr" autocomplete="off">
        </div>    
    </div>
    <div class="clearfix"></div>
</div>

{{-- Fax --}}
<div class="form-group">
    <label class="control-label col-md-2">
       @lang('language.fax'):
    </label>
    <div class="col-md-10" style="margin-bottom: 15px;">
        <div class="input-icon">
            <i class="fa fa-fax"></i>
            <input type="tel" name="frm[xxuser_info][xfax]" value="{{ @$list['xfax'] }}" class="form-control ltr" autocomplete="off">
        </div>    
    </div>
    <div class="clearfix"></div>
</div>

{{-- Address --}}
<div class="form-group">
    <label class="control-label col-md-2">
       @lang('language.address'):
    </label>
    <div class="col-md-10" style="margin-bottom: 15px;">
        <textarea name="frm[xxuser_info][xaddress]" rows="2" style="width: 100%;"></textarea> 
    </div>
    <div class="clearfix"></div>
</div>

@if (@$groupList)
    {{-- User Groups --}}
    <div class="form-group">
        <label class="control-label col-md-2">
           @lang('language.group'):
        </label>
        <div class="col-md-10" style="margin-bottom: 15px;">
            <select name="group[]" class="form-group" multiple="multiple">
                @foreach ($groupList as $key => $val)
                    <option value="{{ $val['xgroupid'] }}" {{ array_key_exists(@$val['xgroupid'], @$list['groupList'] ?: []) ? 'selected="selected"' : '' }}>
                        @lang('language.' . $val['xgroup'])
                    </option>
                @endforeach
            </select>
        </div>
    <div class="clearfix"></div>
    </div>
@endif

@if (config('app.action') != 'profile')
    @if (@$statusList)

    {{-- Status --}}
    <div class="form-group">
        <label class="control-label col-md-2">
           @lang('language.status'):
        </label>
        <div class="col-md-10" style="margin-bottom: 15px;">
            @include('backend.partial.radioBox', ['lang' => 'language', 'list' => @$statusList, 'name' => 'frm[xxuser_name][xuser_status]', 'selected' => @$list['xuser_status']]) 
        </div>
        <div class="clearfix"></div>
    </div>
    @endif    
@endif   

{{-- Register Date --}}
<div class="form-group">
    <label class="control-label col-md-2">
       @lang('language.register date'):
    </label>
    <div class="col-md-10 ltr" style="margin-bottom: 15px;">
        {{  App::getLocale() == 'fa' ? FarsiLib::g2jDate(@$list['xcreation_date']) : @$list['xcreation_date'] }}
    </div>
    <div class="clearfix"></div>
</div>
<script type="text/javascript">
    if (typeof Custom != 'undefined') {
        Custom.multiSelect($('select[multiple]'));    
    }
</script>