@section('content')
    <div class="col-xs-12">    
        {{-- Company title --}}
        <div class="form-group">
            <label class="control-label col-md-2">
                @t("company title"):
                <span class="required">*</span>
            </label>
            <div class="col-md-10">
                <input type="text" name="frm[xcompany]" value="{{ @$list['xcompany'] }}" class="form-control" autocomplete="off" required="required" title="{{ t('messages-list.please enter name') }}">
                <span class="help-block">@t("like"): Google</span>
            </div>
        </div>

        {{-- Email --}}
        <div class="form-group">
            <label class="control-label col-md-2">
                @lang("language.email"):
                <span class="required">*</span>
            </label>
            <div class="col-md-10">
                <div class="input-icon">
                    <i class="fa fa-envelope"></i>
                    <input type="email" name="frm[xcompany_email]" value="{{ @$list['xcompany_email'] }}" class="form-control" autocomplete="off" required="required" title="{{ t('messages-list.please enter valid email address') }}">
                    <span class="help-block">@t("like"): test@example.com</span>
                </div>    
            </div>
        </div>

        {{-- Phone --}}
        <div class="form-group">
            <label class="control-label col-md-2">
                @lang("language.phone"):
                <span class="required">*</span>
            </label>
            <div class="col-md-10">
                <div class="input-icon">
                    <i class="fa fa-phone"></i>
                    <input type="text" name="frm[xcompany_phone]" value="{{ @$list['xcompany_phone'] }}" class="form-control" autocomplete="off" required="required" title="{{ t('messages-list.please enter phone number') }}">
                    <span class="help-block">@t("like"): 123-456-789</span>
                </div>    
            </div>
        </div>

        {{-- Fax --}}
        <div class="form-group">
            <label class="control-label col-md-2">
                @lang("language.fax"):
            </label>
            <div class="col-md-10 mb-15">
                <div class="input-icon">
                    <i class="fa fa-fax"></i>
                    <input type="text" name="frm[xcompany_fax]" value="{{ @$list['xcompany_fax'] }}" class="form-control" autocomplete="off">
                </span>    
            </div>
        </div>

        {{-- Postal code --}}
        <div class="form-group">
            <label class="control-label col-md-2">
                @t("postal code"):
            </label>
            <div class="col-md-10 mb-15">
                <input type="text" name="frm[xcompany_postal_code]" value="{{ @$list['xcompany_postal_code'] }}" class="form-control" autocomplete="off" >
            </div>
        </div>

        {{-- Company Address --}}
        <div class="form-group">
            <label class="control-label col-md-2">
                @lang("language.address"):
            </label>
            <div class="col-md-10 mb-15">
                <textarea name="frm[xcompany_address]" class="form-control">{{ @$list['xcompany_address'] }}</textarea>
            </div>
        </div>

        {{-- Company Status --}}
        <div class="form-group">
            <label class="control-label col-md-2">
                @lang("language.status"):
            </label>
            <div class="col-md-10 mb-15">
                @include('backend.partial.radioBox', ['lang' => 'language', 'list' => @$statusList, 'name' => 'frm[xcompany_status]', 'selected' => @$list['xcompany_status']])
            </div>
        </div>

        {{-- Uploads List --}} 
        <div class="clearfix"></div>
        <div class="row" style="margin-top: 15px;">      
            <div class="form-group col-md-6">
                <label class="control-label col-md-3">
                    @lang('language.logo'):
                </label> 
                <div class="col-md-8">
                    @var('logoSrc', '/pic/company.logo/' . @$list['xcompanyid'])
                    @include('backend.partial.fileInput', [
                        'type' => 'logo', 
                        'name' => 'file/logo', 
                        'action' => 'deleteLogo', 
                        'id' => @$list['xcompanyid'],
                        'exist' => @$logoExist, 
                        'src' => $logoSrc . 'w200h150c1/' . str_random() . '.png?nocache=1', 
                        'srcLarge' => $logoSrc . '/' . str_random() . '.png?nocache=1', 
                    ])
                </div> 
             </div>   
        
            <div class="form-group col-md-6">
                <label class="control-label col-md-3">
                    @lang('language.stamp'):
                </label> 
                <div class="col-md-8">                
                    @var('stampSrc', '/pic/company.stamp/' . @$list['xcompanyid'])
                    @include('backend.partial.fileInput', [
                        'type' => 'stamp', 
                        'action' => 'deleteLogo', 
                        'name' => 'sampFile/stamp',
                        'id' => @$list['xcompanyid'],
                        'exist' => @$stampExist, 
                        'src' => $stampSrc . 'w200h150c1/' . str_random() . '.png?nocache=1', 
                        'srcLarge' => $stampSrc . '/' . str_random() . '.png?nocache=1'
                    ])
                </div> 
            </div>
        </div>    
    </div>
@stop