<!doctype html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>404</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->

    @if (!Request::ajax())
        <link href="{{ asset('/assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->


        <link href="{{ asset('/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/assets/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css"/>

        <!-- BEGIN THEME STYLES -->
        <link href="{{ asset('/assets/plugins/metronic/css/style-metronic.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/assets/plugins/metronic/css/style.css') }}" rel="stylesheet" type="text/css"/>  
        <link href="{{ asset('/assets/css/custom' . (config('app.dir') == 'rtl' ? '-rtl': '') . '.css') }}" rel="stylesheet" type="text/css"/>
    @endif  

    <link href="{{ asset('assets/plugins/metronic/css/pages/error.css') }}" rel="stylesheet" type="text/css" />
</head>

<!-- BEGIN BODY -->
<body class="page-404-full-page">
    <div class="row">
        <div class="col-md-12 page-404">
            <div class="number">
                 404
            </div>
            <div class="details">
                <h3>Oops! You're lost.</h3>
                <p>
                     We can not find the page you're looking for.<br/>
                    <a href="//home">
                         Return home
                    </a>
                     or try the search bar below.
                </p>
                <form action="#">
                    <div class="input-group input-medium">
                        <input type="text" class="form-control" placeholder="keyword...">
                        <span class="input-group-btn">
                            <button type="submit" class="btn blue"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                    <!-- /input-group -->
                </form>
            </div>
        </div>
    </div>
</body>
</html>