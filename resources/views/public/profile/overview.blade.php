<div class="row">
	<div class="col-md-3">
		<ul class="list-unstyled profile-nav">
			<li>
				<div style="min-height: 287px;">
					@if (File::exists(config('upload.User') . auth()->id()))
						<img src="/pic/user/{{ auth()->id() . 'w308h287c1/' . str_random() }}.png?nocache=1" class="img-responsive" alt="">
					@else
						<img style="margin: 0 auto;" src="/images/icon/no-image.png" class="img-responsive" alt="">
					@endif	
				</div>	
				{{-- <a href="#" class="profile-edit">{{ trans('language.edit') }}</a> --}}
			</li>
			<li>
				<a href="#">{{ trans('language.messages') }}
					<span>3</span>
				</a>
			</li>
			<li>
				<a href="#">{{ trans('language.settings') }}</a>
			</li>
		</ul>
	</div>
	<div class="col-md-9">
		<div class="row">
			<div class="col-md-8 profile-info">
				<h1>{{ auth()->user()->info()->xfullname }}</h1>
				<p>{{ auth()->user()->xusername }}</p>
				<p>{{ trans('language.' . auth()->user()->info()->xgender) }}</p>
				<ul class="list-inline">
					@if (auth()->user()->email)
						<li>												
							<i class="fa fa-envelope-o"></i> 
							<a href="mailto: {{ auth()->user()->email }}">
								{{ auth()->user()->email }}
							</a>
						</li>
					@endif

					@if (auth()->user()->info()->xbirthday > 0)
						<li>
							<i class="fa fa-calendar"></i> 
							{{ FarsiLib::g2jDate(auth()->user()->info()->xbirthday, true) }}
						</li>
					@endif
				</ul>

				@if (auth()->user()->info()->xtel or auth()->user()->info()->xmobile)
					<ul class="list-inline">
						@if (auth()->user()->info()->xtel)
							<li>												
								<i class="fa fa-phone"></i> 
								<span style="direction: ltr;display: inline-block;">
									{{ FarsiLib::en2faDigit(auth()->user()->info()->xtel) }}
								</span>
							</li>
						@endif
						@if (auth()->user()->info()->xmobile)	
							<li>												
								<i class="fa fa-mobile" style="font-size: 17px;"></i> 
								<span style="direction: ltr;display: inline-block;">
									{{ FarsiLib::en2faDigit(auth()->user()->info()->xmobile) }}
								</span>
							</li>
						@endif	
					</ul>
				@endif

				@if (auth()->user()->info()->xaddress)
					<ul class="list-inline">
						<li>
							<i class="fa fa-map-marker"></i> 
							{{ auth()->user()->info()->xaddress }}
						</li>
					</ul>
				@endif
			</div>
		</div>
	</div>
</div>