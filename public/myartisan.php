<?php
    if (@$_SERVER['HTTP_X_REQUESTED_WITH']=='XMLHttpRequest') {
        chdir('..');
        putenv("COMPOSER_HOME=/var/www/html/Application/laravel2/.composer");
        if(strpos($_REQUEST['command'], 'composer') === 0) {
            ini_set("memory_limit", "-1");
            set_time_limit(0);
            $res = shell_exec("/usr/bin/php -dmemory_limit=2048M /usr/bin/" . @$_REQUEST['command'] . ' 2>&1');
        } elseif(strpos($_REQUEST['command'], 'artisan') === 0) {
            $res = shell_exec('/usr/bin/php -dmemory_limit=2048M ' . @$_REQUEST['command'] . ' 2>&1');
        } else {
            $res = false;
        }
        if (!$res)
            die("<div style='background: #402626;margin: 2px 0;color: #E8BF6A;'>Invalid Command.</div>");
        else
            die(nl2br(htmlspecialchars($res)));
    }
?>
<!doctype html>
<!--[if lt IE 7]> <html lang="en-us" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html lang="en-us" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html lang="en-us" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-us" class="no-js"> <!--<![endif]-->
<head>
    <title>My Artisan</title>
</head>
<body style="margin: 0; padding: 0;">
<div>
    <pre class="result" style="width: 99%;  background: #2b2b2b; color: #3F6; padding: 5px; margin:0; overflow-y: scroll;">

    </pre>
    <div style="position: relative;">
        <input class="input-command" value="" type="text" style="width: 98.2%;  background: #2b2b2b; color: #fff; padding: 5px 5px 5px 15px;" autofocus="autofocus" />
        <pre style="position: absolute; left: 5px; top: 6px; color: #aaa; margin:0; padding:0;">$ </pre>
    </div>

</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script>
    jQuery(function() {
        jQuery('.result').height(jQuery(window).height() - jQuery('.input-command').height() - 30);
        jQuery('.result').width(jQuery(window).width() - 10);
        jQuery(window).resize(function() {
            jQuery('.result').width(jQuery(window).width() - 10);
            jQuery('.result').height(jQuery(window).height() - jQuery('.input-command').height() - 30)
        });
        setTimeout(function() {
            jQuery('.input-command').focus();
        }, 1);
        var commandList = [];
        var currentIndex = 0;
        jQuery('.input-command').on('keyup', function(e) {
            switch (e.keyCode) {
                case 13:
                    if (e.keyCode == 13 && jQuery('.input-command').val()) {
						var val = $('.input-command').val().trim();
                        if (!val) {
							return;
						}
						commandList.push(val);
						currentIndex = commandList.length;
                        $('.result').append("<div style='margin-top: 12px; color: #CCC;'>$ " + val + "</div>");
                        if (jQuery('.input-command').val().toLowerCase() == 'clear') {
                            jQuery('.result').html('');
                            jQuery('.input-command').val('')
                            return false;
                        }

                        jQuery.ajax({
                            type: "POST",
                            cache: false,
                            data: {'command' : val},
                            url: document.location.href,
                            dataType: "html",
                            success: function (res) {
                                $('.result').append("<div>" + res + "</div>");
								$('.result').scrollTop(10000000);
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                $('.result').append("<div style='color: #E8BF6A;background: #402626;margin: 2px 0;'>Error sending command, Please check your connection and try again.</div>");
                            }
                        });
                        $('.input-command').val('');
                    }
                    break;
                case 38:
                    if (commandList && currentIndex > 0) {
                        currentIndex--;
                        $('.input-command').val(commandList[currentIndex]);
                    }
                    break;
                case 40:
                    if (commandList && currentIndex < commandList.length) {
                        currentIndex++;
                        $('.input-command').val(commandList[currentIndex]);
                    }
                    break;
                case 27:
                    $('.input-command').val('');
					currentIndex = commandList.length;
                    break;
            }

        });
    });
</script>
</body>
</html>