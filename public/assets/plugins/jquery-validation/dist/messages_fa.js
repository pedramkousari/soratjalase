/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: FA (Persian; فارسی)
 */
$.extend( $.validator.messages, {
    required: "تکمیل این فیلد اجباری است.",
    remote: "لطفا این فیلد را تصحیح کنید.",
    email: ".لطفا یک ایمیل صحیح وارد کنید",
    url: "لطفا آدرس صحیح وارد کنید.",
    date: "لطفا یک تاریخ صحیح وارد کنید",
    dateFA: "لطفا یک تاریخ صحیح وارد کنید",
    dateISO: "لطفا تاریخ صحیح وارد کنید (ISO).",
    number: "لطفا عدد صحیح وارد کنید.",
    digits: "لطفا تنها رقم وارد کنید",
    creditcard: "لطفا کریدیت کارت صحیح وارد کنید.",
    equalTo: "لطفا مقدار برابری وارد کنید",
    extension: "لطفا مقداری وارد کنید که ",
    maxlength: $.validator.format( "لطفا بیشتر از {0} حرف وارد نکنید." ),
    minlength: $.validator.format( "لطفا کمتر از {0} حرف وارد نکنید." ),
    rangelength: $.validator.format( "لطفا مقداری بین {0} تا {1} حرف وارد کنید." ),
    range: $.validator.format( "لطفا مقداری بین {0} تا {1} حرف وارد کنید." ),
    max: $.validator.format( "لطفا مقداری کمتر از {0} وارد کنید." ),
    min: $.validator.format( "لطفا مقداری بیشتر از {0} وارد کنید." ),
    minWords: $.validator.format( "لطفا حداقل {0} کلمه وارد کنید." ),
    maxWords: $.validator.format( "لطفا حداکثر {0} کلمه وارد کنید." )
} );

jQuery.validator.addMethod("persian", function(value, element, params) {
    tmp = value.split('');

    var keys = new Array(1711, 0, 0, 0, 0, 1608, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1705, 1572, 0, 1548,
        1567, 0, 1616, 1571, 8250, 0, 1615, 0, 0, 1570, 1577, 0, 0, 0, 1569, 1573, 0, 0, 1614, 1612, 1613, 0, 0,
        8249, 1611, 171, 0, 187, 1580, 1688, 1670, 0, 1600, 1662, 1588, 1584, 1586, 1740, 1579, 1576, 1604, 1575,
        1607, 1578, 1606, 1605, 1574, 1583, 1582, 1581, 1590, 1602, 1587, 1601, 1593, 1585, 1589, 1591, 1594, 1592 , 32);

    res = true;
    $(tmp).each(function(key,value){
        if(keys.indexOf(value.charCodeAt()) == -1){
            res = false;
            return false;
        }
    })

    return res;
}, jQuery.validator.format("فقط حروف فارسی مجاز است"));