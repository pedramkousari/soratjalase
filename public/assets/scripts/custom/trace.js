// JavaScript Document
var options = {
    show: true,
    log: false,

    mainTraceDivStyle: {
        position: 'fixed',
        direction: 'ltr',
        bottom: '0px',
        left: '0px',
        width: window.top.innerWidth-30+'px',
        margin: '0px',
        'font-family': 'tahoma',
        cursor: 'default',
        'z-index': '200000'
    },
    nodeTraceDivStyleMin: {
        float: 'left',
        border: '1px solid #999',
        width: '150px',
        height: '21px',
        margin: '2px',
        background: '#FFFEF5',
        overflow: 'hidden',
        position: ''
    },
    nodeTraceDivStyleMax: {
        position: 'fix',
        top: '4px',
        left: '4px',
        border: '1px solid #999',
        width: window.top.innerWidth-30+'px',
        margin: '2px',
        background: '#FFFEF5',
        overflow: 'hidden'
    },
    bodyTraceDivStyle: {
        'font-family': 'tahoma',
        'font-size': '12px',
        'font-weight' : 'normal',
        color: '#333',
        padding: '4px',
        overflow: 'auto',
        margin: '3px 1px 0 0'
    },
    headerTraceDivStyle: {
        height: '19px'
    },
    titleTraceDivStyle: {
        width: window.top.innerWidth+'px',
        'border-bottom': '1px solid #999',
        height: '19px',
        background: '#3366CC',
        color: '#FFF',
        cursor: 'default',
        'font-size': '13px',
        padding: '2px 0 0 0'
    },
    resizeTraceDivStyle: {
        float: 'left',
        width: '15px',
        border: '1px solid #999',
        height: '15px',
        'background-color': '#000000',
        margin: '2px',
        color: '#EEE',
        'font-weight': 'bold',
        overflow: 'hidden',
        'text-align': 'center',
        cursor: 'default',
        opacity: '0.5'
    },
    closeTraceDivStyle: {
        float: 'left',
        width: '15px',
        border: '1px solid #999',
        height: '15px',
        'background-color': '#DD3333',
        margin: '2px 0 2px 2px',
        color: '#EEE',
        overflow: 'hidden',
        'text-align': 'center',
        cursor: 'default',
        opacity: '0.5'
    }
}

TRACE_CLASS = function(list) {

    this.list =  list;
    this.options = options;

    this.mainTraceDiv = window.top.document.getElementById('mainTraceDiv');
    if(!this.mainTraceDiv) {
        this.mainTraceDiv = jQuery(document.createElement('div'));
        this.mainTraceDiv.id = 'mainTraceDiv';
        jQuery(document.body).append(this.mainTraceDiv);
        this.mainTraceDiv.css(this.options.mainTraceDivStyle);
    }
}

TRACE_CLASS.prototype = {
    loadCookie: function() {
        return ;
        var ar = [];
        var test;

        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1, c.length);
            if (test = c.match(/^\s*(__trace_(\d+)_\d+)=(.*)$/)) {
                ar[test[2]] = ar[test[2]] ? ar[test[2]] + test[3] : test[3];
                document.cookie = test[1]+"=; expires=Sun, 13 Jan 1980 12:00:00 GMT; path=/";
            }
        }
        if(ar.length) {
            for(i=0; i<ar.length; i++) {
                //console.log(unescape(ar[i]));
                console.log(unserialize(unescape(ar[i])));
                this.list.push(unserialize(unescape(ar[i])));
            }
        }
    },
    show : function() {
        var self = this;
        for(var i=0; i<this.list.length; i++) {
            if(!this.list[i]) continue;
            if(typeof(console)!='undefined' && this.options.log)
                console.log(this.list[i].title+':\n'+this.list[i].value);
            if(!this.options.show)
                continue;
            var nodeTraceDiv = document.createElement('div');
            var headerTraceDiv = document.createElement('div');
            var titleTraceDiv = document.createElement('div');
            var resizeTraceDiv = document.createElement('div');
            var closeTraceDiv = document.createElement('div');
            var bodyTraceDiv = document.createElement('div');
            jQuery(nodeTraceDiv).css(this.options.nodeTraceDivStyleMin);
            jQuery(headerTraceDiv).css(this.options.headerTraceDivStyle);
            jQuery(bodyTraceDiv).css(this.options.bodyTraceDivStyle);
            jQuery(closeTraceDiv).css(this.options.closeTraceDivStyle);
            jQuery(resizeTraceDiv).css(this.options.resizeTraceDivStyle);
            jQuery(titleTraceDiv).css(this.options.titleTraceDivStyle);

            this.mainTraceDiv.append(nodeTraceDiv);
            jQuery(nodeTraceDiv).append(headerTraceDiv);
            jQuery(nodeTraceDiv).append(bodyTraceDiv);
            jQuery(headerTraceDiv).append(closeTraceDiv);
            jQuery(headerTraceDiv).append(resizeTraceDiv);
            jQuery(headerTraceDiv).append(titleTraceDiv);


            jQuery(nodeTraceDiv).addClass('nodeTraceDiv');
            jQuery(headerTraceDiv).addClass('headerTraceDiv');
            jQuery(bodyTraceDiv).addClass('bodyTraceDiv');
            jQuery(closeTraceDiv).addClass('closeTraceDiv');
            jQuery(closeTraceDiv).css('float', 'left');
            jQuery(resizeTraceDiv).addClass('resizeTraceDiv');
            jQuery(resizeTraceDiv).css('float', 'left');
            jQuery(titleTraceDiv).addClass('titleTraceDiv');

            jQuery(titleTraceDiv).html(this.list[i].title);
            jQuery(bodyTraceDiv).html('<pre style="margin-top:2px">'+this.list[i].value+'</pre>');

            jQuery(resizeTraceDiv).html('<div style="cursor: pointer; margin: -3px 0 0 0px;">+</div>');
            jQuery(closeTraceDiv).html('<div style="cursor: pointer; margin: -4px 0 0 -2px;">x</div>');

            jQuery(closeTraceDiv).click(function(){jQuery(this).parents('.nodeTraceDiv').remove()});
            jQuery(resizeTraceDiv).click(function (e) { self.restore(e, self); });

            jQuery(closeTraceDiv).on('mouseenter', this.btnEnter);
            jQuery(resizeTraceDiv).on('mouseenter', this.btnEnter);

            jQuery(closeTraceDiv).on('mouseleave', this.btnLeave);
            jQuery(resizeTraceDiv).on('mouseleave', this.btnLeave);
        }
        this.list = [];
    },
    restore : function(e, self) {
        //var e = new Event(e);
        var me = jQuery(e.target);
        var nodeTraceDiv = me.parents('.nodeTraceDiv');
        var bodyTraceDiv = nodeTraceDiv.find('.bodyTraceDiv');
        me.parent().removeClass('maxed_resizeTraceDiv');

        if(me.html() == '+') { //maximize
            me.html('-');
            self.minimize(self.mainTraceDiv.find('.maxed_resizeTraceDiv').first());
            me.parent().addClass('maxed_resizeTraceDiv');
            nodeTraceDiv.css(self.options.nodeTraceDivStyleMax);
            nodeTraceDiv.css('height', window.top.innerHeight - self.mainTraceDiv.height() - 30 + 'px');
            bodyTraceDiv.css('height', nodeTraceDiv.height() - 32 + 'px');
        } else { //minimize
            me.html('+');
            nodeTraceDiv.css(self.options.nodeTraceDivStyleMin);
        }
    },
    minimize : function(me) {
        var nodeTraceDiv = me.parents('.nodeTraceDiv');
        me.parent().removeClass('maxed_resizeTraceDiv');
        me.innerHTML = '+';
        nodeTraceDiv.css(this.options.nodeTraceDivStyleMin);
    },
    btnEnter : function(e){
        var e = new Event(e);
        var me = jQuery(e.target);
//        while(me.first())
//            me = me.first();
        me = me.parent();
        me.fadeTo('slow', 1);
    },
    btnLeave :  function(e){
        var e = new Event(e);
        var me = jQuery(e.target);
//        while(me.first())
//            me = me.first();
        me = me.parent();
        me.fadeTo('slow', 0.5);
    }

}
function traceLoader()
{
    if(window.traceScript) {
        eval(window.traceScript);
        window.traceScript = null;
    }
    if(window.traceLoaderTO) clearInterval(window.traceLoaderTO);
    window.traceLoaderTO = setTimeout('traceLoader();', 500);
}

traceLoader();
