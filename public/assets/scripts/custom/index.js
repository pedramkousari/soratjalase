var Index = function () {

	handleMessage = function(res, options) {
		var options = $.extend({
			'alert': $('.alert'),
			'class': 'alert-danger'
		}, options);

		try {
			var msgs = $.parseJSON(res);
			options.alert
				.removeClass('alert-success')
				.removeClass('alert-danger')
				.addClass(options.class)
				.empty()
				.append('<ul></ul>');

			$.each(msgs, function(km, vm) {
				options.alert.find('ul').append('<li>' + vm + '</li>');
			});
			options.alert.fadeIn(function() {
				$(this).removeClass('hidden');
			});
		} catch (e) {}
	};

	isValidIranianNationalCode = function (input) {
		if (!/^\d{10}$/.test(input))
			return false;

		var check = parseInt(input[9]);
		var sum = [0, 1, 2, 3, 4, 5, 6, 7, 8]
				.map(function (x) { return parseInt(input[x]) * (10 - x); })
				.reduce(function (x, y) { return x + y; }) % 11;

		return sum < 2 && check == sum || sum >= 2 && check + sum == 11;
	}


	

	return {

		//main function
		init: function () {
		},

		send: function (frm, options) {
			var frm = $(frm);
			if (!frm.length) return false;
			var options = $.extend({
				parent: frm.closest('.register-div'),
				ajax: {
					overlay: {
						active: true,
						resultObject: options.parent
					},
					noResult: true
				},
				captcha: frm
			}, options,true);
			if ($.type(options.redirect) != 'undefined') {
				options.ajax.overlay.remove = false;
			}

			if (frm.valid()) {
				frm.off('submit');
				options.parent.find('.alert').fadeOut(function() {
					$(this).addClass('hidden');
				});
				App.ajaxRequest(frm, options.ajax, function(res) {
					if ($.type(options.redirect) != 'undefined') {
						window.location.href = options.redirect;
					} else {
						handleMessage(res, {'alert': options.parent.find('.alert'), 'class': 'alert-success'});
						frm[0].reset();
						App.reCaptcha(options.captcha);
					}
				}, function(res) {
					if (res.status == 422) {
						handleMessage(res.responseText, {'alert': options.parent.find('.alert'), 'class': 'alert-danger'});
						App.reCaptcha(options.captcha);
					}
				});
			}

			return false;
		},

		registerUser: function(form) {
			var form = $(form);
			if (!form.length) return false;

			form.off('submit').submit(function(e) {
				e.preventDefault();
				var frm = $(this);
				var options = {
					'overlay': {
						'active': true,
						'resultObject': frm.closest('div')
					}
				};
				if (frm.valid()) {
					App.ajaxRequest(frm, options, function(res) {
						if (typeof res == 'object' && res.type != undefined) {
							var type = (res.type == 'error' ? 'danger' : res.type);
							frm.find('.alert').addClass('alert-' + type).find('span').html(res.message);
							frm.find('.alert').fadeIn();
							if (res.type == 'success') {
								$('[data-advancedselect]').select2('val', '');
								form.find('.car-row').each(function(key) {
									if (key > 0) $(this).remove();
								});
								App.normalizeRows('car-row');
								form.find('.form-group').removeClass('has-error').removeClass('has-success')
								form[0].reset();
							}
						}
					});
				}
			});
		},

		checkNationalCode : function(val){
			return isValidIranianNationalCode(val)
		},

		soratjalase : function(){
			jQuery.validator.addClassRules({
				zip: {
					required: true,
					digits: true,
					minlength: 5,
					maxlength: 5
				},
				national_code_company: {
					required: true,
					digits: true,
					minlength: 11,
					maxlength: 11,
				},
				postalcode: {
					required: true,
					digits: true,
					minlength: 10,
					maxlength: 10,
				}
			});
		},

		checkManager : function(){
			var chief_cnt=1;
			var supervisor_cnt=2;

			var cnt1 = 0;
			var cnt2 = 0;
			$('.xxrecord_user-list select').each(function(key,obj){
				if(obj.value=='chief'){
					cnt1++;
				}
				else if(obj.value=='supervisor'){
					cnt2++;
				}
			});

			if(cnt1>chief_cnt){
				if(bootbox != undefined){
					bootbox.alert('افراد شرکت کننده در جلسه حده اکثر یک رءیس بیشتر نمیتواند داشته باشد');
				}
				return false;
			}

			if(cnt1<chief_cnt){
				if(bootbox != undefined){
					bootbox.alert('یک نفر را بعنوان رئیس جلسه انتخاب کنید');
				}
				return false;
			}

			if(cnt2<supervisor_cnt){
				if(bootbox != undefined){
					bootbox.alert('تعداد ناظرین در جلسه باید بیشتر از یک نفر باشد');
				}
				return false;
			}

			return true;
		},

		resetError: function(newObject){
			frm_grp = $(newObject).find('.form-group');

			if(frm_grp.hasClass('has-error') || frm_grp.hasClass('has-success')){
				frm_grp.removeClass('has-error');
				frm_grp.removeClass('has-success');
				if(frm_grp.find('[aria-describedby]')>1){
					frm_grp.find('[aria-describedby]').each(function(obj){
						$(obj).removeAttr('aria-describedby');
					});
				}else{
					frm_grp.find('[aria-describedby]').removeAttr('aria-describedby');
				}
				frm_grp.find('span').remove();
			}
		}

	};

}();

$(function() {
	// constants
	var SHOW_CLASS = 'show',
		HIDE_CLASS = 'hide',
		ACTIVE_CLASS = 'active';

	$( '.tabs' ).on( 'click', 'li a', function(e){
		e.preventDefault();
		var $tab = $( this ),
			href = $tab.attr( 'href' );

		$( '.active' ).removeClass( ACTIVE_CLASS );
		$tab.addClass( ACTIVE_CLASS );

		$( '.show' )
			.removeClass( SHOW_CLASS )
			.addClass( HIDE_CLASS )
			.hide();

		$(href)
			.removeClass( HIDE_CLASS )
			.addClass( SHOW_CLASS )
			.hide()
			.fadeIn( 550 );
	});




});