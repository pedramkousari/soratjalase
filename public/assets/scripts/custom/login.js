var Login = function () {
	var handleLogin = function() {
		$('.login-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                username: {
	                    required: true
	                },
	                password: {
	                    required: true
	                },
	                remember: {
	                    required: false
	                },
	                captcha: {
	                	required: true
	                }
	            },

	            messages: {
	                username: {
	                    required: App.trans("username is required")
	                },
	                password: {
	                    required: App.trans("password is required")
	                },
	                captcha: {
	                    required: App.trans("captcha is required")
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	                $('.alert-danger', $('.login-form')).show();
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	                form.submit(); // form validation success, call ajax form submit
	            }
	        });

	        $('.login-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.login-form').validate().form()) {
	                    $('.login-form').submit(); //form validation success, call ajax form submit
	                }
	                return false;
	            }
	        });
	}

	var handleForgetPassword = function () {
		$('.forget-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                email: {
	                    required: true,
	                    //email: true
	                }
	            },

	            messages: {
	                email: {
	                    required: App.trans("email is required")
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	            	//form.submit();
	            	return false;
	            }
	        });

	        $('.forget-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.forget-form').validate().form()) {
	                    $('.forget-form button.submit').trigger('click');
	                }
	                return false;
	            }
	        });

	        jQuery('#forget-password').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.forget-form').show();
	        });

	        jQuery('#back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.forget-form').hide();
	        });

	}

	var handleResetPassword = function() {
        $('.reset-password-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                	minlength: 6,
                    required: true
                },
                password_confirmation: {
                	minlength: 6,
                    required: true,
                    equalTo: "#password"
                },
            },
           

            messages: {
                email: {
                    required: App.trans("email is required")
                },
                password: {
                    required: App.trans("password is required")
                },
                password_confirmation: {
                    equalTo: App.trans('password and confirmation do not match')
                },
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.reset-password-form')).show();
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function (form) {
                form.submit(); // form validation success, call ajax form submit
            }
        });

		$('.reset-password-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.reset-password-form').validate().form()) {
                    $('.reset-password-form').submit();
                }
                return false;
            }
        });
	}

	var handleRegister = function () {

		function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }


		$("#select2_sample4").select2({
		  	placeholder: '<i class="fa fa-map-marker"></i>&nbsp;Select a Country',
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });


		$('#select2_sample4').change(function () {
            $('.register-form').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });



        $('.register-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                
	                fullname: {
	                    required: true
	                },
	                email: {
	                    required: true,
	                    email: true
	                },
	                address: {
	                    required: true
	                },
	                city: {
	                    required: true
	                },
	                country: {
	                    required: true
	                },

	                username: {
	                    required: true
	                },
	                password: {
	                    required: true
	                },
	                rpassword: {
	                    equalTo: "#register_password"
	                },

	                tnc: {
	                    required: true
	                }
	            },

	            messages: { // custom messages for radio buttons and checkboxes
	                tnc: {
	                    required: App.trans("please accept TNC first")
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
	                    error.insertAfter($('#register_tnc_error'));
	                } else if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

			$('.register-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.register-form').validate().form()) {
	                    $('.register-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#register-btn').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.register-form').show();
	        });

	        jQuery('#register-back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.register-form').hide();
	        });
	}

    
    return {
        //main function to initiate the module
        init: function () {
        	
			jQuery.extend(jQuery.validator.messages, {
			    required: App.trans("this field is required"),
			    remote: App.trans("please fix this field"),
			    email: App.trans("please enter a valid email address"),
			    url: App.trans("please enter a valid URL"),
			    date: App.trans("please enter a valid date"),
			    dateISO: App.trans("please enter a valid date (ISO)"),
			    number: App.trans("please enter a valid number"),
			    digits: App.trans("please enter only digits"),
			    creditcard: App.trans("please enter a valid credit card number"),
			    equalTo: App.trans("please enter the same value again"),
			    accept: App.trans("please enter a value with a valid extension"),
			    maxlength: jQuery.validator.format("please enter no more than {0} characters"),
			    minlength: jQuery.validator.format("please enter at least {0} characters"),
			    rangelength: jQuery.validator.format("please enter a value between {0} and {1} characters long"),
			    range: jQuery.validator.format("please enter a value between {0} and {1}"),
			    max: jQuery.validator.format("please enter a value less than or equal to {0}"),
			    min: jQuery.validator.format("please enter a value greater than or equal to {0}")
			});
			if (lang == 'fa') {
				jQuery.extend(jQuery.validator.messages, {
				    maxlength: jQuery.validator.format("لطفا بیشتر از {0} کاراکتر وارد کنید."),
				    minlength: jQuery.validator.format("لطفا حداقل {0} کاراکتر وارد کنید."),
				    rangelength: jQuery.validator.format("لطفا یک مقدار بین {0} و {1} کاراکتر وارد کنید."),
				    range: jQuery.validator.format("لطفا یک مقدار بین {0} و {1} وارد کنید."),
				    max: jQuery.validator.format("لطفا یک مقدار کمتر یا مساوی {0} وارد کنید."),
				    min: jQuery.validator.format("لطفا یک مقدار بیشتر یا مساوی {0} وارد کنید.")
				});
			}

            handleLogin();
            handleForgetPassword();
            handleRegister();        
            handleResetPassword();        
	       
        }

    };

}();