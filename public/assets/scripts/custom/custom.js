/**
 Custom module for you to write your own javascript functions
 **/

var Custom = function () {

    handleMessage = function(res, options) {
        var options = $.extend({
            'alert': $('.alert'),
            'class': 'alert-danger'
        }, options);

        try {
            var msgs = $.parseJSON(res);
            options.alert
                .removeClass('alert-success')
                .removeClass('alert-danger')
                .addClass(options.class)
                .empty()
                .append('<ul></ul>');

            $.each(msgs, function(km, vm) {
                options.alert.find('ul').append('<li>' + vm + '</li>');
            });
            options.alert.fadeIn(function() {
                $(this).removeClass('display-none');
            });
        } catch (e) {}
    };

    // private functions & variables
    var split = function (val, regExp) {
        var regExp = regExp ? regExp : '\/\s*';
        var re = new RegExp(regExp, 'g');
        return val.split(re);
    };

    var multiSelect = function (obj, options) {   
         
        if (typeof obj.multiselect == 'function') {  
            var settings = {  
                allSelectedText: App.trans('all') + '...',
                selectAllText: App.trans('select') + ' ' + App.trans('all') + '...',
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: App.trans('search') + '...',
                includeSelectAllOption: true,
                enableClickableOptGroups: true,
                maxHeight: 250,
                templates: {
                    filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-icon"><i class="fa fa-search" style="z-index: 50;"></i><input class="form-control multiselect-search" type="text"></span></div></li>',
                    filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><i class="fa fa-remove"></i></button></span>'
                },
                buttonText: function(options, select) {
                    if (options.length === 0) {
                        return App.trans('select') + '...';
                    } else if (options.length > 1) {
                        returnText = App.convertDigit(options.length, lang) + ' ' + App.trans('selected');
                        if (options.length == $(select).find('option').length)
                            returnText = App.trans('all') + '...';
                        return returnText;
                    } else {
                        var labels = [];
                        options.each(function() {
                            if ($(this).attr('label') !== undefined) {
                                labels.push($(this).attr('label'));
                            }
                            else {
                                labels.push($(this).html());
                            }
                        });
                        return labels.join(', ') + '';
                    }
                }
            };

            options = $.extend(options, settings);
            obj.multiselect(options);
            obj.multiselect('rebuild');
            $('.multiselect-container input:checkbox').addClass('no-checker');
        } else {
            setTimeout(function() { multiSelect(obj) }, 10);
        }
    };
    
    // public functions
    return {

        //main function
        init: function () {
        },


        send: function (frm, options) {
            var frm = $(frm);
            if (!frm.length) return false;
            var parent = ($.type(options) == 'object' && $.type(options.parent) != undefined) ? options.parent : frm.closest('.register-div');
            var options = $.extend({
                parent: parent,
                ajax: {
                    overlay: {
                        active: true,
                        resultObject: parent
                    },
                    noResult: true
                },
                captcha: frm
            }, options);

            if ($.type(options.redirect) != 'undefined' || ($.type(options.login) != 'undefined' && options.login)) {
                options.ajax.overlay.remove = false;
            }

            if (frm.valid()) {
                frm.off('submit');
                options.parent.find('.alert').fadeOut(function() {
                    $(this).addClass('display-none');
                });
                App.ajaxRequest(frm, options.ajax, function(res) {
                    if ($.type(options.redirect) != 'undefined') {
                        window.location.href = options.redirect;
                    } else {
                        try {
                            var res = $.parseJSON(res);
                            window.location.href = '/' + res.group;
                        } catch(e) {}

                        handleMessage(res, {'alert': options.parent.find('.alert'), 'class': 'alert-success'});
                        frm[0].reset();
                        App.reCaptcha(options.captcha);
                    }
                }, function(res) {
                    if (res.status == 422) {
                        handleMessage(res.responseText, {'alert': options.parent.find('.alert'), 'class': 'alert-danger'});
                        App.reCaptcha(options.captcha);
                    }
                });
            }

            return false;
        },

        //## create multiple select with checkbox and filter
        multiSelect: function(object, options) { 
            var obj = $(object);   
            if (!obj.length && typeof object == 'string') {
                obj = $('.' + object);
                if (!obj.length) {
                    obj = $('#' + object);
                    if (!obj.length) return false;
                }
            }   

            multiSelect(obj, options);     
        },   
    };
}();

/***
 Usage
 ***/
Custom.init();
//Custom.doSomeStuff();
