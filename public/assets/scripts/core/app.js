/**
    Core script to handle the entire theme and core functions
**/
var multipleUpload;

//## object to serialize string
$.fn.serializeObject = function(){

    var self = this,
        json = {},
        push_counters = {},
        patterns = {
            "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_\.]+)\])*$/,
            "key":      /[a-zA-Z0-9_\.]+|(?=\[\])/g,
            "push":     /^$/,
            "fixed":    /^\d+$/,
            "named":    /^[a-zA-Z0-9_\.]+$/
        };


    this.build = function(base, key, value){
        base[key] = value;
        return base;
    };

    this.push_counter = function(key){
        if(push_counters[key] === undefined){
            push_counters[key] = 0;
        }
        return push_counters[key]++;
    };

    $.each($(this).serializeArray(), function(){

        // skip invalid keys
        if(!patterns.validate.test(this.name)){
            return;
        }

        var k,
            keys = this.name.match(patterns.key),
            merge = this.value,
            reverse_key = this.name;

        while((k = keys.pop()) !== undefined){

            // adjust reverse_key
            reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

            // push
            if(k.match(patterns.push)){
                merge = self.build([], self.push_counter(reverse_key), merge);
            }

            // fixed
            else if(k.match(patterns.fixed)){
                merge = self.build([], k, merge);
            }

            // named
            else if(k.match(patterns.named)){
                merge = self.build({}, k, merge);
            }
        }

        json = $.extend(true, json, merge);
    });

    return json;
};

//## print div contents
$.fn.printContents = function () {
    return this.each(function () {
        var container = $(this);

        var hidden_IFrame = $('<iframe></iframe>').attr({
            width: '1px',
            height: '1px',
            display: 'none'
        }).appendTo(container);

        var myIframe = hidden_IFrame.get(0);

        var script_tag = myIframe.contentWindow.document.createElement("script");
        script_tag.type = "text/javascript";
        script = myIframe.contentWindow.document.createTextNode('function Print(){ window.print(); }');
        script_tag.appendChild(script);

        myIframe.contentWindow.document.body.innerHTML = container.html();
        myIframe.contentWindow.document.body.appendChild(script_tag);

        myIframe.contentWindow.Print();
        hidden_IFrame.remove();

    });
};

//## string hash code
String.prototype.hashCode = function() {
  var hash = 0, i, chr, len;
  if (this.length == 0) return hash;
  for (i = 0, len = this.length; i < len; i++) {
    chr   = this.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

//## string hash code
String.prototype.toHtml = function() {
 return this.replace(/&amp;/g, '&')
    .replace(/&lt;/g, '<')
    .replace(/&gt;/g, '>')
    .replace(/&quot;/g, '"')
    .replace(/&#039;/g, "'");
};

//## nl2br && br2nl
jQuery.nl2br = function(varTest){
    return varTest.replace(/(\r\n|\n\r|\r|\n)/g, "<br>");
};
jQuery.br2nl = function(varTest){
    return varTest.replace(/<br>/g, "\r");
};

var App = function () {

    // IE mode
    var isRTL = window.dir == 'rtl' ? true : false;
    var isLocal = environment == 'local' ? true : false;
    var isIE8 = false;
    var isIE9 = false;
    var isIE10 = false;
    var listCurrentPage = 1;
    var msgCountRequest;

    var sidebarWidth = 225;
    var sidebarCollapsedWidth = 35;

    var responsiveHandlers = [];

    // theme layout color set
    var layoutColorCodes = {
        'blue': '#4b8df8',
        'red': '#e02222',
        'green': '#35aa47',
        'purple': '#852b99',
        'grey': '#555555',
        'light-grey': '#fafafa',
        'yellow': '#ffb848'
    };

    // To get the correct viewport width based on  http://andylangton.co.uk/articles/javascript/get-viewport-size-javascript/
    var _getViewPort = function () {
        var e = window, a = 'inner';
        if (!('innerWidth' in window)) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        return {
            width: e[a + 'Width'],
            height: e[a + 'Height']
        }
    }

    // initializes main settings
    var handleInit = function () {

        if ($('body').css('direction') === 'rtl') {
            isRTL = true;
        }

        isIE8 = !! navigator.userAgent.match(/MSIE 8.0/);
        isIE9 = !! navigator.userAgent.match(/MSIE 9.0/);
        isIE10 = !! navigator.userAgent.match(/MSIE 10.0/);

        if (isIE10) {
            jQuery('html').addClass('ie10'); // detect IE10 version
        }
        
        if (isIE10 || isIE9 || isIE8) {
            jQuery('html').addClass('ie'); // detect IE10 version
        }

        /*
          Virtual keyboards:
          Also, note that if you're using inputs in your modal – iOS has a rendering bug which doesn't 
          update the position of fixed elements when the virtual keyboard is triggered  
        */
        var deviceAgent = navigator.userAgent.toLowerCase();
        if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
            $(document).on('focus', 'input, textarea', function () {
                $('.page-header').hide();
                $('.page-footer').hide();
            });
            $(document).on('blur', 'input, textarea', function () {
                $('.page-header').show();
                $('.page-footer').show();
            });
        }

        //## Add by Salah
        if ($('.search-box').length && $('.search-icon').length) {
            $('.search-box').hide();
            $('.search-icon').click(function(e) {
                e.preventDefault();
                if (!$('.search-box').hasClass('active')) {
                    $('.search-box').addClass('active').slideDown();
                } else {
                    $('.search-box').removeClass('active').slideUp();
                }
            });
        }

        $('*[data-advancedselect]').each(function() {
            App.setSelect2($(this));
        });
    }

    var handleSidebarState = function () {
        // remove sidebar toggler if window width smaller than 992(for tablet and phone mode)
        var viewport = _getViewPort();
        if (viewport.width < 992) {
            $('body').removeClass("page-sidebar-closed");
        }
    }

    // runs callback functions set by App.addResponsiveHandler().
    var runResponsiveHandlers = function () {
        // reinitialize other subscribed elements
        for (var i = 0; i < responsiveHandlers.length; i++) {
            var each = responsiveHandlers[i];
            each.call();
        }
    }

    // reinitialize the laypot on window resize
    var handleResponsive = function () {
        handleSidebarState();
        handleSidebarAndContentHeight();
        handleFixedSidebar();
        runResponsiveHandlers();
    }

    // initialize the layout on page load
    var handleResponsiveOnInit = function () {
        handleSidebarState();
        handleSidebarAndContentHeight();
    }

    // handle the layout reinitialization on window resize
    var handleResponsiveOnResize = function () {
        var resize;
        if (isIE8) {
            var currheight;
            $(window).resize(function () {
                if (currheight == document.documentElement.clientHeight) {
                    return; //quite event since only body resized not window.
                }
                if (resize) {
                    clearTimeout(resize);
                }
                resize = setTimeout(function () {
                    handleResponsive();
                }, 50); // wait 50ms until window resize finishes.                
                currheight = document.documentElement.clientHeight; // store last body client height
            });
        } else {
            $(window).resize(function () {
                if (resize) {
                    clearTimeout(resize);
                }
                resize = setTimeout(function () {
                    handleResponsive();
                }, 50); // wait 50ms until window resize finishes.
            });
        }
    }

    //* BEGIN:CORE HANDLERS *//
    // this function handles responsive layout on screen size resize or mobile device rotate.

    // Set proper height for sidebar and content. The content and sidebar height must be synced always.
    var handleSidebarAndContentHeight = function () {
        var content = $('.page-content');
        var sidebar = $('.page-sidebar');
        var body = $('body');
        var height;

        if (body.hasClass("page-footer-fixed") === true && body.hasClass("page-sidebar-fixed") === false) {
            var available_height = $(window).height() - $('.footer').outerHeight() - $('.header').outerHeight();
            if (content.height() < available_height) {
                content.attr('style', 'min-height:' + available_height + 'px !important');
            }
        } else {
            if (body.hasClass('page-sidebar-fixed')) {
                height = _calculateFixedSidebarViewportHeight();
            } else {
                height = sidebar.height() + 20;
                var headerHeight = $('.header').outerHeight();
                var footerHeight = $('.footer').outerHeight();                
                if ($(window).width() > 1024 && (height + headerHeight + footerHeight)  < $(window).height()) {
                    height = $(window).height() - headerHeight - footerHeight;
                }
            }            
            if (height >= content.height()) {
                content.attr('style', 'min-height:' + height + 'px !important');
            }
        }
    }

    // Handle sidebar menu
    var handleSidebarMenu = function () {
        $('.page-sidebar').find('.active').parents('li').addClass('active');
        jQuery('.page-sidebar').on('click', 'li > a', function (e) {
            if ($(this).next().hasClass('sub-menu') == false) {
                if ($('.btn-navbar').hasClass('collapsed') == false) {
                    $('.btn-navbar').click();
                }
                return;
            }

            if ($(this).next().hasClass('sub-menu always-open')) {
                return;
            }

            var parent = $(this).parent().parent();
            var the = $(this);
            var menu = $('.page-sidebar-menu');
            var sub = jQuery(this).next();

            var autoScroll = menu.data("auto-scroll") ? menu.data("auto-scroll") : true;
            var slideSpeed = menu.data("slide-speed") ? parseInt(menu.data("slide-speed")) : 200;

            parent.children('li.open').children('a').children('.arrow').removeClass('open');
            parent.children('li.open').children('.sub-menu:not(.always-open)').slideUp(200);
            parent.children('li.open').removeClass('open');
            
            var slideOffeset = -200;

            if (sub.is(":visible")) {
                jQuery('.arrow', jQuery(this)).removeClass("open");
                jQuery(this).parent().removeClass("open");
                sub.slideUp(slideSpeed, function () {
                    if (autoScroll == true && $('body').hasClass('page-sidebar-closed') == false) {
                        if ($('body').hasClass('page-sidebar-fixed')) {
                            menu.slimScroll({'scrollTo': (the.position()).top});
                        } else {
                            App.scrollTo(the, slideOffeset);
                        }
                    }
                    handleSidebarAndContentHeight();
                });
            } else {
                jQuery('.arrow', jQuery(this)).addClass("open");
                jQuery(this).parent().addClass("open");
                sub.slideDown(slideSpeed, function () {
                    if (autoScroll == true && $('body').hasClass('page-sidebar-closed') == false) {
                        if ($('body').hasClass('page-sidebar-fixed')) {
                            menu.slimScroll({'scrollTo': (the.position()).top});
                        } else {
                            App.scrollTo(the, slideOffeset);
                        }
                    }
                    handleSidebarAndContentHeight();
                });
            }

            e.preventDefault();
        });

        // handle ajax links within sidebar menu
        jQuery('.page-sidebar').on('click', ' li > a.ajaxify', function (e) {
            e.preventDefault();
            App.scrollTop();

            var url = $(this).attr("href");
            var menuContainer = jQuery('.page-sidebar ul');
            var pageContent = $('.page-content');
            var pageContentBody = $('.page-content .page-content-body');

            menuContainer.children('li.active').removeClass('active');
            menuContainer.children('arrow.open').removeClass('open');

            $(this).parents('li').each(function () {
                $(this).addClass('active');
                $(this).children('a > span.arrow').addClass('open');
            });
            $(this).parents('li').addClass('active');

            App.startPageLoading();
            
            if ($(window).width() <= 991 && $('.page-sidebar').hasClass("in")) {
                $('.navbar-toggle').click();
            }

            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: false,
                url: url,
                dataType: "html",
                success: function (res) {
                    App.stopPageLoading();
                    pageContentBody.html(res);
                    App.fixContentHeight(); // fix content height
                    App.initAjax(); // initialize core stuff
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    pageContentBody.html('<h4>Could not load the requested content.</h4>');
                    App.stopPageLoading();
                }
            });
        });

        // handle ajax link within main content
        jQuery('.page-content').on('click', '.ajaxify', function (e) {
            e.preventDefault();
            App.scrollTop();

            var url = $(this).attr("href");
            var pageContent = $('.page-content');
            var pageContentBody = $('.page-content .page-content-body');

            App.startPageLoading();
            
            if ($(window).width() <= 991 && $('.page-sidebar').hasClass("in")) {
                $('.navbar-toggle').click();
            }

            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: false,
                url: url,
                dataType: "html",
                success: function (res) {
                    App.stopPageLoading();
                    pageContentBody.html(res);
                    App.fixContentHeight(); // fix content height
                    App.initAjax(); // initialize core stuff
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    pageContentBody.html('<h4>Could not load the requested content.</h4>');
                    App.stopPageLoading();
                }
            });
        });
    }

    // Helper function to calculate sidebar height for fixed sidebar layout.
    var _calculateFixedSidebarViewportHeight = function () {
        var sidebarHeight = $(window).height() - $('.header').height() + 1;
        if ($('body').hasClass("page-footer-fixed")) {
            sidebarHeight = sidebarHeight - $('.footer').outerHeight();
        }

        return sidebarHeight;
    }

    // Handles fixed sidebar
    var handleFixedSidebar = function () {
        var menu = $('.page-sidebar-menu');

        if (menu.parent('.slimScrollDiv').size() === 1) { // destroy existing instance before updating the height
            menu.slimScroll({
                destroy: true
            });
            menu.removeAttr('style');
            $('.page-sidebar').removeAttr('style');
        }

        if ($('.page-sidebar-fixed').size() === 0) {
            handleSidebarAndContentHeight();
            return;
        }

        var viewport = _getViewPort();
        if (viewport.width >= 992) {
            var sidebarHeight = _calculateFixedSidebarViewportHeight();

            menu.slimScroll({
                size: '7px',
                color: '#a1b2bd',
                opacity: .3,
                position: isRTL ? 'left' : 'right',
                height: sidebarHeight,
                allowPageScroll: false,
                disableFadeOut: false
            });
            handleSidebarAndContentHeight();
        }
    }

    // Handles the sidebar menu hover effect for fixed sidebar.
    var handleFixedSidebarHoverable = function () {
        if ($('body').hasClass('page-sidebar-fixed') === false) {
            return;
        }

        $('.page-sidebar').off('mouseenter').on('mouseenter', function () {
            var body = $('body');

            if ((body.hasClass('page-sidebar-closed') === false || body.hasClass('page-sidebar-fixed') === false) || $(this).hasClass('page-sidebar-hovering')) {
                return;
            }

            body.removeClass('page-sidebar-closed').addClass('page-sidebar-hover-on');

            if (body.hasClass("page-sidebar-reversed")) {
                $(this).width(sidebarWidth);
            } else {
                $(this).addClass('page-sidebar-hovering');
                $(this).animate({
                    width: sidebarWidth
                }, 400, '', function () {
                    $(this).removeClass('page-sidebar-hovering');
                });
            }
        });

        $('.page-sidebar').off('mouseleave').on('mouseleave', function () {
            var body = $('body');

            if ((body.hasClass('page-sidebar-hover-on') === false || body.hasClass('page-sidebar-fixed') === false) || $(this).hasClass('page-sidebar-hovering')) {
                return;
            }

            if (body.hasClass("page-sidebar-reversed")) {
                $('body').addClass('page-sidebar-closed').removeClass('page-sidebar-hover-on');
                $(this).width(sidebarCollapsedWidth);
            } else {
                $(this).addClass('page-sidebar-hovering');
                $(this).animate({
                    width: sidebarCollapsedWidth
                }, 400, '', function () {
                    $('body').addClass('page-sidebar-closed').removeClass('page-sidebar-hover-on');
                    $(this).removeClass('page-sidebar-hovering');
                });
            }
        });
    }

    // Handles sidebar toggler to close/hide the sidebar.
    var handleSidebarToggler = function () {
        var viewport = _getViewPort();
        if ($.cookie && $.cookie('sidebar_closed') === '1' && viewport.width >= 992) {
            $('body').addClass('page-sidebar-closed');
        }

        // handle sidebar show/hide
        $('.page-sidebar, .header').on('click', '.sidebar-toggler', function (e) {
            var body = $('body');
            var sidebar = $('.page-sidebar');

            if ((body.hasClass("page-sidebar-hover-on") && body.hasClass('page-sidebar-fixed')) || sidebar.hasClass('page-sidebar-hovering')) {
                body.removeClass('page-sidebar-hover-on');
                sidebar.css('width', '').hide().show();
                handleSidebarAndContentHeight(); //fix content & sidebar height
                if ($.cookie) {
                    $.cookie('sidebar_closed', '0');
                }
                e.stopPropagation();
                runResponsiveHandlers();
                return;
            }

            $(".sidebar-search", sidebar).removeClass("open");

            if (body.hasClass("page-sidebar-closed")) {
                body.removeClass("page-sidebar-closed");
                if (body.hasClass('page-sidebar-fixed')) {
                    sidebar.css('width', '');
                }
                if ($.cookie) {
                    $.cookie('sidebar_closed', '0');
                }
            } else {
                body.addClass("page-sidebar-closed");
                if ($.cookie) {
                    $.cookie('sidebar_closed', '1');
                }
            }
            handleSidebarAndContentHeight(); //fix content & sidebar height
            runResponsiveHandlers();
        });

        // handle the search bar close
        $('.page-sidebar').on('click', '.sidebar-search .remove', function (e) {
            e.preventDefault();
            $('.sidebar-search').removeClass("open");
        });

        // handle the search query submit on enter press
        $('.page-sidebar .sidebar-search').on('keypress', 'input.form-control', function (e) {
            if (e.which == 13) {
                $('.sidebar-search').submit();
                return false; //<---- Add this line
            }
        });

        // handle the search submit(for sidebar search and responsive mode of the header search)
        $('.sidebar-search .submit').on('click', function (e) {
            e.preventDefault();
            if ($('body').hasClass("page-sidebar-closed")) {
                if ($('.sidebar-search').hasClass('open') == false) {
                    if ($('.page-sidebar-fixed').size() === 1) {
                        $('.page-sidebar .sidebar-toggler').click(); //trigger sidebar toggle button
                    }
                    $('.sidebar-search').addClass("open");
                } else {
                    $('.sidebar-search').submit();
                }
            } else {
                $('.sidebar-search').submit();
            }
        });

        // header search box:

        // handle the search query submit on enter press
        $('.header .search-form').on('keypress', 'input.form-control', function (e) {
            if (e.which == 13) {
                $('.sidebar-search').submit();
                return false; //<---- Add this line
            }
        });

        //handle header search button click
        $('.header .search-form .submit').on('click', function (e) {
            e.preventDefault();
            $('.header .search-form').submit();
        });
    }

    // Handles the horizontal menu
    var handleHorizontalMenu = function () {
        //handle hor menu search form toggler click
        $('.header').on('click', '.hor-menu .hor-menu-search-form-toggler', function (e) {
            if ($(this).hasClass('off')) {
                $(this).removeClass('off');
                $('.header .hor-menu .search-form').hide();
            } else {
                $(this).addClass('off');
                $('.header .hor-menu .search-form').show();
            }
            e.preventDefault();
        });

        //handle tab click
        $('.header').on('click', '.hor-menu a[data-toggle="tab"]', function (e) {     
            e.preventDefault();
            var nav = $(".hor-menu .nav");
            var active_link = nav.find('li.current');
            $('li.active', active_link).removeClass("active");
            $('.selected', active_link).remove();
            var new_link = $(this).parents('li').last();
            new_link.addClass("current");
            new_link.find("a:first").append('<span class="selected"></span>');
        });

        //handle hor menu search button click
        $('.header').on('click', '.hor-menu .search-form .btn', function (e) {
            $('.form-search').submit();
            e.preventDefault();
        });

        //handle hor menu search form on enter press
        $('.header').on('keypress', '.hor-menu .search-form input', function (e) {
            if (e.which == 13) {
                $('.form-search').submit();
                return false;
            }
        });
    }

    // Handles the go to top button at the footer
    var handleGoTop = function () {
        /* set variables locally for increased performance */
        jQuery('.footer').on('click', '.go-top', function (e) {
            App.scrollTo();
            e.preventDefault();
        });
    }

    // Handles portlet tools & actions
    var handlePortletTools = function () {
        jQuery('body').on('click', '.portlet > .portlet-title > .tools > a.remove', function (e) {
            e.preventDefault();
            jQuery(this).closest(".portlet").remove();
        });

        jQuery('body').on('click', '.portlet > .portlet-title > .tools > a.reload', function (e) {
            e.preventDefault();
            var options = {};
            if ($(this).data('url')) {
                options.url = $(this).data('url');
            }
            options.element = $(this).closest(".portlet").children(".portlet-body");
            App.list(options);
        });

        // load ajax data on page init
        $('.portlet .portlet-title a.reload[data-load="true"]').click();

        jQuery('body').on('click', '.portlet > .portlet-title > .tools > .collapse, .portlet .portlet-title > .tools > .expand', function (e) {
            e.preventDefault();
            var el = jQuery(this).closest(".portlet").children(".portlet-body");
            if (jQuery(this).hasClass("collapse")) {
                jQuery(this).removeClass("collapse").addClass("expand");
                el.slideUp(200);
            } else {
                jQuery(this).removeClass("expand").addClass("collapse");
                el.slideDown(200);
            }
        });
    }

    // Handles custom checkboxes & radios using jQuery Uniform plugin
    var handleUniform = function () {
        if (!jQuery().uniform) {
            return;
        }
        var test = $("input[type=checkbox]:not(.toggle, .make-switch, .no-checker), input[type=radio]:not(.toggle, .star, .make-switch, .no-checker)");
        if (test.size() > 0) {
            test.each(function () {
                if ($(this).parents(".checker").size() == 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });
        }
    }

    var handleBootstrapSwitch = function () {
        if (!jQuery().bootstrapSwitch) {
            return;
        }
        $('.make-switch').each(function() {
            if (!$(this).closest('.has-switch').length) {
                $(this).bootstrapSwitch();
            }
        });
    }

    // Handles Bootstrap Accordions.
    var handleAccordions = function () {
        jQuery('body').on('shown.bs.collapse', '.accordion.scrollable', function (e) {
            App.scrollTo($(e.target));
        });
    }

    // Handles Bootstrap Tabs.
    var handleTabs = function () {
        // fix content height on tab click
        $('body').on('shown.bs.tab', '.nav.nav-tabs', function () {
            handleSidebarAndContentHeight();
        });

        //activate tab if tab id provided in the URL
        if (location.hash) {
            var tabid = location.hash.substr(1);
            $('a[href="#' + tabid + '"]').parents('.tab-pane:hidden').each(function(){
                var tabid = $(this).attr("id");
                $('a[href="#' + tabid + '"]').click();    
            });            
            $('a[href="#' + tabid + '"]').click();
        }
    }

    // Handles Bootstrap Modals.
    var handleModals = function () {
        // fix stackable modal issue: when 2 or more modals opened, closing one of modal will remove .modal-open class. 
        $('body').on('hide.bs.modal', function () {
           if ($('.modal:visible').size() > 1 && $('html').hasClass('modal-open') == false) {
              $('html').addClass('modal-open');
           } else if ($('.modal:visible').size() <= 1) {
              $('html').removeClass('modal-open');
           }
        });
            
        $('body').on('show.bs.modal', '.modal', function () {
            if ($(this).hasClass("modal-scroll")) {
                $('body').addClass("modal-open-noscroll");
            } 
        });

        $('body').on('hide.bs.modal', '.modal', function () {
            $('body').removeClass("modal-open-noscroll");
        });
    }

    // Handles Bootstrap Tooltips.
    var handleTooltips = function () {
       $('.tooltips').tooltip();
    }

    // Handles Bootstrap Dropdowns
    var handleDropdowns = function () {
        /*
          Hold dropdown on click  
        */
        $('body').on('click', '.dropdown-menu.hold-on-click', function (e) {
            e.stopPropagation();
        });
    }

    // Handle Hower Dropdowns
    var handleDropdownHover = function () {
        if (typeof dropdownHover == 'function') {
            $('[data-hover="dropdown"]').dropdownHover();
        }
    }

    var handleAlerts = function () {
        $('body').on('click', '[data-close="alert"]', function(e){
            $(this).parent('.alert').hide();
            e.preventDefault();
        });
    }

    // Handles Bootstrap Popovers

    // last popep popover
    var lastPopedPopover;

    var handlePopovers = function () {
        jQuery('.popovers').each(function() {
            $(this).popover();            
            $(this).on('shown.bs.popover', function() {
                var btn = $(this);
                var popover = $(this).next('.popover');
                popover.find('.popover-close').click(function() {
                    btn.popover('hide');
                });
                if (popover.find('.popover-inner-title').length) {
                    popover.find('.popover-content').css({'padding': 0});
                }
            });
        });
        $(document).click(function (e) {
            $('.popovers').each(function () {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    //$(this).popover('hide');
                    if ($(this).data('bs.popover').tip().hasClass('in') && $(this).data('donthide') == undefined) {
                        $(this).popover('toggle');
                    }                    
                    return;
                }
            });
        });
        // // close last poped popover
        // $(document).on('click.bs.popover.data-api', function (e) {
        //     if (lastPopedPopover) {
        //         lastPopedPopover.popover('hide');
        //     }
        // });
    };

    //## import assets
    var importAssets = (function (oHead) {

        function loadError (oError) {
            throw new URIError("The script " + oError.target.src + " is not accessible.");
        }

        return function (sSrc, fOnload) {
            var src = sSrc;
            var callBack = '';       
            if (typeof sSrc == 'object' && !$.isEmptyObject(sSrc)) {
                src = sSrc.shift();
                if (typeof src == 'object') {
                    callBack = (src.onload ? src.onload : ''); 
                    src = src.url;
                }
            }   
                  
            var id = 'assets-' + src.hashCode();
            if ($('#' + id).length) { //## if exists
                if (typeof sSrc == 'object' && sSrc.length) {
                    importAssets(sSrc, fOnload);
                } else if (fOnload)  {
                   fOnload(); 
                }
                return;
            }
            var typeMatch = src.match(/\/.+\.([css]{3}|[js]{2})/i);
            var type = (typeMatch.length ? typeMatch[1].toLowerCase() : 'js');
            var element = document.createElement("script");
            if (type == 'css') {
                element = document.createElement('link');
                element.rel = "stylesheet";
                element.type = "text\/css";
            } else {              
                element.type = "text\/javascript";  
            }
            element.id = id;
            element.onerror = loadError;
            element.onreadystatechange = element.onload = function() {
                setTimeout(function() {
                    if (callBack) { callBack(); }
                    if (typeof sSrc == 'object' && sSrc.length) {
                        importAssets(sSrc, fOnload);
                    } else if (fOnload) {
                        fOnload();
                    }
                }, 10);
            }

            oHead.appendChild(element);
            if(type == 'css') {
                element.href = src;
            } else {
              element.src = src;  
            }
            
        }

    })(document.head || document.getElementsByTagName("head")[0]);
    
    // Initialize Date Picker
    var initDatePicker = function () {
        var jalaliAssets = [
            '/assets/plugins/jalali-calender/skins/calendar-blue.css',
            '/assets/plugins/jalali-calender/jalali.js', 
            '/assets/plugins/jalali-calender/calendar.js',
            '/assets/plugins/jalali-calender/calendar-setup.js',
            '/assets/plugins/jalali-calender/lang/calendar-' + lang + '.js'
        ];
        importAssets(jalaliAssets, function() {
            $('*[data-datepicker]').each(function(key, val) {
                var inpDate = $(this);
                var id = inpDate.attr('id', 'datepicker_' + Math.round(Math.random() * 1000000)).attr('id');
                var options = {};
                switch (typeof inpDate.data('datepicker')) {
                    case 'string':
                        if (inpDate.data('datepicker').match(/^{[^}]*}$/)) {
                            options = $.parseJSON(inpDate.data('datepicker').replace(/\'/g, '"'));
                        }
                        break;
                    case 'object':
                        options = inpDate.data('datepicker');
                        break;
                } 
                if (typeof options != 'object') return false;
                var defaultOptions = {
                    inputField: id,
                    button: 'date_btn',
                    ifFormat: isRTL ? '%Y/%m/%d' : '%Y-%m-%d',    // the date format,
                    dateType: isRTL ? 'jalali' : 'gregorian'
                };
                options = $.extend(defaultOptions, options);
        
                $(this).css('direction', 'ltr');
                if (typeof Calendar == 'function') {
                    Calendar.setup(options);
                    $(this).focus(function(event) {
                        $(this).trigger('click');
                    });                
                } else {
                    setTimeout(function() {
                        if (typeof Calendar == 'function') {
                            Calendar.setup(options);
                        }
                    }, 100);
                }
            });
        });
    };

    //## input mask
    var initInputMask = function() {
        importAssets('/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', function() {
            $("input[data-inputmask]").each(function() {
                var options = {};
                var defaultOptions = { 
                    radixPoint: '.', 
                    autoGroup: true, 
                    groupSeparator: ',', 
                    groupSize: 3
                };
                switch (typeof $(this).data('inputmask')) {
                    case 'string':
                        if ($(this).data('inputmask').match(/^{[^}]*}$/)) {
                            options = $.parseJSON($(this).data('inputmask').replace(/\'/g, '"'));
                        }
                        break;
                    case 'object':
                        options = $(this).data('inputmask');
                        break;
                }
                var type = options.type ? options.type : 'decimal';
                var align = options.align ? options.algin : 'left';
                delete options.type;
                delete options.align;
                options = $.extend(defaultOptions, options);

                if (typeof $(this).inputmask == 'undefined') {
                    setTimeout(initInputMask, 100);
                } else  {   
                    $(this).inputmask(type, options);
                }
                
                $(this).css('text-align', align);
            });
        });
    };

    var removeInputMask = function() {
        $("input[data-inputmask]").each(function() {
            $(this).inputmask('remove');
        });
    };

    var initKnob = function(setPosition) {
        var setPosition = (setPosition ? setPosition : false);
        if (!jQuery().knob || App.isIE8()) {
            return;
        }
        var defaultOptions = {
            'dynamicDraw': true,
            'thickness': 0.2,
            'fgColor': '#E02222',
            'tickColorizeValues': true,
            'skin': 'tron',
            change: function(value) {
                var changeFunc = this.$.data('change');
                if (typeof changeFunc != 'undefined') {
                    eval(changeFunc);
                }
            },
            release: function(value) {
                var releaseFunc = this.$.data('release');
                if (typeof releaseFunc != 'undefined') {
                    eval(releaseFunc);
                }
            }
        };
        var options = {};

        // general knob
        $(".knob").each(function () {
            switch (typeof $(this).data('options')) {
                case 'string':
                    if ($(this).data('options').match(/^{[^}]*}$/)) {
                        options = $.parseJSON($(this).data('options').replace(/\'/g, '"'));
                    }
                    break;
                case 'object':
                    options = $(this).data('options');
                    break;
            }
            options = $.extend(defaultOptions, options);
            $(this).knob(options);
            $(this).removeClass('display-none'); 
            if (window.dir == 'rtl' && setPosition) {
                $(this).css('left', $(this).position().left + 35);
            }
        });
    };

    // Handles scrollable contents using jQuery SlimScroll plugin.
    var handleScrollers = function () {
        $('.scroller').each(function () {
            var height;
            if ($(this).attr("data-height")) {
                height = $(this).attr("data-height");
            } else {
                height = $(this).css('height');
            }
            $(this).slimScroll({
                allowPageScroll: true, // allow page scroll when the element scroll is ended
                size: '7px',
                color: ($(this).attr("data-handle-color")  ? $(this).attr("data-handle-color") : '#bbb'),
                railColor: ($(this).attr("data-rail-color")  ? $(this).attr("data-rail-color") : '#eaeaea'),
                position: isRTL ? 'left' : 'right',
                height: height,
                alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
                railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
                disableFadeOut: true
            });
        });
    }

    // Handles Image Preview using jQuery Fancybox plugin
    var handleFancybox = function () {
        if (!jQuery.fancybox) {
            return;
        }

        if (jQuery(".fancybox-button").size() > 0) {
            jQuery(".fancybox-button").fancybox({
                groupAttr: 'data-rel',
                prevEffect: 'none',
                nextEffect: 'none',
                closeBtn: true,
                helpers: {
                    title: {
                        type: 'inside'
                    }
                }
            });
        }
    }

    // Fix input placeholder issue for IE8 and IE9
    var handleFixInputPlaceholderForIE = function () {
        //fix html5 placeholder attribute for ie7 & ie8
        if (isIE8 || isIE9) { // ie8 & ie9
            // this is html5 placeholder fix for inputs, inputs with placeholder-no-fix class will be skipped(e.g: we need this for password fields)
            jQuery('input[placeholder]:not(.placeholder-no-fix), textarea[placeholder]:not(.placeholder-no-fix)').each(function () {

                var input = jQuery(this);

                if (input.val() == '' && input.attr("placeholder") != '') {
                    input.addClass("placeholder").val(input.attr('placeholder'));
                }

                input.focus(function () {
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                });

                input.blur(function () {
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                        input.val(input.attr('placeholder'));
                    }
                });
            });
        }
    }

    // Handle full screen mode toggle
    var handleFullScreenMode = function() {
        // mozfullscreenerror event handler
       
        // toggle full screen
        function toggleFullScreen() {
          if (!document.fullscreenElement &&    // alternative standard method
              !document.mozFullScreenElement && !document.webkitFullscreenElement) {  // current working methods
            if (document.documentElement.requestFullscreen) {
              document.documentElement.requestFullscreen();
            } else if (document.documentElement.mozRequestFullScreen) {
              document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullscreen) {
              document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
            }
          } else {
            if (document.cancelFullScreen) {
              document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
              document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
              document.webkitCancelFullScreen();
            }
          }
        }

        $('#trigger_fullscreen').click(function() {
            toggleFullScreen();
        });
    }

    // Handle Select2 Dropdowns
    var handleSelect2 = function() {
        if (jQuery().select2) {
            $('.select2me').select2({
                placeholder: "Select",
                allowClear: true
            });
        }
    };
   
    //## auto resize textarea
    var handleTextarea = function() { 
        jQuery.each(jQuery('textarea'), function() {
            var offset = this.offsetHeight - this.clientHeight;
         
            var resizeTextarea = function(el) {
                jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
            };

            resizeTextarea(this);
            jQuery(this).on('keyup input', function() { resizeTextarea(this); });
        });
    };

    //## shift checkbox or check all
    var shiftcheckbox = function() {
        $('.data-list').each(function() {
            var dataList = $(this);
            var count = checkAll = 0;
            dataList.find('div.checker').find('input[type=checkbox]').each(function() {
                if (!$(this).hasClass('check-all')) {
                    if ($(this).is(':checked')) {
                        checkAll++;
                    }
                    count++;
                }
            });

            if (count == checkAll && count > 0) {
                dataList.closest('.data-list').find('.check-all').attr('checked', 'checked');
                dataList.closest('.data-list').find('.check-all').parent().addClass('checked');
            }
            dataList.find('div.checker').shiftcheckbox({
                checkboxSelector : ':checkbox',
                selectAll        : dataList.closest('.data-list').find('.check-all'),
                ignoreClick      : 'a',
                onChange : function(checked) {
                    if (checked) {
                        $(this).closest('span').addClass('checked');
                    } else { 
                        $(this).closest('span').removeClass('checked');
                    }

                }

            });
        });
    };

    // Handle Theme Settings
    var handleTheme = function () {

        var panel = $('.theme-panel');

        //handle theme layout
        var resetLayout = function () {
            $.removeCookie('themeOptions');

            $("body").
                removeClass("page-boxed").
                removeClass("page-footer-fixed").
                removeClass("page-sidebar-fixed").
                removeClass("page-header-fixed").
                removeClass("page-sidebar-reversed");

            $('.header > .header-inner').removeClass("container");

            if ($('.page-container').parent(".container").size() === 1) {
                $('.page-container').insertAfter('body > .clearfix');
            }

            if ($('.footer > .container').size() === 1) {
                $('.footer').html($('.footer > .container').html());
            } else if ($('.footer').parent(".container").size() === 1) {
                $('.footer').insertAfter('.page-container');
            }

            $('body > .container').remove();
        }

        var lastSelectedLayout = '';

        //handle theme layout
        var setLayout = function (changeCookie) {
            var changeCookie = (typeof changeCookie != 'undefined' ? changeCookie : false);
      
            var themeOptions = {
                'layoutOption': $('select.layout-option', panel).val(), 
                'sidebarOption': $('select.sidebar-option', panel).val(), 
                'headerOption': $('select.header-option', panel).val(),
                'footerOption': $('select.footer-option', panel).val(),
                'sidebarPosOption': $('select.sidebar-pos-option', panel).val()
            };

            if (changeCookie) {
                resetLayout(); // reset layout to default state
                $.cookie('themeOptions', JSON.stringify(themeOptions));
            } else {
                var opt = $.parseJSON($.cookie('themeOptions'));
                if (!$.isEmptyObject(opt)) {                    
                    $('select.layout-option').select2('val', opt.layoutOption); 
                    $('select.sidebar-option').select2('val', opt.sidebarOption); 
                    $('select.header-option').select2('val', opt.headerOption);
                    $('select.footer-option').select2('val', opt.footerOption);
                    $('select.sidebar-pos-option').select2('val', opt.sidebarPosOption);
                    themeOptions = $.extend(themeOptions, opt);
                }
            }

            if (themeOptions.sidebarOption == "fixed" && themeOptions.headerOption == "default") {
                alert('Default Header with Fixed Sidebar option is not supported. Proceed with Fixed Header with Fixed Sidebar.');
                $('.header-option', panel).val("fixed");
                $('.sidebar-option', panel).val("fixed");
                themeOptions.sidebarOption = 'fixed';
                themeOptions.headerOption = 'fixed';
            }

            if (themeOptions.layoutOption === "boxed") {
                $("body").addClass("page-boxed");

                // set header
                $('.header > .header-inner').addClass("container");
                var cont = $('body > .clearfix').after('<div class="container"></div>');

                // set content
                $('.page-container').appendTo('body > .container');

                // set footer
                if (themeOptions.footerOption === 'fixed') {
                    $('.footer').html('<div class="container">' + $('.footer').html() + '</div>');
                } else {
                    $('.footer').appendTo('body > .container');
                }
            }

            if (lastSelectedLayout != themeOptions.layoutOption) {
                //layout changed, run responsive handler:
                runResponsiveHandlers();
            }
            lastSelectedLayout = themeOptions.layoutOption;

            //header
            if (themeOptions.headerOption === 'fixed') {
                $("body").addClass("page-header-fixed");
                $(".header").removeClass("navbar-static-top").addClass("navbar-fixed-top");
            } else {
                $("body").removeClass("page-header-fixed");
                $(".header").removeClass("navbar-fixed-top").addClass("navbar-static-top");
            }

            //sidebar
            if ($('body').hasClass('page-full-width') === false) {
                if (themeOptions.sidebarOption === 'fixed') {
                    $("body").addClass("page-sidebar-fixed");
                } else {
                    $("body").removeClass("page-sidebar-fixed");
                }
            }

            //footer 
            if (themeOptions.footerOption === 'fixed') {
                $("body").addClass("page-footer-fixed");
            } else {
                $("body").removeClass("page-footer-fixed");
            }

            //sidebar position
            if (App.isRTL()) {
                if (themeOptions.sidebarPosOption === 'left') {
                    $("body").addClass("page-sidebar-reversed");
                    $('#frontend-link').tooltip('destroy').tooltip({placement: 'right'});
                } else {
                    $("body").removeClass("page-sidebar-reversed");
                    $('#frontend-link').tooltip('destroy').tooltip({placement: 'left'});
                }
            } else {
                if (themeOptions.sidebarPosOption === 'right') {
                    $("body").addClass("page-sidebar-reversed");
                    $('#frontend-link').tooltip('destroy').tooltip({placement: 'left'});
                } else {
                    $("body").removeClass("page-sidebar-reversed");
                    $('#frontend-link').tooltip('destroy').tooltip({placement: 'right'});
                }
            }

            handleSidebarAndContentHeight(); // fix content height            
            handleFixedSidebar(); // reinitialize fixed sidebar
            handleFixedSidebarHoverable(); // reinitialize fixed sidebar hover effect
        }
        setLayout();

        // handle theme colors
        var setColor = function (color) {
            var color_ = (App.isRTL() ? color + '-rtl' : color);
            $('#style_color').attr("href", "/assets/plugins/metronic/css/themes/" + color_ + ".css");
            if ($.cookie) {      
                $.cookie('style_color', color);
            }
        }

        $('.toggler', panel).click(function () {
            $('.toggler').hide();
            $('.toggler-close').show();
            $('.theme-panel > .theme-options').show();
        });

        $('.toggler-close', panel).click(function () {
            $('.toggler').show();
            $('.toggler-close').hide();
            $('.theme-panel > .theme-options').hide();
        });

        $('.theme-colors > ul > li', panel).click(function () {
            var color = $(this).attr("data-style");
            setColor(color);
            $('ul > li', panel).removeClass("current");
            $(this).addClass("current");
        });

        $('.layout-option, .header-option, .sidebar-option, .footer-option, .sidebar-pos-option', panel).change(function(e) {
            setLayout(true);
        });

        if ($.cookie && $.cookie('style_color')) {
            setColor($.cookie('style_color'));
        }
    };


    var handleShortcut = function() {        
        $('.edit-responsive').find('input, select').keypress(function(e) {
            if (e.keyCode == 13 || e.keyCode == 10) {
                e.preventDefault();
            }
        });
    };
    
    //## expand list
    var handleListExpand = function() {        
        $('.list-btn-expand').off('click').click(function() {
            var exRow = $(this).closest('tr').next('tr.row-expand');
            if ($(this).hasClass('in')) {
                $(this).removeClass('in');
                $(this).find('i')
                    .removeClass('fa-minus-square-o')
                    .addClass('fa-plus-square-o');
                exRow.find('.table-responsive').slideUp(function() {
                    $(this).addClass('display-none');
                    exRow.addClass('display-none');
                });
            } else {
                $(this).addClass('in');
                $(this).find('i')
                    .removeClass('fa-plus-square-o')
                    .addClass('fa-minus-square-o');
                exRow.removeClass('display-none').find('.table-responsive').slideDown(function() {
                    $(this).removeClass('display-none');
                });
            }
        });
    };

    var initTinymce = function (obj, config) {
        var obj = $(obj);
        if (!obj.length) return false;
        config.selector = (typeof config.selector != 'undefined' ? config.selector : ('.' + obj.data('class')));
        if (typeof tinymce != 'undefined' && typeof config.selector != 'undefined') {
            tinymce.init(config);
        } else {
            setTimeout(function() {
                initTinymce(obj, config);
            }, 100);
        }
    };

    var handleEditor = function() {

        if ($('textarea[data-editor]').length) {
            var ckEditorExist = tinymceExist = wysihtmlFiveExist = false ;          
            $('textarea[data-editor]:not(:hidden)').each(function() {
                if ($(this).parent().find('.wysihtml5-toolbar').length) return false; 
                var cfg = {};  
                switch (typeof $(this).data('editor')) {
                    case 'string':
                        if ($(this).data('editor').match(/^{[^}]*}$/)) {
                            cfg = $.parseJSON($(this).data('editor').replace(/\'/g, '"'));
                        }
                        break;
                    case 'object':
                        cfg = $(this).data('editor');
                        break;
                } 
                if (cfg.ckEditor) {
                    ckEditorExist = true;
                } else if (cfg.tinymce) {
                    tinymceExist = true;
                } else {
                     wysihtmlFiveExist = true;
                }
            });
            var assets = null;
            if (ckEditorExist) {
                assets = '/assets/plugins/ckeditor/ckeditor.js';
            } else if (tinymceExist) {
                assets = '/assets/plugins/tinymce/tinymce.min.js';
            } else if (wysihtmlFiveExist) {
                assets = [
                    '/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5' + (window.dir == 'rtl' ? '-rtl' : '') + '.css',
                    '/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js',
                    '/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js',
                ]
            }

            importAssets(assets, function() {
                try {
                    setTimeout(function() {
                        $('textarea[data-editor]').each(function() {    
                            var textarea = $(this);                        
                            if ($(this).parent().find('.wysihtml5-toolbar').length) return false; 
                            if ($(this).is(':visible')) {
                                var config = {
                                    language: lang,
                                    contentsLangDirection: window.dir,
                                    baseFloatZIndex: 100000, 
                                    toolbarGroups: [
                                        { name: 'document',    groups: [ 'mode', 'document' ] },        // Displays document group with its two subgroups.
                                        { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },       // Group's name will be used to create voice label.
                                        '/',                                                            // Line break - next group will be placed in new line.
                                        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                                        { name: 'links' },
                                        { name: 'tools' }
                                    ]
                                };

                                var customConfig = {};  
                                var ckID = ($(this).attr('id') ? $(this).attr('id') : ('editor-' + parseInt(Math.random() * 100000))); 
                                $(this).attr('id', ckID);    
                           
                                switch (typeof $(this).data('editor')) {
                                    case 'string':
                                        if ($(this).data('editor').match(/^{[^}]*}$/)) {
                                            customConfig = $.parseJSON($(this).data('editor').replace(/\'/g, '"'));
                                        }
                                        break;
                                    case 'object':
                                        customConfig = $(this).data('editor');
                                        break;
                                } 

                                var config = $.extend(config, customConfig);  
                            
                                if (config.ckEditor) {
                                    delete config.ckEditor;
                                    CKEDITOR.replace(ckID, config);
                                } else if (config.tinymce) {
                                    delete config.tinymce;
                                    config.directionality = window.dir;
                                    $('textarea[data-editor]').each(function(el) {   
                                        var className = ('textarea-' + parseInt(Math.random() * 1000000));
                                        $(this).addClass(className);
                                        $(this).data('class', className);
                                    });
                                    setTimeout(function() {
                                        initTinymce(textarea, config);
                                    }, 100);
                                } else {
                                    customConfig.events = {};
                                    customConfig.events.load =  function() { 
                                        $('.wysihtml5-sandbox').contents().find('html').find('body').css({
                                            'direction': window.dir,
                                            'padding': '8px 8px 0',
                                            'min-height': 175
                                        });
                                    };
                                    customConfig.locale = (lang == 'fa' ? 'fa' : 'en');
                                    $(this).wysihtml5(customConfig);
                                }
                            }
                        });
                    }, 50);
                } catch(e) {
                    console.log(e);
                }
            });
        }
    };

    var handleAjaxError = function(xhr, ajaxOptions, thrownError, errorType)
    {   
        if (isLocal) {
            var msg = '';//App.trans('error on reloading the content. please check your connection and try again.');
            if (xhr.responseText) {
                var err = {};
                try { // laravel error                           
                    var matchErr = xhr.responseText.match(/\{\"error\"\:[^}]+}}/);
                    var err = matchErr.length ? JSON.parse(matchErr[0]) : '';
                    msg = '<div style="padding-top: 15px;direction: ltr;">';
                    msg += xhr.status + ': ' + xhr.statusText +  "<br/>";
                    $.each(err.error, function(key, val) {
                        msg += (key.substring(0, 1).toUpperCase() + key.substring(1)) + ': ' + val + "<br/>";
                    });
                    msg += '</div>';
                } catch(e) {
                    msg = xhr.responseText;
                }
            }
            if (errorType == "toastr" && toastr) {
                toastr.error(msg);
            } else if (errorType == "notific8" && $.notific8) {
                $.notific8('zindex', 11500);
                $.notific8(msg, {theme: 'ruby', life: 3000});
            } else if (msg) {
                bootbox.alert(msg);
            }
        }
    };

    var handleRequest = function(obj, options) {
        var errorType = options.errortype ? options.errortype : '';
        var url = options.url ? options.url : '';
        if (!url) {
            url = obj.attr('href') ? obj.attr('href') : (obj.data('href') ? obj.data('href') :
            (document.location.pathname + (options.method ? ('/' + options.method) : '') + (options.id ? ('/' + options.id) : '') + document.location.search)) ;
        }   
        
        $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: options.cache != undefined ? options.cache : false,
            data: (typeof options.data == 'object' && !$.isEmptyObject(options.data) ? options.data : (options.args != undefined && !$.isEmptyObject(options.args) ? options.args : (options.sendform ? '' : {}))),
            url: url,
            dataType: "html",
            success: function (res) { 
                var editBox = null;

                if (options.method == 'delete' && res.match(/[[ERROR]]/ig) == null) {
                    obj.closest('tr.gradeX').remove();
                }
                if (options.result == 'iframe') {
                    $('.edit-frame').contents().find('html').html(res);
                } else {
                    $('.edit-window').html(res);                
                    var editBox = $('.edit-responsive').modal({
                        backdrop: 'static'
                    });
                    if (editBox.data('draggable') == true) {                       
                        editBox.draggable({
                            handle: ".modal-header"
                        }); 
                        editBox.find('.modal-header').css('cursor', 'move');
                    }
                    if (editBox.data('resizable') == true) {
                        editBox.resizable({
                            edge: 15
                        });
                    }
                    editBox.on('hide.bs.modal', function() {
                        removeInputMask();
                    });

                    $('*[data-advancedselect]').each(function() {
                        App.setSelect2($(this));
                    });

                    App.jstree();
                    //## Auto Increment Display Rank
                    if ($('.display-rank').length) {
                        $('.display-rank').each(function() {
                            if ($(this).val().trim() == '') $(this).val(($(this).data('default') != undefined ? $(this).data('default') : 10000));
                        })
                    }
                }
                App.initAjax();
                App.autoSave(editBox);
                //## Ctrl + Enter
                $('.edit-responsive').keypress(function(e) {
                    if ((e.keyCode == 13 || e.keyCode == 10) && e.ctrlKey == true) {
                        $(this).find('form .store-btn').trigger('click');
                    }
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('body').modalmanager('loading');
                handleAjaxError(xhr, ajaxOptions, thrownError, errorType);
            }
        });
    };

    //## convert seond 2 time H:M:S
    var second2Time = function (second) {        
        var h = m = 0;
        var s = (second < 60 ? second : 0);
        if (second >= 60 && second < 3600) {
            m = parseInt(second / 60);
            s = (second - (m * 60));
        } else if (second >= 3600) {
            h = parseInt(second / 3600);
            m = (second - (h * 3600));
            m = (m >= 60 ? parseInt(m / 60) : m);
            s = (second - ((h * 3600) + (m * 60)));         
        }
        return {'h': h, 'm': m, 's': s};
    };

    var autoSaveForm = function(options) {
        var frm = options.frm ? $(options.frm) : $(defaultOptions.frm);
        var url = options.url ? options.url : frm.find('form').first().attr('action');
        var serializeFormData = frm.find('input[name], select[name], textarea[name]').serialize();
        if (serializeFormData != frm.data('autoSaveFormData')) {
            frm.data('autoSaveFormData', serializeFormData);
            var ajaxRequest = $.post(url, serializeFormData, function(res, status) {

                if (res) {
                    $.each(res, function(key, val) {
                        var r = new RegExp(/idList\d*/);
                        if (key  == 'id') {
                            if (val.className.length) {
                                frm.find('.' + val.className).val(val.id);
                            } else {
                                frm.find('#id, .id').val(val.id);
                            }
                        }
                        if (r.test(key)) {
                            $.each(val.idList, function(row, v) {
                                if (val.className.length) {
                                    var rExp = new RegExp(row);
                                    frm.find('.' + val.className).each(function() {                                                
                                        if (rExp.test($(this).attr('name'))) {
                                            $(this).val(v);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }, "json");
            frm.data('autoSaveRequest', ajaxRequest);
        }
        frm.data('autoSaveInterval', autoSaveInterval);
        frm.on('hide.bs.modal', function() {
            clearInterval(autoSaveInterval);
            clearInterval(frm.data('autoSaveInterval'));
            if (typeof frm.data('autoSaveRequest') == 'object') {
                frm.data('autoSaveRequest').abort();
            }
            frm.data('autoSaveInterval', '');
            frm.data('autoSaveRequest', '');
        });
        if (frm.data('autoSaveInterval')) {    
            clearInterval(frm.data('autoSaveInterval'));        
            var autoSaveInterval = setTimeout(function() { autoSaveForm(options); },  options.periodTime * 1000);
            frm.data('autoSaveInterval', autoSaveInterval);
        }

    
    };

    var handleAutoSave = function(options) {
        var defaultOptions = {
                periodTime: 60, // seconds
                frm: '.edit-responsive',
                showMessage: true
        };

        var options = $.extend(defaultOptions, options);
        options.periodTime = (options.periodTime == 0 ? defaultOptions.periodTime : options.periodTime);
        var frm = options.frm ? $(options.frm) : $(defaultOptions.frm);
        var autoSaveInterval = setTimeout(function() { autoSaveForm(options); },  options.periodTime * 1000);

        //## show msg
        if (options.showMessage) {            
            var pTime = second2Time(options.periodTime);
            var msg = '<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
            var h = App.convertDigit(pTime.h, lang);
            var m = App.convertDigit(pTime.m, lang);
            var s = App.convertDigit(pTime.s, lang);
            if (lang == 'fa') {
                msg += '<span>دیتای این فرم هر ';
                if (pTime.h && pTime.m && pTime.s) {
                    msg += (h + ' ساعت و ' + m + ' دقیقه و ' + s + ' ثانیه');
                } else if (pTime.m && pTime.s) {
                    msg += (m + ' دقیقه و ' + s + ' ثانیه');
                } else if (pTime.h) {
                    msg += (h + ' ساعت');
                } else if (pTime.m) {
                    msg += (m + ' دقیقه');
                } else if (pTime.s) {
                    msg += (s + ' ثانیه');
                }
                msg += ' به صورت خودکار ذخیره می شود.</span>';
            } else {
                if (pTime.h && pTime.m && pTime.s) {
                    msg += (h + ' hour and ' + m + ' minute and ' + s + ' second');
                } else if (pTime.m && pTime.s) {
                    msg += (m + ' minute and ' + s + ' second');
                } else if (pTime.h) {
                    msg += (h + ' hour');
                } else if (pTime.m) {
                    msg += (m + ' minute');
                }  else if (pTime.s) {
                    msg += (s + ' second');
                }
                msg += App.trans('this form data automatically saved every')
            }
            msg += '</div>';
            frm.find('.modal-body').prepend(msg);
        }

        frm.data('autoSaveInterval', autoSaveInterval);
        frm.on('hide.bs.modal', function() {
            clearInterval(autoSaveInterval);
            clearInterval(frm.data('autoSaveInterval'));
            if (typeof frm.data('autoSaveRequest') == 'object') {
                frm.data('autoSaveRequest').abort();
            }
            frm.data('autoSaveInterval', '');
            frm.data('autoSaveRequest', '');
        });
    };

    var serializeEvent = function (e){
        if(!e) return 'NULL';
        var keyList = ["altKey", "clientX", "clientY", "ctrlKey", "metaKey", "pageX", "pageY", "shiftKey", "timeStamp", "target", "type"];
        res = $.parseJSON(JSON.stringify(e, keyList, 4));
        res.target = {'tagName': e.target.tagName, 'id': e.target.id, 'className': e.target.className, 'classList': e.target.classList};        
        return JSON.stringify(res);    
    };

    return {
        reCaptcha : function (obj) {
            if (typeof obj == 'undefined') {
                $('.captcha').attr('src', '/captcha/flat?' + Math.round(Math.random(1) * 1000000));
            } else {
                $(obj).closest('.row').find('img').attr('src', '/captcha/flat?' + Math.round(Math.random(1) * 1000000));
            }
        },
        
        //## load count with ajax (message count) // key -> className , val -> value
        loadCount: function (url, options, callBack) {
            if (!url.length) {
                console.log('url is not defined');
                return;   
            }  
            if (typeof msgCountRequest !== 'undefined') {
                msgCountRequest.abort();
            }
            defaultOptions = {
                parenthesis: true
            };
            var options = $.extend(defaultOptions, options);
            msgCountRequest = $.ajax({
                url: url, 
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                data: {'auto': App.getRandom() }, 
                success: function(res) {
                    if (typeof res == 'object') {
                        $.each(res, function(key, val) {
                            if (val && options.parenthesis) {
                                $(key).html('(' + App.convertDigit(val, lang) + ')');
                            } else if (options.parenthesis == false && val) {    
                                $(key).html(App.convertDigit(val, lang));
                            } else {
                                $(key).html('');
                            }
                        });
                    }
                    if (typeof callBack == 'function') {
                        callBack(res);
                    }
                }
            });
        },
        
        //## send popup form
        sendPopupForm: function(frm, e, options) {
            frm = $(frm);
            if (!frm.length) return false;
            if (!frm.valid()) return false;

            var defaultOptions = {
                "modalObject" : frm.closest('.edit-responsive'),
                "frameObject" : $('.edit-frame'),
                'callback': ''
            };
            if (typeof options == 'string') {
                if (options.match(/^{[^}]*}$/)) {
                    options = $.parseJSON(options.replace(/\'/g, '"'));
                }
            } 
            options = $.extend(defaultOptions, options);
            if (options.modalObject.length) {
                var fileInputList = options.modalObject.find('input[type=file]');
                if(!fileInputList.length) {
                    var formObject = options.modalObject.find('form[target=edit_frame]');
                    formObject.removeAttr('enctype');
                }
            }

            if (options.frameObject.length) {
                options.frameObject.addClass('hidden');
                App.createOverlay(frm, true); // create form overlay
                options.frameObject.off('load').on('load', function() {
                    try {
                        var frameHTML = options.frameObject.contents().find('html');
                        if (frameHTML.find('.phpdebugbar-body').length) {
                            frameHTML.find('.phpdebugbar-body').height(300);
                        }
                        var frameHTMLContent = frameHTML.html();
                        var result = frameHTMLContent.toLowerCase().indexOf('[[ok]]');
                        if (options.modalObject.length && result != -1 && !App.isHTML(frameHTMLContent)) {
                            options.frameObject.removeClass('hidden');
                            options.modalObject.modal('hide');
                            if (frm.data('aftersend')) {
                                var statementList = frm.data('aftersend').split(';');
                                if (statementList.length) {
                                    $.each(statementList, function(key, val) {
                                        if (val.trim().length)
                                            eval(val);
                                    });
                                }
                            } else {
                                if (typeof options.callback == 'function') {                                 
                                    options.callback();
                                } else {
                                    App.list();
                                }
                            }
                            
                        } else {
                             if (App.isHTML(frameHTMLContent)) {
                                var msgFrame = "<iframe class='err-frame' style='width: 100%; height: 400px;border: none;'></iframe>";   
                                frameHTMLContent = frameHTMLContent.replace(/[^\.]alert/gi, '//alert');
                                if (result == -1 && frameHTMLContent.match(/[^\.]alert/gi) == null) {
                                    bootbox.dialog({
                                        message: msgFrame,
                                        title: '<strong style="color: #700;">' + App.trans("error exception") + '</strong>',
                                        className: 'error-modal'
                                    }).css('left', '37%').find('.modal-dialog').css('width', 900);
                                }
                                var response = options.frameObject.contents().find('html').html();
                                options.frameObject.contents().find('html').empty(); 
                                setTimeout(function() { 
                                    $('.err-frame').contents().find('html').html(frameHTMLContent);
                                    options.frameObject.removeClass('hidden');
                                }, 300);

                                if (result != -1) {
                                    options.modalObject.modal('hide');
                                    if (typeof options.callback == 'function') {                                    
                                        options.callback(response);
                                    } else {
                                        App.list();
                                    }
                                } 
                            } else {
                                var objMsg = {};
                                try {
                                    var objMsg = $.parseJSON(frameHTML.find('body').html());
                                    frameHTMLContent = objMsg.message;
                                } catch(e) {}

                                if (frameHTMLContent.match(/array/i) != null) {
                                    frameHTMLContent = '<pre class="pre-alert" style="direction: ltr;margin-top: 20px;">' + frameHTMLContent + '</pre>';
                                }

                                var alertBox = bootbox.alert($.nl2br(frameHTMLContent.toHtml()));
                                if (typeof objMsg.type != 'undefined' && objMsg.type == 'success') {
                                    options.frameObject.removeClass('hidden');
                                    options.modalObject.modal('hide');
                                }
                                
                                setTimeout(function() {
                                    var preObject = $(alertBox).find('.pre-alert');
                                    $(alertBox).data('height', $(alertBox).height());
                                    var preH = ($(alertBox).height() >= $(window).height()  ? ($(window).height() - 300 ) : preObject.height());
                                    preH = ($(window).height() <= 300 ? 200 : preH);
                                    preObject.height(preH);
                                    $(window).resize(function() {
                                        preH = ($(alertBox).data('height') >= $(window).height()  ? ($(window).height() - 300 ) : preObject.height());
                                        preH = ($(window).height() <= 300 ? 200 : preH);
                                        preObject.height(preH);
                                    });
                                }, 250);
                                options.frameObject.contents().find('html').empty();
                                options.frameObject.removeClass('hidden');
                            }
                        }
                    } catch (e) {
                        console.log(e);
                    }                    
                    App.removeOverlay(frm); // remove form overlay
                });
            }
        },

        //## import css & js and run callback after loading assets
        importAssets: function(src, callBack) {
            importAssets(src, callBack);
        },

        //## auto save popup form
        autoSave: function (editBox) {    
            if (editBox == null) return;
            editBox = $(editBox);
            if (!editBox.length) return;
            if (editBox.data('autosave')) {
                var opt = {};
                switch (typeof editBox.data('autosave')) {
                    case 'string':
                        if (editBox.data('autosave').match(/^{[^}]*}$/)) {
                            opt = $.parseJSON(editBox.data('autosave').replace(/\'/g, '"'));
                        }
                        break;
                    case 'object':
                        opt = editBox.data('autosave');
                        break;
                }
                opt.frm = editBox;
                handleAutoSave(opt);
            }
        },

        searchSide: function (obj) {
            var obj = $(obj);
            if (!obj.length) return false;
            var qStr = obj.val();

            $.extend($.expr[':'], {
              'containsi': function(elem, i, match, array) {
                return (elem.textContent || elem.innerText || '').toLowerCase()
                    .indexOf((match[3] || "").toLowerCase()) >= 0;
              }
            });
            if (qStr) {
                $(".page-sidebar-menu")
                    .find("li:not(.sidebar-toggler-wrapper, .sidebar-search-wrapper)")
                    .addClass('hidden');
  
                $(".page-sidebar-menu")
                    .find("li:containsi('" + qStr + "')")
                    .removeClass('hidden');
            } else {
                $(".page-sidebar-menu").find("li").removeClass('hidden');
            }
        },

        isHTML: function (str) {
            var a = document.createElement('div');
            a.innerHTML = str;
            for (var c = a.childNodes, i = c.length; i--; ) {
                if (c[i].nodeType == 1) return true; 
            }
            return false;
        },

        convertDigit: function(num, type) {
            type = type ? type : 'en';
            if (type == 'en') {
                num = String(num);
                num = (num).replace(/٫/g, '.');
                num = (num).replace(/۰/g, '0');
                num = (num).replace(/۱/g, '1');
                num = (num).replace(/۲/g, '2');
                num = (num).replace(/۳/g, '3');
                num = (num).replace(/۴/g, '4');
                num = (num).replace(/۵/g, '5');
                num = (num).replace(/۶/g, '6');
                num = (num).replace(/۷/g, '7');
                num = (num).replace(/۸/g, '8');
                num = (num).replace(/۹/g, '9');
                num = (num).replace(/٤/g, '4');
                num = (num).replace(/٥/g, '5');
                num = (num).replace(/٦/g, '6');
            } else if (type == 'fa') {
                num = String(num);
                num = (num).replace(/0/g, '۰');
                num = (num).replace(/1/g, '۱');
                num = (num).replace(/2/g, '۲');
                num = (num).replace(/3/g, '۳');
                num = (num).replace(/4/g, '۴');
                num = (num).replace(/5/g, '۵');
                num = (num).replace(/6/g, '۶');
                num = (num).replace(/7/g, '۷');
                num = (num).replace(/8/g, '۸');
                num = (num).replace(/9/g, '۹');
            }
            return num;
        },

        // set sub group
        setSubGroup: function (objData, first, group1, group2, selected) {
            var src = $((typeof group1 == 'undefined') ? 'group1' : group1);
            var des = $((typeof group2 == 'undefined') ? 'group2' : group2);
            var first = first ? first : 0;
            var lang = isRTL ? 'fa' : 'en';
        
            if (!src.length || !des.length || !objData) return;

            var desType = (typeof src.data('destype') != 'undefined' ? src.data('destype') : 'option');
            var desName = (typeof src.data('desname') != 'undefined' ? src.data('desname') : 'chk');

            if (typeof des.data('advancedselect') != 'undefined') {
                des.parent().find('.select2-container').remove();
            }

            var data = {};

            if (typeof src.val() == 'object' && desType == 'option' && !$.isEmptyObject(src.val())) {                  
                $.each(src.val(), function(key, val) {
                    var text = (src.find('option[value="' + val + '"]').length ? src.find('option[value="' + val + '"]').first().text() : val);
                    data[text] = objData[val]; 
                }); 
            } else if (src.val()) {
                data = objData[src.val()];
            } else {
                $.each(objData, function(key, val) {
                    if (typeof val == 'object' && !$.isEmptyObject(val)) {
                        $.each(val, function(k, v) {
                            data[k] = v;
                        });
                    } else {
                        data[key] = val;
                    }
                });
            }
            
            switch(desType) {
                case 'checkbox':
                    des.empty();                    
                    if (typeof data == 'undefined' || data.length == 0) {
                        if (des.data('advancedselect') != undefined) {
                            App.setSelect2(des);
                        }
                        return;
                    };
                    if (typeof data == 'object') {
                        $.each(data, function (key, val) {
                            var id = desName + key;
                            des.append('<input id="' + id + '" type="checkbox" name="' + desName + 
                                '[]" value="' + val + '"' + (src.data('checkall') != undefined ? 'checked="checked"' : '')  
                                + '>&nbsp;<label data-value="' + val + '">' + App.convertDigit(key, lang) + '</label>&nbsp;&nbsp;');
                        });
                    }
                    break;
                default:
                    des.find('option, optgroup').each(function (key) {
                        if (key >= first) $(this).remove();
                    });                    
                    if (typeof data == 'undefined' || data.length == 0) {
                        if (des.data('advancedselect') != undefined) {
                            App.setSelect2(des);
                        }
                        return;
                    };
                    if (typeof data == 'object') {
                        $.each(data, function (key, val) {
                            var opt = null;
                            if (typeof val == 'object' && !$.isEmptyObject(val)) {
                                opt = '<optgroup label="' + key + '">';
                                $.each(val, function(k, v) {
                                    opt += ('<option value="' + v + '">' + k + '</option>');
                                });
                                opt += "</optgroup>";
                                des.append(opt); 
                            } else if (typeof val != 'object')  {
                                opt = '<option value="' + val + '">' + key + '</option>';
                                des.append(opt); 
                            }                       
                        });                        
                    }
                    
                    des.find('option').each(function (key) {
                        if ($(this).val() == src.data('selected') || (typeof selected != 'undefined' && $(this).val() == selected)) {
                            $(this).attr('selected', 'selected');
                        }
                    });
                    break;    

            }
            if (des.data('advancedselect') != undefined) {
                App.setSelect2(des);
            }
        },

        // normalizeRows
        normalizeRows: function (className, parentClass,removeCount) {

            var removeCount = removeCount?removeCount:1;
            var cnt = 0;
            var hiddenCheck = (hiddenCheck ? hiddenCheck : false);
            parentClass = parentClass ? ('.' + parentClass + ' ') : '';
            var rows = $(parentClass + '.' + className);
            var rowsCount = rows.length;
            rows.each(function (index, el) {
                cnt++;
                $(this).find('.addRow, .remRow, .rem, .counterName, .addBut, .remBut').each(function (i, elp) {
                    if ($(this).hasClass('addRow') || $(this).hasClass('addBut')) {
                        if (cnt < rowsCount)
                            $(this).addClass('invisible');
                        else
                            $(this).removeClass('invisible');
                    } else if ($(this).hasClass('remRow') || $(this).hasClass('rem') || $(this).hasClass('remBut')) {
                        if (rowsCount <= removeCount)
                            $(this).addClass('invisible');
                        else
                            $(this).removeClass('invisible');
                    } else if ($(this).hasClass('counterNum')) {
                        $(this).innerHTML = cnt;
                    }
                });
            });
        },

        getRandom: function() {
            return random;
        },
        
        // Duplicate Row
        duplicateRow: function (className, options, callBack) {
            var options = typeof options == 'object' ? options : {};
            
            // Clone the Row
            var obj = $('.' + className);
             
            if (!obj) alert('error: this not a object.');
            obj = $(obj[obj.length - 1]);
            obj.find('*[data-advancedselect]').select2('destroy');
            var newObj = obj.clone();
            if (options.limit) {
                if ($('.' + className).length >= parseInt(options.limit)) {
                    bootbox.alert('<div style="color: #c00;">' + App.trans('in this part, you are allowed a maximum of') + ' ' +
                        App.convertDigit(options.limit, (isRTL ? 'fa' : 'en')) + ' ' +  App.trans('rows of data record') + '</div>');
                    $('.' + className).eq(options.limit - 1).find('.addBut').remove();
                    return false;
                }
            }
            
            // Reset Values
            // Set Name and Id's
            cnt = 1;            
            $('.' + className).each(function (index, el) {
                el.lang = cnt;
                $(this).find('select, input, textarea').each(function (key, val) {
                    var strName = $(this).attr('name');
                    if (!strName) return;
                    if (options.childRow != undefined) {
                        strName = strName.replace(/(\[|\()child\d+(\]|\))/, '$1child' + cnt + '$2');
                    } else {
                        strName = strName.replace(/(\[|\()row\d+(\]|\))/, '$1row' + cnt + '$2');
                    }
                    $(this).attr('name', strName);
                    var id = $(this).attr('id');
                    if ($('#' + id).length) {
                        $(this).attr('id', $('#' + $(this).attr('name')).length ? ($(this).attr('name') + Math.round(Math.random() * 1000000)) : $(this).attr('name'));
                    } else if (id) {
                        $(this).attr('id', id);
                    }
                    if ($(this).data('advancedselect') != undefined) {
                        App.setSelect2($(this));
                    }
                });
                $(this).attr('id', 'reg_' + cnt);
                cnt++;
            });

            newObj.attr('id', 'reg_' + cnt);
            newObj.find('select, input, textarea, img, div, a, button, td').each(function (index, el) {
                if ($(this).attr('name') && options.parentRow) {
                    $(this).attr('name', $(this).attr('name').toString().replace(/(\[|\()row\d+(\]|\))/, '$1row' + cnt + '$2'));
                    $(this).attr('name', $(this).attr('name').toString().replace(/(\[|\()child\d+(\]|\))/, '$1child1$2'));
                } else if ($(this).attr('name') && options.childRow) {
                    $(this).attr('name', $(this).attr('name').toString().replace(/(\[|\()child\d+(\]|\))/, '$1child' + cnt + '$2'));
                } else if ($(this).attr('name')) {
                    $(this).attr('name', $(this).attr('name').toString().replace(/(\[|\()row\d+(\]|\))/, '$1row' + cnt + '$2'));
                }
                var id = $(this).attr('id');
                if ($('#' + id).length) {
                    $(this).attr('id', $('#' + $(this).attr('name')).length ? ($(this).attr('name') + Math.round(Math.random() * 1000000)) : $(this).attr('name'));
                } else if (id) {
                    $(this).attr('id', id);
                }
                if ($(this).hasClass('auto-increment')) {
                    if ($(this).data('zebra')) {
                        var zebra = $(this).data('zebra').split(/;/);
                        if ($(this).parent().length && zebra.length > 1) {
                            $(this).parent().css('backgroundColor', ($(this).parent().css('backgroundColor') == zebra[0].trim() ? zebra[1].trim() : zebra[0].trim()));
                        }  
                    }
                    $(this).html(App.convertDigit(((isNaN(parseInt(App.convertDigit($(this).text().trim()))) ? 0 : parseInt(App.convertDigit($(this).text().trim()))) + 1), 'fa'));
                }
                if ($(this).hasClass('remove')) $(this).remove();
                if ($(this).hasClass('tooltips')) {
                    $(this).attr('data-title', '');
                    $(this).removeAttr('data-original-title');
                }
                switch ($(this).prop('tagName').toLowerCase()) {
                    case 'select' :
                        $(this).selectedIndex = 0;
                        if ($(this).data('advancedselect') != undefined) {
                            $(this).parent().find('.select2-container').remove();
                             App.setSelect2($(this));
                        }

                        if ($(this).hasClass('reset')) {
                            if ($(this).data('advancedselect') != undefined) {
                                $(this).parent().find('.select2-container').remove();
                            }
                            $(this).find('option').eq(0).attr('selected', 'selected');
                            App.setSelect2($(this));
                        }
                        break;
                    case 'textarea':
                        $(this).val('');
                        $(this).text = null;
                        break;
                    case 'img':
                        var defaultSrc = $(this).data('src') != 'undefined' ? $(this).data('src') : null;
                        if ($(this).hasClass('img_box') || $(this).hasClass('reset')) $(this).attr('src', defaultSrc);
                        if ($(this).hasClass('delImg')) $(this).parent().remove();
                        break;                        
                    case 'div':
                        if ($(this).hasClass('fileinput-exists') && $(this).hasClass('fileinput')) {
                            $(this).removeClass('fileinput-exists')
                                .addClass('fileinput-new');
                        }
                        if ($(this).hasClass('fileinput-preview')) {
                            $(this).data('icon', $(this).data('reseticon'));
                        }
                        if ($(this).data('resetvalue')) $(this).html($(this).data('resetvalue')); 
                    case 'td':
                        if ($(this).hasClass('reset')) $(this).empty();
                        break;   
                    case 'a':
                        if ($(this).hasClass('reset')) $(this).attr('href', 'javascript:void(0);');
                        break;
                    case 'input':
                        var resetVal = $(this).data('resetvalue') ? $(this).data('resetvalue') : '';
                        if ($(this).hasClass('reset')) $(this).val(resetVal);
                        if (!$(this).hasClass('no-reset')) {
                            if ($(this).prop('type') == 'text' && !$(this).hasClass('display-rank')) $(this).val(resetVal);
                            if ($(this).prop('type') == 'number' && !$(this).hasClass('display-rank')) $(this).val(resetVal);
                            if ($(this).prop('type') == 'email') $(this).val(resetVal);
                            if ($(this).prop('type') == 'tel') $(this).val(resetVal);
                            if ($(this).prop('type') == 'date') $(this).val(resetVal);
                            if ($(this).prop('type') == 'datetime') $(this).val(resetVal);
                            if ($(this).prop('type') == 'file') $(this).val(resetVal);
                            if ($.inArray($(this).prop('type'), ['checkbox', 'radio']) != -1 ) {
                                $(this).checked = false;
                                $(this).prop('checked', false);
                                $(this).removeAttr('checked');
                                $(this).parent().removeClass('active').removeClass('checked');
                                if ($(this).closest('.checker').length) {
                                    var cp = $(this).clone();
                                    var parent = $(this).closest('.checker').parent();
                                    $(this).closest('.checker').remove();
                                    parent.prepend(cp);
                                }
                            }

                            if ($(this).prop('type') == 'hidden' && $(this).hasClass('reset')) $(this).val(resetVal);
                            if ($(this).prop('type') == 'radio' && $(this).parent().length)
                                $(this).parent().html($(this).parent().html().replace(/(\[|\()row\d+(\]|\))/, '$1row' + cnt + '$2'));
                                $(this).parent().removeClass('active');
                        }
                        if ($(this).hasClass('display-rank')) {
                            var newRank = parseInt($(this).val()) - 10;
                            $(this).val(newRank > 0 ? newRank : 0);
                        }
                        break;
                }
            });

            newObj.find('.imagebox, remPic, .fileDel, .mceEditor').each(function (index, el) {
                $(this).remove();
            });

            newObj.find('.inVal').each(function (index, el) {
                $(this).html('');
            })

            newObj.find('textarea.tinymce_editor').each(function (index, el) {
                $(this).attr('id', null);
                el.css({
                    display: 'inline',
                    overflow: 'scroll'
                });
            });
            if (obj.hasClass('addRow')) obj.children('.addRow').css('visibility', 'hidden');
            newObj.removeClass('hidden');
            var normalizeClass = '';
            if (options.removeOtherClass) {
                var firstRemoveIndex = options.firstRemoveIndex ? options.firstRemoveIndex : 1;
                newObj.find('.' + options.removeOtherClass).each(function(kOther) {
                    var regExp = new RegExp(options.removeOtherClass + '(\\d+)' );
                    var matchNum = $(this).attr('class').match(regExp);
                    if (matchNum) {
                        var currentNum = (isNaN(parseInt(matchNum[1])) ? (Math.random() * 5000) : parseInt(matchNum[1]))
                        var classNum =  currentNum + 1;
                        normalizeClass = (options.removeOtherClass + classNum);
                        $(this).data('normalizeclass', normalizeClass);
                        $(this).attr('class', $(this).attr('class').replace(regExp, normalizeClass));
                        $(this).find('.addBut').attr('onclick', $(this).find('.addBut').attr('onclick').replace((options.removeOtherClass + currentNum), normalizeClass));
                        $(this).find('.remBut').attr('onclick', $(this).find('.remBut').attr('onclick').replace((options.removeOtherClass + currentNum), normalizeClass));
                    }
                    if (kOther >= firstRemoveIndex) $(this).remove();
                });
            }
            newObj.insertAfter((options.insertAfterObject ? (options.insertAfterObject.length ? options.insertAfterObject : obj) : obj));
            App.normalizeRows((normalizeClass ? normalizeClass : className));
            App.initAjax();

            if (typeof(tconf) == 'object') tiny_init(tconf);

            $.each(options, function(key, val) {
                if (key == 'functionList') {
                    var fnList = val.split(';');
                    $.each(fnList, function(key, fn) {
                        eval(fn);
                    });
                }
            });
            if (typeof callBack == 'function') {
                callBack(newObj);
            }

            newObj.find('[required], [data-rule-length], [data-rule-required], [data-rule-number], [data-rule-min], [data-rule-max]').each(function() {
                $(this).removeAttr('aria-describedby')
                    .removeAttr('aria-invalid')
                    .removeClass('error');
                $(this).closest('.form-group')
                    .removeClass('has-error')
                    .removeClass('has-success')
                    .find('label')
                    .remove();
            });

            return newObj;
        },

        // remove Row
        removeRow: function (obj, parentClass, removeAddress, removeId , removeCount) {
            var obj = $(obj);
            if (!obj.length) return false;
            obj.closest('.' + parentClass).remove();
            if (removeAddress && removeId) {
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: removeAddress,
                    data: {id: removeId}
                });
            }
            var removeCount = removeCount?removeCount:1;
            App.normalizeRows((obj.data('normalizeclass') ? obj.data('normalizeclass') : parentClass),null,removeCount);

        },

        // remove File
        removeFile: function (obj, e, type, callback) {
            e.preventDefault();
            var obj = $(obj);
            var type = type ? type : '';
            if (!obj) return;
            var url = obj.attr('href') ? obj.attr('href') : obj.data('url');
            var fileIcon = obj.closest('.fileinput').find('.fileinput-icon');
            var preview = obj.closest('.fileinput').find('.fileinput-preview');
            var iconUrl = preview.data('icon') ? preview.data('icon') : '/images/noimage.gif';
            if (!url) return;
            App.confirm(App.trans('are you sure want to delete this file'), function(res) {
                if (res) {
                    $.ajax({
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: url,
                        data: {'type': type},
                        success: function(response) {
                            if (callback) {
                                callback(response);
                            }
                            fileIcon.html('<img src="' + iconUrl + '" alt="" />');
                            obj.remove();
                            $('.edit-frame').contents().find('html').html(response);
                        }
                    });
                }
            }, 'warning');
        },

        //## ajax pagination
        pagination: function () {                
            $('.pagination li a, .pagination li span').each(function() {
                var lang = (window.dir == 'rtl' ? 'fa' : 'en');
                $(this).html(App.convertDigit($(this).html(), lang));
            });
            $('.pagination li a').off('click').click(function (e) {
                var pageMatch = $(this).attr('href').match(/page\s*=\s*(\d+)/);
                if (pageMatch !== null) {
                    e.preventDefault();
                    listCurrentPage = pageMatch[1];
                    var url = $(this).attr('href').replace(/\/\?/, '?');
                    App.list({url: url});
                }
            });
        },

        //## get list data
        list: function (options) {     
            var settings = {
                data: {},
                element: $('.content-data-list.data-list').closest(".portlet").children(".portlet-body"),
                resultObject: ((options != undefined && options.element != undefined) ? (options.element.find('.data-list').length ? options.element.find('.data-list') : options.element) : $('.content-data-list.data-list')),
                url: document.location.href,
                error: false
            }; 

            var options = $.extend(settings, options);

            var parent = options.element.closest('.portlet-form').length ? options.element.closest('.portlet-form') : options.element.closest('.portlet');
            var data = (!$.isEmptyObject(options.data) ? options.data : parent.find('input, select, textarea').serializeObject());
            
            options.page = (typeof options.page != 'undefined' ? options.page : (options.search ? 1 : listCurrentPage));
            if (typeof data == 'object') {
                data.page = options.page;
                data._t = $.now();
            }

            if (typeof options.search != 'undefined' && options.search) {
                listCurrentPage = 1;
            }

            if ($('input.knob.per-page').length) {
                $.cookie('perPage', $('input.knob.per-page').val(), { expires: 30, path: '/' + (section ? section : '')});
            }
           
            if (options.url) {
                App.blockUI({target: options.element, iconOnly: true});
                $.ajax({
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    cache: false,
                    url: options.url,
                    data: data,
                    dataType: "html",
                    success: function (res) {
                        App.unblockUI(options.element);
                        options.resultObject.html(res);
                        App.initAjax();
                        if (parent.find('.query-order').length ) {
                            parent.find('.query-order').each(function() {
                                var fld = $(this).data('fld');
                                if ($(this).val().toUpperCase() == 'DESC') {
                                    parent.find('.sortable[data-fld="' + fld + '"]')
                                        .removeClass('sorting_asc')
                                        .addClass('sorting_desc');
                                } else {
                                    parent.find('.sortable[data-fld="' + fld + '"]')
                                        .removeClass('sorting_desc')
                                        .addClass('sorting_asc');
                                }
                            });
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        App.unblockUI(options.element);

                        if (xhr.statusText != 'abort') {
                            var msg = '';//App.trans('error on reloading the content. please check your connection and try again.');
                            if (xhr.responseText) {
                                var err =  JSON.parse(xhr.responseText);
                                msg = xhr.status + ': ' + xhr.statusText +  "\n";
                                $.each(err.error, function(key, val) {
                                    msg += key + ': ' + val + "\n";
                                });
                            }
                            if (msg) {
                                if (options.error == "toastr" && toastr) {
                                    toastr.error(msg);
                                } else if (options.error == "notific8" && $.notific8) {
                                    $.notific8('zindex', 11500);
                                    $.notific8(msg, {theme: 'ruby', life: 3000});
                                } else {
                                    alert(msg);
                                }
                            }
                        }
                    }
                });
            } else {
                // for demo purpose
                App.blockUI({target: options.element, iconOnly: true});
                windoliw.setTimeout(function () {
                    App.unblockUI(options.element);
                }, 1000);
            }
        },

        //sort list of data
        sortList: function (object, options) {
            var obj = $(object);
            var parent = obj.closest('.portlet-form').length ? obj.closest('.portlet-form') : obj.closest('.portlet');
            if (!obj.length || !parent.length) return false;

            var fld = obj.data('fld');
            if (typeof fld == 'undefined' || !fld) return false;

            var inp = parent.find('.query-order[data-fld="' + fld + '"]');
            if (inp.length) {
                inp.val(inp.val() == 'DESC' ? 'ASC' : 'DESC');
            } else if (obj.closest('.data-list').length) {
                parent.find('.query-order').remove();
                parent.append('<input class="query-order" type="hidden" name="q[order][' + fld + ']" data-fld="' + fld + '" value="ASC" />');
            }

            App.list(options);
        },

        //## show password
        showPassword: function(obj) {
            return false;
        },

        //## translate a message
        trans: function(msg) {
            var $msg = msg;
            if (typeof translate == 'object') {
                $.each(translate, function(index, value) {
                    if (index == msg) {
                        $msg = value;
                    }
                })
            }
            return $msg;
        },

        //## call method server side language with ajax
        call: function(obj, options, e) {
            if (typeof e != 'undefined') {
                e.preventDefault();
            }
            var obj = obj ? $(obj) : $(e.target);
            if (!obj.length) return false;
            var settings = {
                sendform: false,
                url: (obj.attr('action') != undefined ? obj.attr('action') : (obj.attr('href') != undefined ? obj.attr('href') : (obj.data('url') != undefined ? obj.data('url') : ''))),
                args: {
                    "_t": ((typeof e != 'undefined' && typeof e.timeStamp != 'undefined') ? e.timeStamp : $.now()),
                    "event": ((typeof e != 'undefined') ? serializeEvent(e) : '')
                },
                data: {},
            };

            var options = $.extend(settings, options);
            
            //## selected list btn
            $('.list-btn').closest('tr').removeClass('info');
            obj.closest('.list-btn').closest('tr').addClass('info');

            if (options.sendform) {
                var frmData = obj.find('input[name], select[name], textarea[name]').serializeObject();
                options.args = $.extend(options.args, frmData);
            }

            if (options.method == 'delete') {
                App.confirm(App.trans('are you sure want to delete this record'), function(res) {
                    if (res) {
                        handleRequest(obj, options);
                    }
                }, 'warning');
            } else {  
                if (!options.noloading)              
                    $('body').modalmanager('loading');
                handleRequest(obj, options);
            }
        },

        jstree: function(destenation, options) {

            var options = options ? options : {};
            var dest = destenation ? destenation : '#jstree';
            if (!$(dest).length) {
                dest = '#' + destenation;
                if (!$(dest).length) dest = '.' + destenation;
            }

            var defaultoptions = {
                "core" : {
                    "themes" : {
                        "responsive": false
                    }, 
                    // so that create works
                    "check_callback" : true,
                    'data': []
                },
                "contextmenu" : {
                    'items' : function(node) {
                        var tmp = $.jstree.defaults.contextmenu.items();
                        delete tmp.create.action;
                        tmp.create.label = App.trans("create");
                        tmp.rename.label = App.trans("rename");
                        tmp.remove.label = App.trans("remove");
                        tmp.ccp.label = App.trans("edit");
                        tmp.ccp.submenu.cut.label = App.trans("cut");
                        tmp.ccp.submenu.copy.label = App.trans("copy");
                        tmp.ccp.submenu.paste.label = App.trans("paste");
                        tmp.create.submenu = {
                            "create_folder" : {
                                "separator_after" : true,
                                "label" : App.trans("subgroup"),
                                "action" : function (data) {
                                    var inst = $.jstree.reference(data.reference),
                                    obj = inst.get_node(data.reference);
                                    inst.create_node(obj, { type : "default" }, "last", function (new_node) {
                                        setTimeout(function () { inst.edit(new_node); },0);
                                    });
                                }
                            },
                            "create_file" : {
                                "label" : App.trans("final subgroup"),
                                "action" : function (data) {
                                    var inst = $.jstree.reference(data.reference),
                                    obj = inst.get_node(data.reference);
                                    inst.create_node(obj, { type : "file" }, "last", function (new_node) {
                                        setTimeout(function () { inst.edit(new_node); },0);
                                    });
                                }
                            }
                        };
                    if(this.get_type(node) === "file") {
                        delete tmp.create;
                    }
                        return tmp;
                    }
                }, 
                "types" : {
                    "default" : {
                        "icon" : "fa fa-folder icon-success icon-lg"
                    },
                    "file" : {
                        "icon" : "fa fa-file icon-warning icon-lg"
                    }
                },
                "state" : { "key" : "demo2" },
                "plugins" : [ "contextmenu", "dnd", "state", "types" ]
            };

            $("*[data-jstree]" + (dest ? ',' + dest : '')).each(function(index, el) {
                options = options ? options : $(this).data['jstree'];
                options = $.extend({}, defaultoptions, options);
                $(this).jstree(options);
            });
        },

        setSelect2: function (obj, data) {
            var data = data ? data : [];
            if (!obj) {
                var obj = $(obj);
                if (!obj) return false;
            }
            var options = {};
            switch (typeof obj.data('advancedselect')) {
                case 'string':
                    if (obj.data('advancedselect').match(/^{[^}]*}$/)) {
                        options = $.parseJSON(obj.data('advancedselect').replace(/\'/g, '"'));
                    }
                    break;
                case 'object':
                    options = obj.data('advancedselect');
                    break;
            }
            if (typeof options != 'object') return false;
            options = $.extend({}, options);
            if (!obj.data('loadmore')) {
                obj.select2(options);
            } else {
                obj.find('option').each(function (key, value) {
                    data.push({id: "" + $(this).val(), text: "" + $(this).text().trim()})
                });
                var inputClass = "select2_" + Math.round(Math.random() * 10000);
                obj.after('<input type="text" class="form-control product-select ' + inputClass + '">');
                obj.remove();
                $('input.' + inputClass).select2({
                    initSelection: function (element, callback) {
                        var selection = _.find(data, function (metric) {
                            return metric.id === element.val();
                        })
                        callback(selection);
                    },
                    query: function (options) {
                        var pageSize = 100;
                        var startIndex = (options.page - 1) * pageSize;
                        var filteredData = data;

                        if (options.term && options.term.length > 0) {
                            if (!options.context) {
                                var term = options.term.toLowerCase();
                                options.context = data.filter(function (metric) {
                                    return ( metric.text.toLowerCase().indexOf(term) !== -1 );
                                });
                            }
                            filteredData = options.context;
                        }

                        options.callback({
                            context: filteredData,
                            results: filteredData.slice(startIndex, startIndex + pageSize),
                            more: (startIndex + pageSize) < filteredData.length
                        });
                    },
                    placeholder: App.trans('Select') + '...',
                    minimumInputLength: 5
                });
            }

        },

        //## create loading overlay
        createOverlay: function(obj, options) {
            var defaultOptions =  {
                overlay: {
                    'active': false,
                    'css': {
                        'text-align' : 'center'
                    },
                    'resultObject' : false
                }, 
                loading: {
                    src: '/images/loading/loading-' + window.dir + '.gif',
                    alt: App.trans('searching'),
                }
            };
            if (typeof options != 'object') {
                options = {overlay: {active: true}};
            }
            var options = $.extend(defaultOptions, options);
            
        
            var loadingStyles = overlayStyles = '';
            if (!$.isEmptyObject(options.loading.css)) {
                $.each(options.loading.css, function(key, val) {
                    loadingStyles += (key + ': ' + val + '; ');
                });
            }
            if (!$.isEmptyObject(options.overlay.css)) {
                $.each(options.overlay.css, function(key, val) {
                    overlayStyles += (key + ': ' + val + '; ');
                });
            }
            
            if (options.overlay.active) {              
                obj.data('position', obj.css('position'));
                obj.css('position', 'relative');
                $('.block-overlay').fadeOut(function() {
                    $('.block-overlay').remove();
                });
                obj.append('<div class="block-overlay" style="' + overlayStyles + '">' +
                    (options.loading && !$.isEmptyObject(options.loading) ? ('<img  src="' + options.loading.src + 
                        '" alt="' + options.loading.alt + '..." />') : '') + '</div>');
                $('.block-overlay').fadeIn();  
            } else {
                $('.loading').remove();
                obj.after('<div class="loading" style="text-align: center;">' +
                    (options.loading && !$.isEmptyObject(options.loading) ? ('<img src="' + options.loading.src + '" style="' + loadingStyles  
                    + '" alt="' + options.loading.alt + '..." />') : '') + '</div>');            
            }           
        },

        //## remove overlay box
        removeOverlay: function(obj) {
            if ($('.block-overlay').length) {                
                obj.css('position', obj.data('position'));
                $('.block-overlay').fadeOut(function() {
                    $('.block-overlay').remove();
                });
            } else {
                $('.loading').remove();
            }
        },

        //## ajax request
        ajaxRequest: function(object, options, callBack, callBackError) {
            var obj = $(object);
            if (!obj.length && typeof object == 'string') {
                obj = $('.' + object);
                if (!obj.length) return;
            }
            
            var defaultOptions = {
                request: {},
                type: "POST",
                dataType: "html",
                showSuccessMessages: true,
                showErrorMessages: true,
                alert: false,
                toastr: false,
                url: null,
                resultObject: (obj.data('result') ? ('.' + obj.data('result')) : 'result'),
                cache: false,
                noResult: false,
                overlay: {
                    active: false,
                    css: {
                        'text-align' : 'center'
                    } ,
                    resultObject : false
                }, 
                loading: {
                    src: '/images/loading/loading-' + window.dir + '.gif',
                    alt: App.trans('searching'),
                    position: 'bottom'
                }
            };
            if (typeof options == 'string') {
                if (options.match(/^{[^}]*}$/)) {
                    options = $.parseJSON(options.replace(/\'/g, '"'));
                }
            }
            options = $.extend(defaultOptions, options);

            options.url = (options.url ? options.url : (obj.data('url') ? obj.data('url') : (obj.attr('action') ? obj.attr('action') : '')));
            options.url = (options.url ? options.url : obj.attr('href'));
            if (options.url == undefined || options.url.match(/javascript\:/)) {
                console.log('url undefined.');
                return;
            }
            
            options.parentObject = (options.parentObject ? options.parentObject : 'div');
            if (!options.url) {
                toastr.error('please set data-url or href or action for element');
            }

            if (typeof ajax_request !== 'undefined') {
                ajax_request.abort();
            }
            var formObj = (obj.data('form') ? $(obj.data('form')) : obj);

            if (typeof options.request == 'object'){
                options.request = ((Object.keys(options.request).length !== 0) ? options.request : (obj.find('input[name], select[name], textarea[name]').serialize()));
            } else {
                options.request = (options.request ? options.request : (formObj.find('input[name], select[name], textarea[name]').serialize()));
            };
            var resObj = (options.resultObject ? $(options.resultObject) : $('.ajax-reuslt'));
            if (!resObj.length && options.resultObject) {
                resObj = $('.' + options.resultObject);
            }

            var overlayBox = obj;
            if (options.overlay.resultObject) {
                if (options.overlay.resultObject == true) {
                    overlayBox = resObj;
                } else if (typeof options.overlay.resultObject == 'object') {
                    overlayBox = options.overlay.resultObject;
                    if (!overlayBox.length) overlayBox = $(overlayBox);
                } else {
                    overlayBox = $('.' + options.overlay.resultObject).length ? $('.' + options.overlay.resultObject) : $('#' + options.overlay.resultObject);
                }
            }

            if (options.overlay.active) {
                App.createOverlay(overlayBox, options);                
            } else if (!$.isEmptyObject(options.loading)) {                             
                App.createOverlay(obj, options);
            }

            if (typeof ajaxRequest !== 'undefined' && options.abort) {
                ajaxRequest.abort();
            }

            ajaxRequest = $.ajax({
                type: (options.sendMethod ? options.sendMethod: options.type),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: options.cache,
                data: (options.data != undefined ? options.data : options.request),
                url: options.url,
                dataType: options.dataType,
                success: function (res) { 
                    if (options.overlay.remove == undefined || options.overlay.remove) {
                        if (options.overlay.active) {
                            App.removeOverlay(overlayBox); 
                        } else if (!$.isEmptyObject(options.loading)) {
                            App.removeOverlay($('.loading')); 
                        }
                    }

                    if (!options.noResult) {
                        try {
                            res = JSON.parse(res);
                            var msgType = res.type ? ('alert-' + (res.type == 'error' ? 'danger' : res.type)) : 'alert-warning';
                               
                            var msg = "<div class='alert " + msgType +" display-hide' style='display: block;'>" +
                                          "<button data-close='alert' class='close'></button><span>" + res.message + "</span></div>";
                            if ((options.alert || res.alert) && options.showSuccessMessages) {
                                bootbox.alert(msg);
                            } else if ((options.toastr || res.toastr) && options.showSuccessMessages) {
                                switch(res.type) {
                                    case 'success':
                                        toastr.success(res.message);
                                        break;
                                    case 'error':
                                        toastr.error(res.message);
                                        break;
                                    case 'warning':
                                        toastr.warning(res.message);
                                        break;
                                    default:
                                        toastr.info(res.message);
                                        break;
                                }
                            } else if (options.showSuccessMessages && res.type) {
                                resObj.html(msg).fadeIn();
                            }
                            
                        } catch (e) {
                            resObj.fadeIn(function() {
                                resObj.html(res);
                            });
                        }
                        App.initAjax();
                    }

                    if (callBack) {
                        callBack(res);
                        App.initAjax();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    if (options.overlay.active) {
                        App.removeOverlay(overlayBox); 
                    } else {
                        App.removeOverlay($('.loading')); 
                    }

                    if (xhr.statusText != 'abort' && xhr.status != 422) {
                        handleAjaxError(xhr, ajaxOptions, thrownError);
                    }

                    if (typeof callBackError == 'function') {
                        callBackError(xhr, ajaxOptions, thrownError);
                    }
                }
            });

        },
        
        //## handle multiple file upload
        multipleFileUpload: function(object) {
            var assets = [            
                '/assets/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css',           // jquery file upload blueimp
                '/assets/plugins/jquery-file-upload/css/jquery.fileupload.css',                         // jquery file upload
                '/assets/plugins/jquery-file-upload/css/jquery.fileupload-ui.css',                      // jquery ui file upload
                
                //jquery file upload
                '/assets/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js',                     // jquery ui file upload widget
                '/assets/plugins/jquery-file-upload/js/vendor/load-image.min.js',                       // The Load Image plugin is included for the preview images and image resizing functionality
                '/assets/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js',                   // The Canvas to Blob plugin is included for image resizing functionality
                '/assets/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js',     //  blueimp Gallery script
                '/assets/plugins/jquery-file-upload/js/jquery.iframe-transport.js',                     // The Iframe Transport is required for browsers without support for XHR file uploads
                '/assets/plugins/jquery-file-upload/js/jquery.fileupload.js',                           // The basic File Upload plugin 
                '/assets/plugins/jquery-file-upload/js/jquery.fileupload-process.js',                   // The File Upload processing plugin
                '/assets/plugins/jquery-file-upload/js/jquery.fileupload-image.js',                     // The File Upload image preview & resize plugin
                '/assets/plugins/jquery-file-upload/js/jquery.fileupload-audio.js',                     // The File Upload audio preview plugin
                '/assets/plugins/jquery-file-upload/js/jquery.fileupload-video.js',                     // The File Upload video preview plugin
                '/assets/plugins/jquery-file-upload/js/jquery.fileupload-validate.js',                  // The File Upload validation plugin
            ];

            var obj = $(object);
            if (!obj.length) {
                obj = ($('.' + object).length ? $('.' + object) : $('#' + object));
                if (!obj.length) return;
            }
            if (typeof obj.data('url') == 'undefined') {                
                console.log('data-url not set on file object.');
                return;
            }
            if (typeof obj.data('deleteurl') == 'undefined') {
                console.log('data-deleteurl not set on file object.');
            }
            var showImage = ((obj.data('show-image') != undefined && obj.data('show-image') == true) ? true : false);
            importAssets(assets, function() {

                obj.closest('div').after('<table class="table table-striped table-file-upload margin-top-10" role="presentation"><tbody class="files"></tbody></table>');
                var jqXHR = {};
                setTimeout(function() {
                    try {
                        obj.fileupload({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            autoUpload: true,
                            dataType: 'html',
                            add: function (e, data) {
                                var reader = new FileReader();
                                var file = data.files[0];
                                var fileSize = (file.size < 1024 ? (file.size / 1024).toFixed(2) : Math.round(file.size / 1024));
                                fileSize = (lang == 'fa' ? fileSize.toString().replace(/\./, '/') : fileSize);
                                var rnd = Math.round(Math.random() * 10000);
                            
                                if (obj.data('max-size') != undefined && obj.data('max-size') < file.size) {
                                    toastr.error(App.trans('the file size exceeds the max upload restrict'));
                                    return false;
                                }
                                if (obj.data('accept-types') != undefined) {
                                    var match = obj.data('accept-types').match(new RegExp('^/(.*?)/([gimy]*)$'));
                                    var regex = new RegExp(match[1], match[2]);
                                    if (!regex.test(file.type)) { 
                                        var typeMsg = (obj.data('msg-type') != undefined ? obj.data('msg-type') : App.trans('only image files (JPG, GIF, PNG) are allowed'));
                                        toastr.error(typeMsg);
                                        return false;
                                    }
                                }
                                var fileDetails = '<tr class="template-upload fade in"><td class="name" width="30%"><span class="file-name">' + file.name + '</span></td>'+
                                    '<td class="size" width="40%"><span>' + App.convertDigit(fileSize, lang) + ' ' + App.trans('kb') + '</span></td>';
                                if (file.error) {
                                    fileDetails += '<td class="error" width="20%" colspan="2"><span class="label label-danger">Error</span>' + file.error + '</td>';
                                } else {
                                    fileDetails += '<td class="td-progress">' +
                                    '<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">' +
                                    '<div class="progress-bar progress-bar-success" style="width:0%;"></div></div></td>';
                                }
                                fileDetails += '<td width="10%" align="right"><button class="btn btn-sm red cancel"><i class="fa fa-ban"></i>' +' <span>'+ App.trans('cancel') + '</span></button></td></tr>';
                                data.context = $(fileDetails).appendTo('.table-file-upload tbody.files');

                                //## show image
                                if (showImage) {
                                    var image = new Image();
                                    reader.onload = function(event) {
                                        image.src = event.target.result;
                                        if (event.target.result) {
                                            var imageWidth = 128;
                                            image.onload = function() {
                                                imageHeight = this.height;
                                                if (this.width >= 128) {
                                                    imageHeight = imageWidth * this.height / this.width;
                                                } else {
                                                    imageWidth = this.width;
                                                }
                                                var objFileName = data.context.find('.file-name');
                                                objFileName.after('<img src="" alt="" title=""/>');
                                                var img = data.context.find('img');

                                                objFileName.remove();
                                                if (obj.data('min-width') != undefined && this.height < obj.data('min-width')) {
                                                    toastr.error(App.trans('the width of the image is less than the minimum allowable width'));
                                                    data.context.remove();
                                                    return false;
                                                }
                                                if (obj.data('min-height') != undefined && this.height < obj.data('min-height')) {
                                                    toastr.error(App.trans('the height of the image is less than the minimum allowable height'));
                                                    data.context.remove();
                                                    return false;
                                                }
                                                img.attr('width', imageWidth).attr('height', imageHeight);
                                                img.attr('src', event.target.result).attr('alt', file.name).attr('title', file.name);
                                            }
                                        }
                                    };
                                    reader.readAsDataURL(file);
                                }
                                jqXHR[rnd] = data.submit();

                                $(data.context).find('.cancel').off('click').click(function() {
                                    jqXHR[rnd].abort();
                                    $(this).closest('tr').fadeOut(function() {
                                        $(this).remove();
                                    });
                                });
                            },
                            progress: function(e, data) {
                                var file = data.files[0];
                                var progress = parseInt(data.loaded / data.total * 100, 10);
                                var progressBar = $(data.context).find('.progress.active');
                                progressBar.find('.progress-bar').addClass('text-center').css('width', progress + '%').text(App.convertDigit(progress, lang) + '%');
                            }, 
                            done: function(e, data) {
                                var file = data.files[0];
                                var fileId = 0;
                                if (typeof data.result == 'string') {
                                    try {
                                        var jsonResult = $.parseJSON(data.result);
                                        fileId = jsonResult.id;
                                    } catch (e) {}
                                }
                                var deleteURL = ($(this).data('deleteurl') ? ($(this).data('deleteurl')) : '');
                                var deleteQS = '';
                                if (deleteURL) {
                                    if (deleteURL.match(/\?.*/) != null) {
                                        deleteQS = deleteURL.match(/\?.*/)[0];
                                    }
                                    deleteURL = deleteURL.replace(/\?.*/, '');  
                                    deleteURL += ('/' + fileId + deleteQS);
                                } 
                                $(data.context).find('.cancel').parent().append('<button class="btn default btn-sm delete"'+
                                    'data-url="' + deleteURL + '"><i class="fa fa-times"></i></button>');
                                $(data.context).find('.cancel').remove();
                                $(data.context).find('.td-progress').fadeOut();
                                $(data.context).find('.delete').off('click').click(function() {   
                                    var btnDelete = $(this);             
                                    var options = {noResult: true, resultObject: btnDelete.closest('tr')};                    
                                    if (deleteURL.match(/javascript:void/i) != null) {
                                        btnDelete.closest('tr').fadeOut(function() {
                                            btnDelete.closest('tr').remove();
                                        });
                                    } else {
                                        if (!deleteURL || typeof deleteURL == 'undefined') {
                                            bootbox.alert('data-deleteurl not set on file object.');
                                        } else {                          
                                            App.ajaxRequest($(this), options, function() {
                                                multipleUpload = {};
                                                btnDelete.closest('tr').fadeOut(function() {
                                                    btnDelete.closest('tr').remove();
                                                });
                                            }); 
                                        }
                                    }
                                });
                                $('body').append('<div class="hidden">' + data.result + '</div>');
                                
                            },
                            success: function(res, data) {
                                try {
                                    res = JSON.parse(res);
                                    var msgType = res.type ? ('alert-' + (res.type == 'error' ? 'danger' : res.type)) : 'alert-warning';
                                       
                                    var msg = "<div class='alert " + msgType +" display-hide' style='display: block;'>" +
                                                  "<button data-close='alert' class='close'></button><span>" + res.message + "</span></div>";
                                    if (res.alert != undefined && res.toastr == undefined) {
                                        bootbox.alert(msg);
                                    } else if (res.toastr != undefined) {
                                        switch(res.type) {
                                            case 'success':
                                                toastr.success(res.message);
                                                break;
                                            case 'error':
                                                toastr.error(res.message);
                                                break;
                                            case 'warning':
                                                toastr.warning(res.message);
                                                break;
                                            default:
                                                toastr.info(res.message);
                                                break;
                                        }
                                    }  
                                    if (res.callBack != undefined) {
                                        res.callBack();
                                    }                                  
                                } catch (e) {
                                    console.log(res);
                                }
                            },
                            fail: function(e, data) {
                                if (data.errorThrown != 'abort')
                                    console.log(data.jqXHR);
                            }
                        });
                    } catch(e) {
                        console.log(e);
                    }
                }, 50);
            });

        },

        //## confirm with bootbox
        confirm: function(msg, callBack, type) {
            var msg = msg != undefined ? msg : '';
            if (type != undefined) {
                var color = 'orange';
                color = (type == 'error' ? '#ED4E2A' : (type == 'success' ? '#3CC051' : color));
                msg = '<div style="margin-top: 15px;" class="note note-' + type + '"><i style="color: ' + color + '" class="fa fa-' + type + '"></i> ' + msg + '</div>';
            }
            bootbox.confirm({
                buttons: {
                    confirm: {
                        label: App.trans('confirm'),
                    },
                    cancel: {
                        label: App.trans('cancel'),
                    }
                },
                message: msg, 
                callback: function(result) {
                    if (result && typeof callBack == 'function') {
                        callBack(result);
                    }
                }
           });
        },

        validateForm: function(frm, options) {
            var form = $(frm);

            if (!form.length && frm.length)  {
                form = frm;
            } else if (!form.length) {
                return false;
            }
            if (lang == 'fa') {
                jQuery.extend(jQuery.validator.messages, {
                    maxlength: jQuery.validator.format("لطفا بیشتر از {0} کاراکتر وارد کنید."),
                    minlength: jQuery.validator.format("لطفا حداقل {0} کاراکتر وارد کنید."),
                    rangelength: jQuery.validator.format("لطفا یک مقدار بین {0} و {1} کاراکتر وارد کنید."),
                    range: jQuery.validator.format("لطفا یک مقدار بین {0} و {1} وارد کنید."),
                    max: jQuery.validator.format("لطفا یک مقدار کمتر یا مساوی {0} وارد کنید."),
                    min: jQuery.validator.format("لطفا یک مقدار بیشتر یا مساوی {0} وارد کنید.")
                });
            }
            var settings = {
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                errorPlacement: function (error, element) { // render error placement for each input type
                    error.insertAfter(element); // for other inputs, just perform default behavior
                },

                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label.closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label.addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                    $(form).off('submit').submit();
                }
            };
            var options = $.extend(settings, options);
            
            form.validate(options);            
        },

        //## handle change password
        changePassword: function(frm) {
            var form = $(frm);
            if (!form.length && frm.length)  {
                form = frm;
            } else if (!form.length) {
                return false;
            }

            if (lang == 'fa') {
                jQuery.extend(jQuery.validator.messages, {
                    maxlength: jQuery.validator.format("لطفا بیشتر از {0} کاراکتر وارد کنید."),
                    minlength: jQuery.validator.format("لطفا حداقل {0} کاراکتر وارد کنید."),
                    rangelength: jQuery.validator.format("لطفا یک مقدار بین {0} و {1} کاراکتر وارد کنید."),
                    range: jQuery.validator.format("لطفا یک مقدار بین {0} و {1} وارد کنید."),
                    max: jQuery.validator.format("لطفا یک مقدار کمتر یا مساوی {0} وارد کنید."),
                    min: jQuery.validator.format("لطفا یک مقدار بیشتر یا مساوی {0} وارد کنید.")
                });
            }

            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    password: {
                        required: true
                    },
                    newpassword: {
                        minlength: 6,
                        required: true
                    },
                    retypepassword: {
                        minlength: 6,
                        required: true,
                        equalTo: "#new-password"
                    },
                },
               

                messages: {
                    password: {
                        required: App.trans("current password is required")
                    },
                    newpassword: {
                        required: App.trans("new password is required")
                    },
                    retypepassword: {
                        required: App.trans("re-type new password is required"),
                        equalTo: App.trans('password and confirmation do not match')
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    $('.alert-danger', form).show();
                },

                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },

                errorPlacement: function (error, element) {
                    error.insertAfter(element.closest('.input-icon'));
                },

                submitHandler: function (form) {
                    App.ajaxRequest(form, {overlay: {active: true}, resultObject: $('.change-password-result')});
                    return false;
                }
            });
        },

        //main function to initiate the theme
        init: function () {
            //IMPORTANT!!!: Do not modify the core handlers call order.

            App.pagination();
            initDatePicker();
            initKnob(true);
            handleListExpand();
            setTimeout(handleTextarea, 50);
            setTimeout(shiftcheckbox, 50);

            //core handlers
            handleInit(); // initialize core variables
            handleResponsiveOnResize(); // set and handle responsive    
            handleUniform(); // hanfle custom radio & checkboxes
            handleBootstrapSwitch(); // handle bootstrap switch plugin
            handleScrollers(); // handles slim scrolling contents 
            handleResponsiveOnInit(); // handler responsive elements on page load

            //layout handlers
            handleFixedSidebar(); // handles fixed sidebar menu
            handleFixedSidebarHoverable(); // handles fixed sidebar on hover effect
            handleSidebarMenu(); // handles main menu
            handleHorizontalMenu(); // handles horizontal menu
            handleSidebarToggler(); // handles sidebar hide/show
            handleFixInputPlaceholderForIE(); // fixes/enables html5 placeholder attribute for IE9, IE8
            handleGoTop(); //handles scroll to top functionality in the footer
            handleTheme(); // handles style customer tool

            //ui component handlers
            handleFancybox() // handle fancy box
            handleSelect2(); // handle custom Select2 dropdowns
            handlePortletTools(); // handles portlet action bar functionality(refresh, configure, toggle, remove)
            handleAlerts(); //handle closabled alerts
            handleDropdowns(); // handle dropdowns
            handleTabs(); // handle tabs
            handleTooltips(); // handle bootstrap tooltips
            handlePopovers(); // handles bootstrap popovers
            handleAccordions(); //handles accordions 
            handleModals(); // handle modals
            handleFullScreenMode(); // handles full screen
            setTimeout(function() {
                $('.phpdebugbar-body').height(300);
            }, 100);
        },

        //main function to initiate core javascript after ajax complete
        initAjax: function () { 
            try {     
                App.pagination();
                initDatePicker();
                initInputMask();
                App.initFancybox();
                initKnob();
                handleEditor();
                handleListExpand();
                handleShortcut();
                setTimeout(handleTextarea, 50);
                setTimeout(shiftcheckbox, 50);
                //## handle multiple file upload
                $('input[multiple][data-url]').each(function() {
                    if (typeof multipleUpload == 'undefined') {
                        multipleUpload = {};
                        App.multipleFileUpload($(this));
                    }
                });

                handleScrollers(); // handles slim scrolling contents
                handleSelect2(); // handle custom Select2 dropdowns
                handleDropdowns(); // handle dropdowns
                handleTooltips(); // handle bootstrap tooltips
                handlePopovers(); // handles bootstrap popovers
                handleAccordions(); //handles accordions
                handleUniform(); // hanfle custom radio & checkboxes
                handleBootstrapSwitch(); // handle bootstrap switch plugin
                handleDropdownHover() // handles dropdown hover
            } catch(e) {}
        },

        //public function to fix the sidebar and content height accordingly
        fixContentHeight: function () {
            handleSidebarAndContentHeight();
        },

        //public function to remember last opened popover that needs to be closed on click
        setLastPopedPopover: function (el) {
            lastPopedPopover = el;
        },

        //public function to add callback a function which will be called on window resize
        addResponsiveHandler: function (func) {
            responsiveHandlers.push(func);
        },

        // useful function to make equal height for contacts stand side by side
        setEqualHeight: function (els) {
            var tallestEl = 0;
            els = jQuery(els);
            els.each(function () {
                var currentHeight = $(this).height();
                if (currentHeight > tallestEl) {
                    tallestColumn = currentHeight;
                }
            });
            els.height(tallestEl);
        },

        // wrapper function to scroll(focus) to an element
        scrollTo: function (el, offeset) {
            var pos = (el && el.size() > 0) ? el.offset().top : 0;

            if (el) {
                if ($('body').hasClass('page-header-fixed')) {
                    pos = pos - $('.header').height(); 
                }            
                pos = pos + (offeset ? offeset : -1 * el.height());
            }

            jQuery('html,body').animate({
                scrollTop: pos
            }, 'slow');
        },

        // function to scroll to the top
        scrollTop: function () {
            App.scrollTo();
        },

        // wrapper function to  block element(indicate loading)
        blockUI: function (options) {
            var options = $.extend(true, {}, options);
            var html = '';
            if (options.iconOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><img style="" src="/images/loading-spinner-grey.gif" align=""></div>';
            } else if (options.textOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><span>&nbsp;&nbsp;' + (options.message ? options.message : 'LOADING...') + '</span></div>';
            } else {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><img style="" src="/images/loading-spinner-grey.gif" align=""><span>&nbsp;&nbsp;' + (options.message ? options.message : 'LOADING...') + '</span></div>';
            }

            if (options.target) { // element blocking
                var el = jQuery(options.target);
                if (el.height() <= ($(window).height())) {
                    options.cenrerY = true;
                }            
                el.block({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    centerY: options.cenrerY != undefined ? options.cenrerY : false,
                    css: {
                        top: '10%',
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#000',
                        opacity: options.boxed ? 0.05 : 0.1, 
                        cursor: 'wait'
                    }
                });
            } else { // page blocking
                $.blockUI({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    css: {
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#000',
                        opacity: options.boxed ? 0.05 : 0.1,
                        cursor: 'wait'
                    }
                });
            }            
        },

        // wrapper function to  un-block element(finish loading)
        unblockUI: function (target) {
            if (target) {
                jQuery(target).unblock({
                    onUnblock: function () {
                        jQuery(target).css('position', '');
                        jQuery(target).css('zoom', '');
                    }
                });
            } else {
                $.unblockUI();
            }
        },

        startPageLoading: function(message) {
            $('.page-loading').remove();
            $('body').append('<div class="page-loading"><img src="assets/img/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>' + (message ? message : 'Loading...') + '</span></div>');
        },

        stopPageLoading: function() {
            $('.page-loading').remove();
        },

        // initializes uniform elements
        initUniform: function (els) {
            if (els) {
                jQuery(els).each(function () {
                    if ($(this).parents(".checker").size() == 0) {
                        $(this).show();
                        $(this).uniform();
                    }
                });
            } else {
                handleUniform();
            }
        },

        //wrapper function to update/sync jquery uniform checkbox & radios
        updateUniform: function (els) {
            $.uniform.update(els); // update the uniform checkbox & radios UI after the actual input control state changed
        },

        //public function to initialize the fancybox plugin
        initFancybox: function () {
            handleFancybox();
        },

        //public helper function to get actual input value(used in IE9 and IE8 due to placeholder attribute not supported)
        getActualVal: function (el) {
            var el = jQuery(el);
            if (el.val() === el.attr("placeholder")) {
                return "";
            }
            return el.val();
        },

        //public function to get a paremeter by name from URL
        getURLParameter: function (paramName) {
            var searchString = window.location.search.substring(1),
                i, val, params = searchString.split("&");

            for (i = 0; i < params.length; i++) {
                val = params[i].split("=");
                if (val[0] == paramName) {
                    return unescape(val[1]);
                }
            }
            return null;
        },

        // check for device touch support
        isTouchDevice: function () {
            try {
                document.createEvent("TouchEvent");
                return true;
            } catch (e) {
                return false;
            }
        },

        getUniqueID: function(prefix) {
            return 'prefix_' + Math.floor(Math.random() * (new Date()).getTime());
        },

        alert: function(options) {

            options = $.extend(true, {
                container: "", // alerts parent container(by default placed after the page breadcrumbs)
                place: "append", // append or prepent in container 
                type: 'success',  // alert's type
                message: "",  // alert's message
                close: true, // make alert closable
                reset: true, // close all previouse alerts first
                focus: true, // auto scroll to the alert after shown
                closeInSeconds: 0, // auto close after defined seconds
                icon: "" // put icon before the message
            }, options);

            var id = App.getUniqueID("app_alert");

            var html = '<div id="'+id+'" class="app-alerts alert alert-'+options.type+' fade in">' + (options.close ? '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>' : '' ) + (options.icon != "" ? '<i class="fa-lg fa fa-'+options.icon + '"></i>  ' : '') + options.message+'</div>'

            if (options.reset) {0
                $('.app-alerts').remove();
            }

            if (!options.container) {
                $('.page-breadcrumb').after(html);
            } else {
                if (options.place == "append") {
                    $(options.container).append(html);
                } else {
                    $(options.container).prepend(html);
                }
            }

            if (options.focus) {
                App.scrollTo($('#' + id));
            }

            if (options.closeInSeconds > 0) {
                setTimeout(function(){
                    $('#' + id).remove();
                }, options.closeInSeconds * 1000);
            }
        },

        // check IE8 mode
        isIE8: function () {
            return isIE8;
        },

        // check IE9 mode
        isIE9: function () {
            return isIE9;
        },

        //check RTL mode
        isRTL: function () {
            return isRTL;
        },

        // get layout color code by color name
        getLayoutColorCode: function (name) {
            if (layoutColorCodes[name]) {
                return layoutColorCodes[name];
            } else {
                return '';
            }
        }

    };

}();