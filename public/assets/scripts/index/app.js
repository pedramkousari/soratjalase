var Index = function () {

    handleMessage = function(frm, res, options) {
        var options = $.extend({
            'alert': frm.find('.alert'),
            'class': 'alert-danger'
        }, options);

        try {
            var msgs = $.parseJSON(res);
            options.alert
                .removeClass('alert-success')
                .removeClass('alert-danger')
                .addClass(options.class)
                .empty()
                .append('<ul></ul>');

            $.each(msgs, function(km, vm) {
                options.alert.find('ul').append('<li>' + vm + '</li>');
                //toastr.error(vm);
            });
            options.alert.fadeIn(function() {
                $(this).removeClass('display-none');
            });
        } catch (e) {}
    };

    return {

        //main function
        init: function () {
        },

        send: function (frm, options) {
            var frm = $(frm);
            if (!frm.length) return false;
            var parent = ($.type(options) == 'object' && $.type(options.parent) != undefined) ? options.parent : frm.closest('.register-div');
            var options = $.extend({
                parent: parent,
                ajax: {
                    overlay: {
                        active: true,
                        resultObject: parent
                    },
                    noResult: true
                },
                captcha: frm
            }, options);

            if ($.type(options.redirect) != 'undefined' || ($.type(options.login) != 'undefined' && options.login)) {
                options.ajax.overlay.remove = false;
            }

            if (frm.valid()) {
                frm.off('submit');
                options.parent.find('.alert').fadeOut(function() {
                    $(this).addClass('display-none');
                });
                App.ajaxRequest(frm, options.ajax, function(res) {
                    if ($.type(options.redirect) != 'undefined') {
                        window.location.href = options.redirect;
                    } else {
                        try {
                            var res = $.parseJSON(res);
                            window.location.href = '/' + res.group;
                        } catch(e) {}

                        handleMessage(frm, res, {'alert': options.parent.find('.alert'), 'class': 'alert-success'});
                        frm[0].reset();
                        if(options.captcha != false)
                            App.reCaptcha(options.captcha);
                    }
                }, function(res) {
                    if (res.status == 422) {
                        handleMessage(frm, res.responseText, {'alert': options.parent.find('.alert'), 'class': 'alert-danger'});
                        if(options.captcha != false)
                            App.reCaptcha(options.captcha);
                    }
                });
            }

            return false;
        },
    };

}();


var owl;
$('document').ready(function(){
    setTimeout(seSizeBtnOwl,250);
    setDiv();
    //setFooter();
    owl = $("#owl-document");
    owl.owlCarousel({
        margin:20,
//            stagePadding: 50,
//            nav:true,
        rtl:true,
        loop:true,
        items: 2,
        autoplay:true,
        autoplayTimeout:1500,
        autoplayHoverPause:true,
        responsiveClass:true,
        responsive:{
            400:{
                items:2,
            },
            500:{
                items:4,
            },
            768:{
                items:4,
                nav:false
            },
            1000:{
                items:4,
                loop:false
            },
            1200:{
                items:5,
                loop:false
            },
        },

    });


    $('.btn-owl-next-arrow').click(function() {
        owl.trigger('next.owl.carousel');
    })

    $('.btn-owl-prev-arrow').click(function() {
        owl.trigger('prev.owl.carousel');
    })

    $(".rslides").responsiveSlides({
        auto: true,             // Boolean: Animate automatically, true or false
        speed: 500,             // Integer: Speed of the transition, in milliseconds
        timeout: 4000,          // Integer: Time between slide transitions, in milliseconds
        pager: false,           // Boolean: Show pager, true or false
        nav: false,             // Boolean: Show navigation, true or false
        random: false,          // Boolean: Randomize the order of the slides, true or false
        pause: false,           // Boolean: Pause on hover, true or false
        pauseControls: true,    // Boolean: Pause when hovering controls, true or false
        prevText: "Previous",   // String: Text for the "previous" button
        nextText: "Next",       // String: Text for the "next" button
        maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
        navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
        manualControls: "",     // Selector: Declare custom pager navigation
        namespace: "rslides",   // String: Change the default namespace used
        before: function(){},   // Function: Before callback
        after: function(){}     // Function: After callback
    });
});



$(window).resize(function(e){
    setDiv();
    //setFooter();
    setTimeout(seSizeBtnOwl,250);
});

function setFooter(){
    if($('body').height()< (window.innerHeight - $('.footer').height())){
        $('.footer').css({
            'position' : 'fixed',
            'bottom' : 0,
        })
    }else{
        $('.footer').css({
            'position' : 'relative'
        })
    }
}
function setDiv(){
    if(window.innerWidth<992){
        $('.inner-banner-left-ctrl').removeClass('inner-banner-left hidden-xs hidden-sm');
    }else{
        $('.inner-banner-left-ctrl').addClass('inner-banner-left hidden-xs hidden-sm');
    }
}
function seSizeBtnOwl(){
    hC = $('.content-owl').height();
    hB = $('.btn-owl').height();

    size = (hC-hB)/2;
    fs = $('.btn-owl .fa').first().height();
    $('.btn-owl .fa').each(function(){
        $(this).css({
            'font-size' : parseFloat(fs) + parseFloat(size)
        })
    })
}