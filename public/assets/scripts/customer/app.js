var Wizard = function () {
    var _cntStep; //count step
    var _currentStep;
    var _progressStep;
    var _url;
    var _urlSendData;
    var _activeNextAndBack = true;

    var _nextStep = function (e) {
        e.preventDefault();
        if(!_activeNextAndBack) return;

        _urlSendData = _url + "/step";
        if(_currentStep<_cntStep){

            if(!validate_step()){
                return false;
            }else{

                frm = $('.form-step'+_currentStep);
                if(frm.find('[data-partner-stock]').length){
                    if(!Form.checkIsTrueStock())
                    {
                        bootbox.alert('مجموع سهامی وارد شده باید برابر 100 باشد.');
                        return false;
                    }
                }
                SendData(function(){
                    $('.progress-line').css({width:parseInt(_progressStep*_currentStep)+'%'});
                    $('.step > .active').last().next().addClass('active');
                    $('.form-step'+_currentStep).hide().next().show();
                    _currentStep++;

                    if(_currentStep == _cntStep){
                        if($('button.finish').hasClass('hidden')){
                            $('button.next').addClass('hidden');
                            $('button.finish').removeClass('hidden');
                            $('.btn-wizard a.next').addClass('hidden');
                        }
                    }
                    if($('.btn-wizard a.back').hasClass('hidden')){
                        $('.btn-wizard a.back').removeClass('hidden');
                    }
                });
            }
        }
    };
    var _backStep = function (e) {
        e.preventDefault();
        if(!_activeNextAndBack) return;

        _urlSendData = _url + "/stepLoad";
        if(_currentStep>1){
            $('.form-step'+_currentStep).hide().prev().show();
            _currentStep--;

            SendData(function(){
                $('.progress-line').css({width:parseInt(_progressStep*(_currentStep-1))+'%'});
                $('.step > .active').last().removeClass('active');

                if(_currentStep==1){
                    $('.btn-wizard a.back').addClass('hidden');
                }

                if($('.btn-wizard a.next').hasClass('hidden')){
                    $('.btn-wizard a.next').removeClass('hidden');
                }

                if($('button.next').hasClass('hidden')){
                    $('button.next').removeClass('hidden');
                    $('button.finish').addClass('hidden');
                }
            });
        }
    };
    var _finish = function(e){
        e.preventDefault();
        _urlSendData = _url + "/finish";

        if(!validate_step()){
            return false;
        }else {
            SendData(function () {
                _activeNextAndBack = false;
            }, _urlSendData);
        }
    };

    var getStep= function(){
        console.log('currentStep: '+_currentStep , 'progressStep'+ _progressStep);
    }

    /**
     * validate form
     * @returns {boolean}
     */
    validate_step = function(){
        if (typeof jQuery.fn.validate === 'undefined') { throw new Error('required jqury validation plugin') };

        if(_currentStep == 0){
            _currentStep = '-finish';
        }
        curInputs = $('.form-step'+_currentStep).find("input,select,textarea"),
        isValid = true;



        $(".form-group").removeClass("has-error");

        curInputs.each(function(key,obj){
            if($(obj).is('[data-off-validate]')) return;

            if (!$(obj).valid()){
                isValid = false;
                $(obj).closest(".form-group").addClass("has-error");
            }
        });

        $

        if (isValid)
            return true;
        else
            return false;
    }

    /**
     * send data ajax
     * @constructor
     */
    var SendData = function(callback , url){
        if(typeof App == 'undefined'){
            return 0;
        }
        if(_currentStep == 0){
            _currentStep = '-finish';
        }
        frm = $('.form-step'+_currentStep);



        var frm = $(frm);
        if (!frm.length) return false;
        var parent = frm;

        var options = $.extend({
            parent: parent,
            ajax: {
                overlay: {
                    active: true,
                    resultObject: parent
                },
                noResult: true,
                url : url ? url :  _urlSendData+_currentStep
            },
            captcha: frm
        }, options);

        App.ajaxRequest(frm, options.ajax, function(res) {
            res = JSON.parse(res);
            view =  $(res[0]);
            view.html(res[1]);
            //if ($.type(options.redirect) != 'undefined') {
            //    window.location.href = options.redirect;
            //} else {
            //    try {
            //        var res = $.parseJSON(res);
            //        window.location.href = '/' + res.group;
            //    } catch(e) {}
            //
            //    handleMessage(res, {'alert': options.parent.find('.alert'), 'class': 'alert-success'});
            //    //frm[0].reset();
            //    //App.reCaptcha(options.captcha);
            //}

            callback();
        }, function(res) {
            if (res.status == 422) {
                handleMessage(res.responseText, {'alert': options.parent.find('.alert'), 'class': 'alert-danger'});
                //App.reCaptcha(options.captcha);
            }

        });
    }

    return {
        init: function (options) {
            _url = options.urlSendData ? options.urlSendData : '';
            _cntStep = $('.step > div').length;
            _currentStep = $('.step > .active').length;

            _progressStep = 100/(_cntStep-1);

            $('.btn-wizard a.next,button.next').bind('click', _nextStep);
            $('.btn-wizard a.back,button.prev').bind('click', _backStep);

            $('button.finish').bind('click', _finish);
        },
    };
}();




var Form = function () {

    var _money = 0;
    var _checkIsFull= function(){
        total = 0;
        $('[data-partner-stock]').each(function(key , obj){
            total += parseInt(Number($(obj).val()));
        });

        if(total>100){
            return true;
        }
        return false;
    };

    var _checkIsTrueStock= function(){
        total = 0;
        $('[data-partner-stock]').each(function(key , obj){
            total += parseInt(Number($(obj).val()));
        });

        if(total==100){
            return true;
        }
        return false;
    };

    return {
        init: function () {
        },

        //addUserPartner: function (obj) {
        //    obj = $(obj);
        //    console.log(obj);
        //
        //    if (!obj.valid()){
        //        isValid = false;
        //        $(obj).closest(".form-group").addClass("has-error");
        //    }
        //},

        setMoney: function(obj){
            obj = $(obj);
            if (!$(obj).valid()){
                isValid = false;
                $(obj).closest(".form-group").addClass("has-error");
            }
            else{
                p = obj.val();
                _money = Number(p.replaceAll(',',''));
            }
        },

        stockToMoney: function(obj){

            obj = $(obj);

            if (!$(obj).valid()){
                isValid = false;
                $(obj).closest(".form-group").addClass("has-error");
                return false;
            }

            if(_checkIsFull()){
                bootbox.alert('مجموع سهام وارد شده بیشتر از 100 میباشد.');
                obj.val('');
                obj.closest('.partner').find('[data-set-money]').first().val(0);
                return false;
            }
            else{
                p = parseInt(Number(obj.val()));
                money = (_money * p) / 100;

                if(money == 0){
                    money = '';
                }
                obj.closest('.partner').find('[data-set-money]').first().val(money);
            }
        },

        checkIsTrueStock: _checkIsTrueStock,

        setAttr : function(obj , data){
            obj = $(obj);
            $('[data-'+data+']').val(json_address[obj.val()]);
        },


    };
}();

Form.init();

jQuery.validator.addMethod("mony-company", function(value, element ,params) {
    value = value.replace(/,/g, "");
    return this.optional(element) || value>=999999;
}, "لطفا مبلغ بیشتر از 999,999 وارد کنید");

jQuery.validator.addMethod("length", function(value, element, params) {
    if(!value.trim()){
        return true;
    }
    return (value.length == params);
}, jQuery.validator.format("تعداد کاراکتر باید {0} باشد"));

jQuery.validator.addMethod("persian", function(value, element, params) {
    tmp = value.split('');

    var keys = new Array(1711, 0, 0, 0, 0, 1608, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1705, 1572, 0, 1548,
        1567, 0, 1616, 1571, 8250, 0, 1615, 0, 0, 1570, 1577, 0, 0, 0, 1569, 1573, 0, 0, 1614, 1612, 1613, 0, 0,
        8249, 1611, 171, 0, 187, 1580, 1688, 1670, 0, 1600, 1662, 1588, 1584, 1586, 1740, 1579, 1576, 1604, 1575,
        1607, 1578, 1606, 1605, 1574, 1583, 1582, 1581, 1590, 1602, 1587, 1601, 1593, 1585, 1589, 1591, 1594, 1592 , 32);

    res = true;
    $(tmp).each(function(key,value){
        if(keys.indexOf(value.charCodeAt()) == -1){
            res = false;
            return false;
        }
    })

    return res;
}, jQuery.validator.format("فارسی"));

String.prototype.replaceAll = function(target, replacement) {
    return this.split(target).join(replacement);
};

$(document).ready(function(){
    setFooter();
});

$(window).resize(function(e){
    setFooter();
})


function setFooter(){
    if($('body').height()< (window.innerHeight - $('.footer').height())){
        $('.footer').css({
            'position' : 'fixed',
            'bottom' : 0,
        })
    }else{
        $('.footer').css({
            'position' : 'relative'
        })
    }
}