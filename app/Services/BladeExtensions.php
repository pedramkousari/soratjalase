<?php namespace App\Services;

use Blade;

Class BladeExtensions {

	public static function register() 
	{
		/* @eval($var++) */
		Blade::extend(function($view)
		{
		    return preg_replace('/\@eval\s*\((.+)\)/', '<?php eval($1); ?>', $view);
		});

		//## @var(param1 [,param2])
		Blade::extend(function($value) {
		    return preg_replace('/(\s*)@var\(([^,]+),(.*)\)/', '$1<?php ${$2} = $3; ?>', $value);
		});

		//## break|continue;
		Blade::extend(function($value) {
		    return preg_replace('/(\s*)@(break|continue)(\s*)/', '$1<?php $2; ?>$3', $value);
		});

		/* @digit($param) */
		Blade::extend(function($view)
		{
		    return preg_replace('/\@digit\((.+)\)/', '<?php echo FarsiLib::convertDigit($1); ?>', $view);
		});

		/* @resetAssets($param) */
		Blade::extend(function($view)
		{
		    return preg_replace('/\@resetAssets\(\)/', '<?php Assets::reset(); ?>', $view);
		});

		/* @assets($param) */
		Blade::extend(function($view)
		{
		    return preg_replace('/\@assets\((.+)\)/', '<?php Assets::add($1); ?>', $view);
		});

		//## @t($param)
		Blade::extend(function($value) {
			$lang = (config('custom.site.lang', '') ?: 'language');	
			return preg_replace('/(\s*)@t\(["|\']{0,1}([^("|\')]+)["|\']{0,1}\)/i', "$1<?php echo trans(\"{$lang}.$2\") ?>", $value);
		});
	}
}