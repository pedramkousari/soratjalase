<?php namespace App\Services;

use App;
use Auth;
use File;
use Request;
use Config;
use Session;

Class Lib {

	/**
	 * //creates hash for image id  1000 => 000001/1000
	 * @param int $id
	 * @param bool $contact_id
	 * @return string
	 */
	public function uploadPath($id, $concat_id = true) 
    {
        return str_pad(intval($id / 1000), 6, '0', STR_PAD_LEFT) . DIRECTORY_SEPARATOR . ($concat_id ? $id : '');
    }

	/**
	 * call action 
	 * @param string $prefix
	 * @param string $controller
	 * @param string $controller
	 * @param string $action
	 * @param string $args
	 * @return mixed
	 */
	public function callAction($prefix = '', $controller = 'home', $action = 'index', $args = '') 
	{
	    Config::set('app.section', trim($prefix));
	    Config::set('app.controller', trim($controller));
	    Config::set('app.action', trim($action));
	    Config::set('app.random', str_random());

	    //## check permission 
	    $userGroups = (!is_null(Auth::user()) ? Auth::user()->group() : []);
	    if (Auth::id()) {
			if (in_array($prefix, config('app.prefixes')) and !in_array($prefix, array_flip($userGroups))) {
				if (Request::ajax()) {
					return "<script>window.location.href = '/auth/login'</script>";
				} else {
					return redirect()->guest('auth/login');
				}
			}
	    }		

	    $prefixPath = app_path() . '/Http/Controllers/' . studly_case($prefix);
	    if (!is_dir($prefixPath))
	        File::makeDirectory($prefixPath, 0775, true, true);

	    $controller = 'App\\Http\\Controllers\\' . ($prefix ? (studly_case($prefix) . '\\')  : '') . studly_case($controller) . "Controller";
	    $params = explode("/", $args);
	    if ($params && preg_match('/\d+/', $params[0])) {
	        Config::set('app.id', $params[0]);
	    } else {
	        Config::set('app.id', 0);
	    }
	    $app = app();
	 
	    if (!class_exists($controller)) {
	        return abort(404);
	    }
	    if (preg_match('/^\d+$/', $action)) {
	        $params = array($action);
	        Config::set('app.id', $action);
	    }

	    $action = !method_exists($controller, $action) ? 'index' : $action;
	    Config::set('app.action', trim($action));
	    $controller = $app->make($controller);
	    return $controller->callAction($action, $params);
	}
	
	/**
	 * get current url
	 * @param string $partname
	 * @param bool $prefix
	 * @return string
	 */
	public function getCurrentURL($partname = '', $prefix = false) 
	{
	    $url = $prefix ? (env('APP_URL') . '/') : '';
	    $localization = config('app.localization') ? ('/' . config('app.localization')) : '';
	    $currentSection = (strtolower(config('app.section')) == 'index' ? '' : config('app.section'));

	    switch ($partname) {
	        case 'locale':
	        case 'localization':
	            $url .= (config('app.localization') ? config('app.localization') : '');
	            break;
	        case 'section':
	            $url .= ((strtolower(config('app.section')) == 'index' and !$localization) ? '/' : ($localization  . '/')) . $currentSection;
	            break;
	        case 'controller':
	            $url .= $localization . '/' . ($currentSection ? ($currentSection . '/') : '') . config('app.controller');
	            break;
	        case 'action':
	            $url .= $localization . '/' . ($currentSection ? ($currentSection . '/') : '') . config('app.controller') . '/' . config('app.action');
	            break;
	        case 'id':
	            $url .= $localization . '/' . ($currentSection ? ($currentSection . '/') : '') . config('app.controller') . '/' . config('app.action') . '/' . config('app.id');
	            break;
	        default:
	            $url .= Request::path();
	            break;
	    }
	    return $url;
	}

	/**
	 * get path in array 
	 * @param string $path
	 * @param array $current
	 * @return array
	 */
	public function getPath($path, $current = array())
	{
	    $localization = @$current['localization'] ? $current['localization'] : '';
	    $section = @$current['section'] ? $current['section'] : '';
	    $controller = @$current['controller'] ? $current['controller'] : '';
	    $method = @$current['action'] ? $current['action'] : '';
	    $param = @$current['param'] ? $current['param'] : '';

	    $id = 0;
	    $res = '/';
	    if (is_array($path)) {
	        $localization = @$path['localization'] ? $path['localization'] . '/' : (config('app.localization') ? config('app.localization') . '/' : '');
	        $section = @$path['section'] ? $path['section'] : config('app.section');
	        $res .= $localization . $section;
	        if (@$path['controller'] and @$path['action'] and is_array(@$path['param']) or (@$path['controller'] and is_array(@$path['param']))) {
	            $res .= '/' . $path['controller'] . (@$path['action'] ? '/' . $path['action'] : '');
	            foreach ($path['param'] as $key => $param) {
	                if (preg_match('/\d+/', $param) and $key == 'id') {
	                    $res .= '/' . $param;
	                    $id = $param;
	                } else {
	                    preg_match('/\?/', $res, $match);
	                    if (!$match) {
	                        $res .= "?$key=$param";
	                        $param .= "?$key=$param";
	                    } else {
	                        $res .= "&$key=$param";
	                        $param .= "&$key=$param";
	                    }
	                }
	            }
	            $controller = $path['controller'];
	            $method = @$path['action'];
	        } elseif (@$path['controller'] and @$path['action']) {
	            $res .= '/' . $path['controller'] . '/' . $path['action'];
	            $controller = $path['controller'];
	            $method = $path['action'];
	        } else if (@$path['controller']) {
	            $res .= '/' . $path['controller'];
	            $controller = $path['controller'];
	        }
	    } else {
	        $isURL = preg_match('/\//', $path) ? true : false;
	        if ($isURL) {
	            $segments = explode('/', $path);
	            if ($segments) {
	                foreach ($segments as $segment) {
	                    if (!trim($segment)) continue;
	                    if (!$localization and in_array($segment, config('app.locales', ['en']))) {
	                        $localization = $segment;
	                    } elseif (!$section and in_array($segment, config('app.prefixes', []))) {
	                        $section = $segment;
	                    } elseif (!$controller and !preg_match('/\d+/', $segment)) {
	                        $controller = $segment;
	                    } elseif (!$method and !preg_match('/\d+/', $segment)) {
	                        $method = $segment;
	                    } else {
	                        $id = intval($segment);
	                    }
	                }
	            }
	            $localization = ($localization ? $localization : config('app.localization') ? config('app.localization') . '/' : '');
	            $section = $section ? $section : config('app.section');
	            $res .= $localization . $section . $path;
	        }
	    }
	    return array(
	        'path' => preg_replace('/\/+/', '/', $res),
	        'localization' => str_replace('/', '', $localization),
	        'section' => $section,
	        'controller' => $controller,
	        'action' => $method,
	        'id' => $id,
	        'param' => $param
	    );
	}

	public function makeNavigation($arrNav, $hasUL = false, $children = false) 
	{
	    if (is_array($arrNav)) {
	        if ($children and $hasUL) {
	            $return = '<ul class="sub-menu">';
	        } else {
	            $return = $hasUL ? '<ul>' : '';
	        }
	        $cnt = 0;
	        foreach ($arrNav as $nav) {
	            if ($nav['label']) {
	                $currentSection = config('app.section');
	                $currentCtrl = config('app.controller');
	                $currentAction = config('app.action');
	                $currentId = config('app.id');
	                $active = $url = '';
	                $permissionList = array();
	                if (@$nav['permission']) {
	                    $permissionList = is_array($nav['permission']) ? $nav['permission'] : (is_string($nav['permission']) ? explode(',', $nav['permission']) : array());
	                }
	                if ($permissionList) {
	                    foreach ($permissionList as $kp => $permission) {
	                        $permissionList[$kp] = trim($permission);
	                    }
	                }
	                $permissionExist = @$nav['permission'] ? 1 : 0;
	                if (!in_array($currentSection, $permissionList) and $permissionExist) continue;
	                if (@$nav['controller']) {
	                    $active = $currentCtrl == @$nav['controller'] ? 'active' : '';
	                    $pathReturn = self::getPath(array('controller' => $nav['controller'], 'action' => @$nav['action']), ['section' => $currentSection]);
	                    $url = @$pathReturn['path'];
	                } elseif (@$nav['url']) { //## active li
	                    $pathReturn = self::getPath($nav['url'], ['section' => $currentSection]);
	                    if ($pathReturn['controller'] and $pathReturn['action'] and $pathReturn['id']) {
	                        $active = ($pathReturn['controller'] == $currentCtrl and $pathReturn['action'] == $currentAction and $pathReturn['id'] == $currentId) ? 'active' : '';
	                    } elseif ($pathReturn['controller'] and $pathReturn['action'] and !$pathReturn['id']) {
	                        $active = ($pathReturn['controller'] == $currentCtrl and $pathReturn['action'] == $currentAction) ? 'active' : '';
	                    } elseif ($pathReturn['controller'] and $pathReturn['id']) {
	                        $active = ($pathReturn['controller'] == $currentCtrl and $pathReturn['id'] == $currentId) ? 'active' : '';
	                    } elseif ($pathReturn['controller'] and $currentAction == 'index') {
	                        $active = ($pathReturn['controller'] == $currentCtrl) ? 'active' : '';
	                    }
	                    $url = $pathReturn['path'];
	                    if ($pathReturn['controller'] == 'logout') {
	                    	$url = "/auth/logout";
	                    }

	                } else {
	                    $url = 'javascript:;';

	                    //## active parent if child this page
	                    $actvieParent = 0;
	                    if (@$nav['children']) {
	                        foreach ($nav['children'] as $child) {
	                            if (@$child['controller']) {
	                                $actvieParent += $currentCtrl == @$child['controller'] ? 1 : 0;
	                                $pathReturn = self::getPath(array('controller' => $child['controller'], 'action' => @$child['action']), ['section' => $currentSection]);
	                                $url = @$pathReturn['path'];
	                            } elseif (@$child['url']) {
	                                $pathReturn = self::getPath($child['url'], ['section' => $currentSection]);
	                                if ($pathReturn['controller'] and $pathReturn['action'] and $pathReturn['id']) {
	                                    $actvieParent += ($pathReturn['controller'] == $currentCtrl and $pathReturn['action'] == $currentAction and $pathReturn['id'] == $currentId) ? 1 : 0;
	                                } elseif ($pathReturn['controller'] and $pathReturn['action'] and !$pathReturn['id']) {
	                                    $actvieParent += ($pathReturn['controller'] == $currentCtrl and $pathReturn['action'] == $currentAction) ? 1 : 0;
	                                } elseif ($pathReturn['controller'] and $pathReturn['id']) {
	                                    $actvieParent += ($pathReturn['controller'] == $currentCtrl and $pathReturn['id'] == $currentId) ? 1 : 0;
	                                } elseif ($pathReturn['controller'] and !$currentAction) {
	                                    $actvieParent += ($pathReturn['controller'] == $currentCtrl) ? 1 : 0;
	                                }
	                            }
	                        }
	                    }
	                    $active = $actvieParent ? 'active' : '';
	                }

	                $start = ($cnt == 0 and !$hasUL) ? 'start' : '';
	                $classList = "$start $active";
	                $propStr = '';
	                if (@$nav['extra']) {
	                    foreach($nav['extra'] as $prop => $val) {
	                        if (strtolower(trim($prop)) == 'class') {
	                            $classList .= $val;
	                        } else {
	                            $propStr .= $prop . "='$val'";
	                        }
	                    }
	                }
	                $return .= "<li class='{$classList}' {$propStr} ><a href='{$url}'>";

	                if (@$nav['fa-icon']) {
	                    if (preg_match('/^<[^>]+>/', $nav['fa-icon']))
	                        $return .= $nav['fa-icon'] . ($children ? ' ' : '');
	                    else
	                        $return .= "<i class='{$nav['fa-icon']}'></i>" . ($children ? ' ' : '');
	                } else {
	                    $return .= "<i class='fa'></i>" . ($children ? ' ' : '');
	                }

	                $translated = trim(trans(config('custom.site.lang', 'language') . '.' . $nav['label']));
	                if (preg_match('/^language\./', $nav['label'])) {
	                	$translated = trim(trans($nav['label']));
	                }
	                $return .= '<span class="title">' . $translated . '</span><span class="selected"></span>';
	                if (@$nav['children']) {
	                    $return .= '<span class="arrow '. ($active ? 'open': '') .'"></span>';
	                }
	                $return .= "</a>";
	                if (@$nav['children']) {
	                    $return .= self::makeNavigation($nav['children'], true, $children = true);
	                }
	                $return .= '</li>';
	                $cnt++;
	            }
	        }
	        $return .= $hasUL ? '</ul>' : '';
	    }
    	return $return;
	}

	//### translate with explode
	public function transExplode($msg, $seperator = ',', $perfix = 'language', $returnSeperator = '، ') {
	    $msgList = explode($seperator, $msg);
	    $res = '';
	    if ($msgList) {
	        $count = count($msgList);
	        $cnt = 0;
	        foreach ($msgList as $txt) {
	            $cnt++;
	            $res .= trans($perfix . '.' . trim($txt)) . ($cnt < $count ? "$returnSeperator" : '');
	        }
	    }
	    return $res;
	}

	//## convert object to array
	public function object2array(&$obj, $recursive = false)
	{
		if(is_array($obj) or is_object($obj)) {
			foreach($obj as $key=>$val) {
				if($recursive and (is_array($val) or is_object($val))) {
					$res[$key] = self::object2array($val, true);
				} else {
					$res[$key] = $val;
				}	
			}	
		} else {
			$res = $obj;
		}
		return $res;
	}
}