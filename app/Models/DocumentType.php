<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class DocumentType extends Model {

    use SoftDeletes;
    protected $times = ['deleted_at'];

    public function documents(){
        return $this->hasMany(Document::class);
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'xxdocument_type';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'xdocument_typeid';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * get Result
     * @param $query
     * @return mixed
     */
    public static function getResult()
    {
        $instance = new static();

        $res = DB::table($instance->table . ' AS dt')
            ->select(['*', "{$instance->primaryKey} as xid"]);

        return $res;
    }

    /**
     * get First
     * @param $id
     * @return mixed
     */
    public static function first($id)
    {
        $instance = new static();

        $res = self::getResult()
            ->where("dt.{$instance->primaryKey}", $id)
            ->first();

        return $res;
    }

    /**
     * update or insert
     * @param $data
     * @param int $id
     * @return int
     */
    public static function store($data, $id = 0)
    {
        $instance = new static();
        if (!$id)
            $id = $instance->newQuery()->insertGetId($data);
        else
            $instance->newQuery()->where($instance->primaryKey, intval($id))->update($data);

        return $id;
    }

    /**
     * delete
     * @param $id
     * @return mixed
     */
    public static function remove($id)
    {
        $instance = new static();

        $cond = !intval($id) ? $id : "{$instance->primaryKey} = '$id'";

        $res = $instance->newQuery()->whereRaw($cond)->delete();
        return $res;
    }

}
