<?php namespace App\Models;

use Auth;
use Crypt;
use DB;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'xxuser_name';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'xuserid';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'xusername',
        'xpassword',
        'email',
        'xuser_status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['xpassword', 'xremember_token', 'xuser_status'];

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->xpassword;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'xremember_token';
    }


    /**
     * get user info
     * @param $query
     * @return mixed
     */
    public static function info($id = '')
    {
        $instance = new static();
        $id = ($id ? $id : Auth::id());
        DB::connection()->setFetchMode(\PDO::FETCH_OBJ);

        return DB::table('xxuser_info')
            ->where($instance->primaryKey, $id)
            ->select(array('*', DB::raw("CONCAT(xname, ' ', xfamily) AS xfullname")))
            ->first();
    }

    /**
     * get user groups
     * @param $query
     * @return mixed
     */
    public static function group($id = '')
    {
        $instance = new static();
        $id = ($id ? $id : Auth::id());
        DB::connection()->setFetchMode(\PDO::FETCH_OBJ);

        return DB::table('xxuser_role AS ur')
            ->leftJoin('xxuser_group AS ug', 'ug.xgroupid', '=', 'ur.xgroupid')
            ->where($instance->primaryKey, $id)
            ->select('xgroup', 'ug.xgroupid')
            ->lists('xgroupid', 'xgroup');
    }

    /**
     * get Result
     * @param $query
     * @return mixed
     */
    public static function getResult()
    {
        $instance = new static();

        $res = DB::table($instance->table . ' as u')
            ->leftJoin('xxuser_info as ui', 'ui.xuserid', '=', 'u.xuserid')
            ->leftJoin('xxuser_role as ur', 'ur.xuserid', '=', 'u.xuserid')
            ->leftJoin('xxuser_group as ug', 'ug.xgroupid', '=', 'ur.xgroupid')
            ->select([
                'u.*', 'ui.*', DB::Raw('NULL AS xpassword'),
                DB::Raw("GROUP_CONCAT(xgroup SEPARATOR ', ') AS xgroup"),
                DB::Raw("CONCAT(ui.xname, ' ', ui.xfamily) AS xfullname"), 
                DB::Raw('GROUP_CONCAT(ug.xgroupid ORDER BY ug.xgroupid) AS xgroupid'), 
                "u.{$instance->primaryKey} AS xid"
            ]);

        return $res;
    }

    /**
     * first
     * @return mixed
     */
    public static function first($id)
    {
        $instance = new static();

        $res = self::getResult()
            ->where("u.{$instance->primaryKey}", $id)
            ->first();

        return $res;
    }

    /**
     * insert new user
     * @param $valueList array has to key user & userInfo 
     * @param $groupList array array of user groups
     * @return mixed
     * @throws Exception
     */
    public static function store($valueList, $groupList, $id = 0)
    {
        $instance = new static();
        $id = (intval($id) ?: ((!is_array($groupList) and intval($groupList)) ? intval($groupList) : 0));
  
        if ($groupList) {
            //## get group id list
            $groups = [];
            if (is_array($groupList)) {
                foreach ($groupList as $key => $val) {
                    $groups[] = (intval($val) != 0 ? $val : UserGroup::getResult()
                        ->where('ug.xgroup', trim($val))
                        ->pluck('xgroupid'));
                }
            }
            DB::beginTransaction();

            //## check for empty password 
            if (!empty(@$valueList[$instance->table]['xpassword'])) {
                $valueList[$instance->table]['xpayload'] = Crypt::encrypt($valueList[$instance->table]['xpassword']);
                $valueList[$instance->table]['xpassword'] = bcrypt($valueList[$instance->table]['xpassword']);
            } else {
                unset($valueList[$instance->table]['xpassword']);
            }

            //## check username exist
            $username = strtolower(@$valueList[$instance->table]['xusername']);
            $userCond = "lower(u.xusername) = '$username'";
            $userCond .= ($id ? " AND u.xuserid <> '$id'" : '');

            $userRow = User::getResult() 
                ->whereRaw($userCond)
                ->first();
            if (intval(@$userRow['xuserid']) and !$id) {
                die(json_encode(array('type' => 'error', 'message' => trans('language.username is already exist'))));
            }
            
            $userId = $id;
            if ($id) {
                $instance->newQuery()->where($instance->primaryKey, intval($id))->update($valueList[$instance->table]);
            } else {
                $userId = $instance->newQuery()->insertGetId($valueList[$instance->table]);
            }

            if (!$userId) {
                DB::rollback();
                die(json_encode(array('type' => 'error', 'message' => trans('language.can not create user'))));
            }

            unset($valueList[$instance->table]);

            foreach ($valueList as $tbl => $fv) {
                $fv['xuserid'] = intval($userId);
                if ($id) {
                    DB::table($tbl)
                        ->where($instance->primaryKey, $id)
                        ->update($fv);
                } else {
                    DB::table($tbl)
                        ->insert($fv);
                }
            }

            //## delete user group permission
            $res = UserRole::where($instance->primaryKey, intval($userId))->delete();

            //## insert user group permission   
            if (is_array($groupList)) {
                foreach ($groups as $group) { //update groupuser
                    UserRole::store(['xuserid' => intval($userId), 'xgroupid' => intval($group)]);
                }
            }
            
            DB::commit();
            return @$userId;
        } else {
            die(json_encode(array('type' => 'error', 'message' => trans('language.user group not selected'))));
        }
    }

    /**
     * remove 
     * @param $cond
     * @param string $tablelist
     * @return mixed
     */
    public static function remove($cond, $tablelist = 'xxuser_name, xxuser_info, xxuser_role')
    {
        $instance = new static();
        $cond = (intval($cond) == $cond ? "{$instance->primaryKey} = $cond" : $cond);

        $joinedTables = implode(' NATURAL LEFT JOIN ', explode(',', $tablelist));
        return DB::statement("DELETE $tablelist FROM $joinedTables WHERE $cond");
    }


    /**
     * relation one to one
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userinfo(){
        return $this->hasOne(UserInfo::class ,'xuserid','xuserid');
    }


    /**
     * The roles that belong to the user.
     */
    public function groups()
    {
        return $this->belongsToMany(User::class,'xxuser_role','xuserid','xgroupid');
    }

    /**
     * #foreignKey: "documents.xuser_id"
     * #localKey: "xuserid"
     * @return mixed
     */
    public function documents(){
        return $this->hasMany(Document::class,'xuser_id','xuserid');
    }
}
