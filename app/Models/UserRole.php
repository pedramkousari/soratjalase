<?php namespace App\Models;

use DB;

class UserRole extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'xxuser_role';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'xuser_roleid';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * get Result
     * @param $query
     * @return mixed
     */
    public static function getResult()
    {
        $instance = new static();

        $res = DB::table($instance->table . ' AS ur')
            ->leftJoin('xxuser_group AS ug', 'ug.xgroupid', '=', 'ur.xgroupid')
            ->select(['*', "ur.{$instance->primaryKey} AS xid"]);

        return $res;
    }

    /**
     * get First
     * @param $id
     * @return mixed
     */
    public static function first($id)
    {
        $instance = new static();
       
        $res = self::getResult($instance->primaryKey, $id)
            ->first();
        return $res;
    }

    /**
     * update or insert
     * @param $data
     * @param int $id
     * @return int
     */
    public static function store($data, $id = 0)
    {
        $instance = new static();
        if (!$id)
            $id = $instance->newQuery()->insertGetId($data);
        else
            $instance->newQuery()->where($instance->primaryKey, intval($id))->update($data);
        
        return $id;
    }

    /**
     * delete
     * @param $id
     * @return mixed
     */
    public static function remove($id)
    {
        $instance = new static();
        $cond = (int) $id ? "{$instance->primaryKey} = '$id'" : $id;

        $res = $instance->newQuery()->whereRaw($cond)->delete();
        return $res;
    }
}