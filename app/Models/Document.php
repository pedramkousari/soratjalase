<?php namespace App\Models;
use DB;

class Document extends Model {

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['document_types_id' , 'code'];

    /**
     * @return mixed
     */
    public function user(){
        return $this->belongsTo(User::class,'xuser_id','xuserid');
    }

    /**
     * @return mixed
     */
    public function documentType(){
        return $this->blongTo(DocumentType::class);
    }

    /**
     * @return mixed
     */
    public function frmRegisterCompany(){
        return $this->hasOne(FrmRegisterCompanyLtd::class,'document_id','id');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'xxdocument';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'xdocumentid';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * get Result
     * @param $query
     * @return mixed
     */
    public static function getResult()
    {
        $instance = new static();

        $res = DB::table($instance->table . ' AS d')
            ->whereNull('d.deleted_at')
            ->select(['*', "{$instance->primaryKey} as xid"]);

        return $res;
    }

    /**
     * get First
     * @param $id
     * @return mixed
     */
    public static function first($id)
    {
        $instance = new static();

        $res = self::getResult()
            ->where("d.{$instance->primaryKey}", $id)
            ->first();

        return $res;
    }

    /**
     * update or insert
     * @param $data
     * @param int $id
     * @return int
     */
    public static function store($data, $id = 0)
    {
        $instance = new static();
        if (!$id)
            $id = $instance->newQuery()->insertGetId($data);
        else
            $instance->newQuery()->where($instance->primaryKey, intval($id))->update($data);

        return $id;
    }

    /**
     * delete
     * @param $id
     * @return mixed
     */
    public static function remove($id)
    {
        $instance = new static();

        $cond = !intval($id) ? $id : "{$instance->primaryKey} = '$id'";

        $res = $instance->newQuery()->whereRaw($cond)->delete();
        return $res;
    }



}
