<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Company extends Model {

    use softDeletes;

    protected $dates = ['deleted_at'];

//    protected $fillable = ['type','name','national_code','address','postal_code','money'];


    public function frmRegisterCompany(){
        return $this->hasOne(FrmRegisterCompanyLtd::class);
    }


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'xxcompany';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'xcompanyid';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    /**
     * get Result
     * @param $query
     * @return mixed
     */
    public static function getResult()
    {
        $instance = new static();

        $res = DB::table($instance->table . ' AS c')
            ->select(['*', "c.{$instance->primaryKey} as xid"]);

        return $res;
    }

    /**
     * get First
     * @param $id
     * @return mixed
     */
    public static function first($id)
    {
        $instance = new static();

        $res = self::getResult()
            ->where("c.{$instance->primaryKey}", $id)
            ->first();

        return $res;
    }

    /**
     * update or insert
     * @param $data
     * @param int $id
     * @return int
     */
    public static function store($data, $id = 0)
    {
        $instance = new static();
        if (!$id)
            $id = $instance->newQuery()->insertGetId($data);
        else
            $instance->newQuery()->where($instance->primaryKey, intval($id))->update($data);

        return $id;
    }

    /**
     * delete
     * @param $id
     * @return mixed
     */
    public static function remove($id)
    {
        $instance = new static();

        $cond = !intval($id) ? $id : "{$instance->primaryKey} = '$id'";

        $res = $instance->newQuery()->whereRaw($cond)->delete();
        return $res;
    }


}
