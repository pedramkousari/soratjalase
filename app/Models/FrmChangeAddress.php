<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class FrmChangeAddress extends Model {

    use softDeletes;

    protected $dates = ['deleted_at'];


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'xxfrm_change_address';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'xfrm_change_addressid';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    /**
     * get Result
     * @param $query
     * @return mixed
     */
    public static function getResult()
    {
        $instance = new static();

        $res = DB::table($instance->table . ' AS fca')
            ->leftJoin('xxfrm_data as fd' , 'fd.xfrm_id', '=' , 'fca.xfrm_change_addressid')
            ->leftJoin('xxcompany as c' , 'c.xcompanyid', '=' , 'fd.xcompanyid')
            ->where('c.xfrm_type',documentType(7))
            ->where('fd.xfrm_type',documentType(7))

            ->select(['*', "fca.{$instance->primaryKey} as xid"]);

        return $res;
    }

    /**
     * get First
     * @param $id
     * @return mixed
     */
    public static function first($id)
    {
        $instance = new static();

        $res = self::getResult()
            ->where("fca.{$instance->primaryKey}", $id)
            ->first();

        return $res;
    }

    /**
     * update or insert
     * @param $data
     * @param int $id
     * @return int
     */
    public static function store($data, $id = 0)
    {
        $instance = new static();
        if (!$id)
            $id = $instance->newQuery()->insertGetId($data);
        else
            $instance->newQuery()->where($instance->primaryKey, intval($id))->update($data);

        return $id;
    }

    /**
     * delete
     * @param $id
     * @return mixed
     */
    public static function remove($id)
    {
        $instance = new static();

        $cond = !intval($id) ? $id : "{$instance->primaryKey} = '$id'";

        $res = $instance->newQuery()->whereRaw($cond)->delete();
        return $res;
    }


}
