<?php namespace App\Models;
use DB;

class FrmRegisterCompanyLtd extends Model {

    /**
     * @return mixed
     */
    public function document(){
        return $this->belongsTo(Document::class,'document_id','id');
    }

    /**
     * @return mixed
     */
    public function company(){
        return $this->belongsTo(Company::class);
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'xxfrm_register_company_ltd';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'xfrm_register_company_ltdid';

    protected $fillable = ['company_id','finished'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * get Result
     * @param $query
     * @return mixed
     */
    public static function getResult()
    {
        $instance = new static();

        $res = DB::table($instance->table . ' AS rcl')
            ->join('xxdocument as d','d.xdocumentid','=','rcl.xdocumentid')
            ->leftJoin('xxcompany as c','c.xcompanyid','=','rcl.xcompanyid')
            ->select(['*', "{$instance->primaryKey} as xid"]);

        return $res;
    }

    /**
     * get First
     * @param $id
     * @return mixed
     */
    public static function first($id)
    {
        $instance = new static();

        $res = self::getResult()
            ->where("rcl.{$instance->primaryKey}", $id)
            ->first();

        return $res;
    }

    /**
     * update or insert
     * @param $data
     * @param int $id
     * @return int
     */
    public static function store($data, $id = 0)
    {
        $instance = new static();
        if (!$id)
            $id = $instance->newQuery()->insertGetId($data);
        else
            $instance->newQuery()->where($instance->primaryKey, intval($id))->update($data);

        return $id;
    }

    /**
     * delete
     * @param $id
     * @return mixed
     */
    public static function remove($id)
    {
        $instance = new static();

        $cond = !intval($id) ? $id : "{$instance->primaryKey} = '$id'";

        $res = $instance->newQuery()->whereRaw($cond)->delete();
        return $res;
    }

}
