<?php namespace App\Models;
use DB;

class BoardOfDirector extends Model {

    //
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'xxboard_of_director';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'xboard_of_directorid';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * get Result
     * @param $query
     * @return mixed
     */
    public static function getResult()
    {
        $instance = new static();

        $res = DB::table($instance->table . ' AS bd')
            ->leftJoin('xxuser as u' , 'u.xuser_id' , '=' , 'bd.xuser_id')
            ->select(['*', "bd.{$instance->primaryKey} as xid" , DB::raw('Concat(xuser_name ," ", xuser_family) as name')]);

        return $res;
    }

    /**
     * get First
     * @param $id
     * @return mixed
     */
    public static function first($id)
    {
        $instance = new static();

        $res = self::getResult()
            ->where("bd.{$instance->primaryKey}", $id)
            ->first();

        return $res;
    }

    /**
     * update or insert
     * @param $data
     * @param int $id
     * @return int
     */
    public static function store($data, $id = 0)
    {
        $instance = new static();
        if (!$id)
            $id = $instance->newQuery()->insertGetId($data);
        else
            $instance->newQuery()->where($instance->primaryKey, intval($id))->update($data);

        return $id;
    }

    /**
     * delete
     * @param $id
     * @return mixed
     */
    public static function remove($id)
    {
        $instance = new static();

        $cond = !intval($id) ? $id : "{$instance->primaryKey} = '$id'";

        $res = $instance->newQuery()->whereRaw($cond)->delete();
        return $res;
    }

}
