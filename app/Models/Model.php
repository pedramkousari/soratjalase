<?php namespace App\Models;

use DB;
use Schema;
use App\Models\Builder as QueryBuilder;
use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel {

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = array())
    {
        DB::connection()->setFetchMode(\PDO::FETCH_ASSOC);
        parent::__construct($attributes);
    }

	protected function newBaseQueryBuilder()
	{
	    $conn = $this->getConnection();
	    $grammar = $conn->getQueryGrammar();
	    return new QueryBuilder($conn, $grammar, $conn->getPostProcessor());
	}

    /**
     * get Sql and set bindings
     * @return $sql
     */
    public static function getSql() 
    {
        $instance = new static();
        $builder = $instance->getBuilder(); 
        $sql = $builder->toSql();
        
        foreach($builder->getBindings() as $binding) {
          $value = is_numeric($binding) ? $binding : "'".$binding."'";
          $sql = preg_replace('/\?/', $value, $sql, 1);
        }

        return $sql;
    }
    
	 /**
     * fetch Enum
     * @param $table
     * @param $column
     * @return array
     */
    public static function fetchEnum($column, $table = '')
    {
        $instance = new static();
        DB::connection()->setFetchMode(\PDO::FETCH_ASSOC);
        $table = ($table ? $table : $instance->getTable());
        $res = array();

        $sql = "SHOW COLUMNS FROM $table LIKE '$column'";
        $result = DB::select(DB::Raw($sql), array());
 
        if( count( $result ) > 0){
            $result = $result[0];
            $type = @$result['type'] ? $result['type'] : $result['Type'];
            preg_match_all("/'(.*?)'/", $type, $matches);
            if(@$matches[1]){
                foreach($matches[1] as $key=>$val){
                    $res[$val] = $val; 
                }
            }   
        }
        return $res;
    }

    /**
     * get List
     * @param $table
     * @param string $field
     * @param bool $sort
     * @param null $where
     * @return array
     */
    public static function getList($table, $field='', $sort = true, $where = 1) 
    {
        DB::connection()->setFetchMode(\PDO::FETCH_ASSOC);
        $sfield = null;
        $res = array();
        if($sort === true) {
            $field = $field ? $field : substr($table, 1, strlen($table)+1);
        } elseif(gettype($sort)=='string') {
            $sfield = $sort;    
        }
        $fid = substr($table . "id", 1, strlen($table) + 1); 
        $res = DB::table($table)
            ->whereRaw($where)
            ->orderByRaw(($sfield ? $sfield : ($field ? $field : $fid)))
            ->lists($field);
        return $res;    
    }
}