<?php namespace App\Models;

use DB;

class UserGroup extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'xxuser_group';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'xgroupid';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['xgroup'];

    /**
     * get Result
     * @param $query
     * @return mixed
     */
    public static function getResult()
    {
        $instance = new static();

        $res = DB::table($instance->table . ' AS ug')
            ->select(['*', "{$instance->primaryKey} as xid"]);

        return $res;
    }

    /**
     * get First
     * @param $id
     * @return mixed
     */
    public static function first($id)
    {
        $instance = new static();

        $res = self::getResult()
            ->where("ug.{$instance->primaryKey}", $id)
            ->first();

        return $res;
    }

    /**
     * update or insert
     * @param $data
     * @param int $id
     * @return int
     */
    public static function store($data, $id = 0)
    {
        $instance = new static();
        if (!$id)
            $id = $instance->newQuery()->insertGetId($data);
        else
            $instance->newQuery()->where($instance->primaryKey, intval($id))->update($data);
        
        return $id;
    }
    
    /**
     * delete
     * @param $id
     * @return mixed
     */
    public static function remove($id)
    {
        $instance = new static();

        $cond = !intval($id) ? $id : "{$instance->primaryKey} = '$id'";

        $res = $instance->newQuery()->whereRaw($cond)->delete();
        return $res;
    }

    /**
     * The roles that belong to the user.
     * @example UserGroup::find(1)->users->toArray();
     * @example $ug = UserGroup::create(['xgroup'=>'customer']);dd($ug->users()->create(['xusername'=>'pedram','xpassword'=>'123456']));
     * @example UserGroup::find(1)->users()->find(1)->userinfo->toArray()
     * @example $ug = UserGroup::where('xgroup','Customer')->get()->first();
        $res = $ug->users()->create(['xusername'=>'pedramkousari','xpassword'=>bcrypt('-+')]);
     *  OR
     * @example $ug = UserGroup::where('xgroup','Customer')->get()->first();
        $res = User::create([
        'xusername'=> 'mohsen',
        'xpassword'=>bcrypt('sada')
        ]);


        $ug->users()->attach($res);
     */
    public function users()
    {
        return $this->belongsToMany(User::class,'xxuser_role','xgroupid','xuserid');
    }
}