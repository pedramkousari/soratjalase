<?php namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \App\Services\Lib
 */
class Lib extends Facade {

    protected static function getFacadeAccessor() { return 'lib'; }

}