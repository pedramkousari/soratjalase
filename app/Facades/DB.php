<?php namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \App\Models\Connection
 */
class DB extends Facade {

    protected static function getFacadeAccessor() { return 'db'; }

}