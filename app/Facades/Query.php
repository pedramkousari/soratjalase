<?php namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \App\Models\Connection
 */
class Query extends Facade {

    protected static function getFacadeAccessor() { return 'query'; }

}