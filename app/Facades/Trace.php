<?php namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \App\Services\Trace
 */
class Trace extends Facade {

    protected static function getFacadeAccessor() { return 'trace'; }

}