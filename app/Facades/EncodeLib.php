<?php namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \App\Services\Encode
 */
class EncodeLib extends Facade {

    protected static function getFacadeAccessor() { return 'encode'; }

}