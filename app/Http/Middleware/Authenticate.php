<?php namespace App\Http\Middleware;

use Auth;
use App\Models\User;
use Closure;
use Illuminate\Contracts\Auth\Guard;

class Authenticate {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($this->auth->guest()) {

			if ($request->ajax()) {
				if(strtolower($request->segment(1)) == 'customer' || strtolower($request->segment(2)) == 'customer'){
					return "<script>window.location.href = '/'</script>";
				}
				return "<script>window.location.href = '/auth/login'</script>";
				//return response('Unauthorized.', 401);
			} else {
				if(strtolower($request->segment(1)) == 'customer' || strtolower($request->segment(2)) == 'customer'){
					return redirect()->guest('/');
				}
				return redirect()->guest('auth/login');
			}
		}

		return $next($request);
	}

}
