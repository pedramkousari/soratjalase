<?php namespace App\Http\Middleware;

use FarsiLib;
use Closure;

class NormalizeRequest {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$data = $request->all();
		if ($data) {
			$data = self::normalize($data);
		}

		$request->replace($data);
		
		return $next($request);
	}

	//## normalize input
	public function normalize($data) {
		if (is_array($data)) {
			foreach ($data as $key => $value) {
				if (is_array($value)) {
					$data[$key] = self::normalize($value);
				} else {
					$data[$key] = FarsiLib::fa2enDigit(FarsiLib::faNormalize($value));
				}
			}
		}
		return $data;
	}

}
