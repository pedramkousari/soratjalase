<?php

//## clear view cache
if (!function_exists('clearCache')) {
    function clearCache() {
        $cachedViewsDirectory = base_path() . '/storage/framework/views/';
        $files = glob($cachedViewsDirectory . '*');
        foreach ($files as $file) {
            if (is_file($file)) {
                File::delete($file);
            }
        }
    }
}

//## convertDigit
if (!function_exists('convertDigit')) {
    function convertDigit($number, $html = false) {
        return FarsiLib::convertDigit($number, $html);
    }
}

//## translate
if (!function_exists('t')) {
    function t($msg) {
        $lang = config('custom.site.lang') . '.';
        if (strpos($msg, 'language.') === 0) {
            $lang = '';
        }
        return trans($lang . $msg);
    }
}

//## farsi date
if (!function_exists('farsiDate')) {
    function farsiDate($param, $format = 'Y/m/d', $farsiDigit = true) {
        $date = FarsiLib::jDate($format, 0, 0, strtotime($param));
        return ($farsiDigit ? FarsiLib::en2faDigit($date) : $date);
    }
}

//## translation explode 
if (!function_exists('transExplode')) {
    function transExplode($msg, $seperator = ',', $perfix = 'language', $returnSeperator = '، ') {
        return Lib::transExplode($msg, $seperator, $perfix, $returnSeperator);
    }
}
//## getCurrentURL
if (!function_exists('getCurrentURL')) {
    function getCurrentURL($partname = '', $siteurl_perfix = false) {
        return Lib::getCurrentURL($partname, $siteurl_perfix);
    }
}

//## alert
if (!function_exists('alert')) {
	function alert($msg) {
		return Trace::alert($msg);
	}
}

//## trace 
if (!function_exists('trace')) {	
	function trace($msg, $die = false, $dir = 'ltr') {
        $title = null;
	    if (is_string($die)) {
	        $title = $die;
	        $die = ($dir == 'ltr') ? 0 : $dir;
	    }
	   	Trace::add($msg, $title, $dir); 
        Trace::flush();

	    if ($die) die;
	}
}

//## trace all
if (!function_exists('trace')) {
	function traceAll($die = false, $dir = 'ltr')
	{
	    $allInfo['GET'] = @$_GET;
	    $allInfo['POST'] = @$_POST;
	    $allInfo['SESSION'] = @$_SESSION;
	    $allInfo['COOKIE'] = @$_COOKIE;
	    $allInfo['FILES'] = @$_FILES;
	    $allInfo['REQUEST'] = @$_REQUEST;
	    trace($allInfo, $die, $dir);
	}
}        
   

//## create file json of language
function jsLanguage($file = '', $dest = '', $gc = 10) {
    $file = ($file ? $file : (base_path() . '/resources/lang/'. App::getLocale()));
    $fileName = ''; 

    if (File::exists($file)) {
        $fileList = array();
        if (is_dir($file)) {
            foreach (File::glob($file . '/*.php') as $key => $filePath) {
                $fileList[$filePath] = File::lastModified($filePath);
            }
        } else {
            $fileList = array($file => File::lastModified($file));
        }

        $crc32 = crc32(json_encode($fileList));

        $dest = $dest ? (public_path() . $dest) : (public_path() . '/assets/cache/');
        $fileName = 'language_' . $crc32 . '.js';
        if (!is_dir($dest))
            File::makeDirectory($dest, 0777, true);

        if (!File::exists($dest . $fileName)) {
            $language = array();
            foreach ($fileList as $filePath => $lastModified) {  
                $language = array_merge($language, array_dot(File::getRequire($filePath)));
            }
            $translate = '{';
            if ($language) {
                $cntLang = 0;
                foreach ($language as $kl => $vl) {
                    $cntLang++;
                    if (is_array($vl)) continue;
                    $translate .= '"' . e($kl) . '":"' . e($vl) . '"' . ($cntLang < count($language) ? ',': '');
                }  
                $translate .= "};"; 
            }
            $destFile = "var translate=" . $translate; 

            //## delete language file
            $langFileList = File::glob($dest . 'language_*.js');
            if (count($langFileList) >= $gc) {
                foreach ($langFileList as $langFile) {
                    File::delete($langFile);
                }
            }
            // set new file
            File::put($dest . $fileName, $destFile);
        }
    }
    return $fileName;
}

//## compress assets
function compareAssets($assets = array(), $gc = 20) {
    //md5(implode($assets));
    $css_regex = '/.\.css$/i';
    $js_regex = '/.\.js$/i';
    $timestamp = Config::get('assets.pipeline');
    if (is_array($assets) and $timestamp) {
        $fileList = array();
        $cacheDir = Config::get('assets.css_dir');
        $ext = 'css';
        foreach ($assets as $key => $val) {    
            $filePath = public_path() . $val;
            if (File::exists($filePath)) {              
                $fileList[$filePath] = File::lastModified($filePath);
                if (preg_match($js_regex, $val)) {
                    $cacheDir = Config::get('assets.js_dir') ? Config::get('assets.js_dir') : $cacheDir;
                    $ext = 'js';
                }  
            }            
        }        
        $timestamp = crc32(json_encode($fileList));
        $cacheDir = public_path() . $cacheDir . '/' . Config::get('assets.pipeline_dir');  
        $cacheFile = $cacheDir . '/' . md5('?' . $timestamp . implode($assets)) . '.' . $ext;

        if (!File::exists($cacheFile) and count(File::allFiles($cacheDir)) > $gc) { 
            foreach (glob($cacheDir . '/*') as $file) {             
                if (preg_match("/\/[0-9a-f]{32}(?:[0-9a-f]{8})?\.$ext/i", $file)) {
                    File::delete($file);
                }
            } 
        }  
    }

    return $timestamp;
}

// restore lost tags
function restoreTags($input)
{
    $opened = array();

    // loop through opened and closed tags in order
    if(preg_match_all("/<(\/?[a-z][a-z1-6]*)>?/i", $input, $matches)) {
        foreach($matches[1] as $tag) {
            if(preg_match("/^[a-z][a-z1-6]*$/i", $tag, $regs)) {
            // a tag has been opened
                if(!preg_match('/^(br|input|hr|meta|base|basefont)$/i', $regs[0])) $opened[] = $regs[0];
            } elseif(preg_match("/^\/([a-z][a-z1-6]*)$/i", $tag, $regs)) {
            // a tag has been closed
                unset($opened[array_pop(array_keys($opened, $regs[1]))]);
            }
        }
    }

    // close tags that are still open
    if($opened) {
        $tagstoclose = array_reverse($opened);
        foreach($tagstoclose as $tag) $input .= "</$tag>";
    }

    return $input;
}

// truncate text smart
function truncate($string, $limit, $break=" ", $pad="...")
{
    // return with no change if string is shorter than $limit
    if(strlen($string) <= $limit) return $string;

    // is $break present between $limit and the end of the string?
    if(($breakpoint = strpos($string, $break, $limit)) !== false) {
        if($breakpoint < strlen($string) - 1) {
            $string = restoreTags(substr($string, 0, $breakpoint)) . $pad;
        }
    }

    return $string;
}


//## status 
if (!function_exists('status')) {  
    function status($status, $lang = '', $textAlign = 'center') {
        $lang = (trim($lang) ? $lang . '.' : '');
        switch ($status) {
            case 'pending':
                $return =  '<span class="label label-warning">' . t($lang . $status) . '</span>';
                break;  
            case 'confirm':         
            case 'active':
                $return =  '<span class="label label-success">' . t($lang . $status) . '</span>';
                break;  
            case 'disapproval':          
            case 'fail':
            case 'deleted':
                $return =  '<span class="label label-danger">' . t($lang . $status) . '</span>';
                break;            
            case 'revision':
                $return =  '<span class="label label-info">' . t($lang . $status) . '</span>';
                break;
            case 'anuoncment':
                $return =  '<span class="label label-primary">' . t($lang . $status) . '</span>';
                break;
            case 'archive':
            case 'inactive':
                $return =  '<span class="label label-default">' . t($lang . $status) . '</span>';
                break;
            default:
                $return =  "وضعیت نامشخص";
                break;
        }
        $textAlign = (($textAlign == 'right') ? 'left' : ($textAlign == 'left' ? 'right' : $textAlign));
        return "<div class=\"text-{$textAlign}\">{$return}</div>";
    }
}

if ( !function_exists('documentType') ) {
    function documentType($index) {

        $documentType = [
            1   => 'liquidation',
            7	=> 'changeAddress',
            26 	=> 'registerCompany',
        ];

        return $documentType[$index];
    }
}

if ( !function_exists('encryptId') ) {
    function encryptId($id) {
        $id = $id.date('Y/m/d');
        return Crypt::encrypt($id);
    }
}
if ( !function_exists('decryptId') ) {
    function decryptId($id) {
        try {
            $id = intval(substr(Crypt::decrypt($id), 0, -10));
        } catch (Exception $e) {
            $id=0;
        }

        return $id;
    }
}
if ( !function_exists('setStrLength') ) {
    function setStrLength($string , $len) {
        $strLen = strlen($string);
        $abs = $len-$strLen;
        if($abs > 0){
            $str = '';
            for($abs;$abs>0;$abs--){
                $str .='0';
            }
        }
        return @$str.$string;
    }
}