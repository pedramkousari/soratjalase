<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/welcome', 'WelcomeController@index');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

//## debugbar
Route::get('open', [
    'uses' => 'OpenHandlerController@handle',
    'as' => 'debugbar.openhandler',
]);
Route::get('assets/stylesheets', [
    'uses' => 'AssetController@css',
    'as' => 'debugbar.assets.css',
]);

Route::get('assets/javascript', [
    'uses' => 'AssetController@js',
    'as' => 'debugbar.assets.js',
]);
//---

//## captcha
Route::get('captcha/{config?}', '\Mews\Captcha\CaptchaController@getCaptcha');

Route::get('home', 'HomeController@index');

Route::any('/pic/{folder?}/{filename?}/{output?}', function($folder, $filename, $output) {
    $args = $folder . '/' . $filename . '/' . $output;
    return Lib::callAction('', 'pic', 'index', $args);
});
Route::any('/download/{folder?}/{filename?}/{cid?}/{output?}', function($folder, $filename, $cid, $output) {
    $args = "{$folder}/{$filename}/{$cid}/{$output}";
    return Lib::callAction('', 'pic', 'download', $args);
});

Route::resource('password', 'RemindersController', array(
    'only' => array('index', 'store', 'show', 'update')
));

$locale = Request::segment(1);
if (in_array($locale, Config::get('app.locales'))) {
    App::setLocale($locale);
    Config::set('app.localization', $locale);
} else if (Request::get('lang') && in_array(Request::get('lang'), Config::get('app.locales'))) {
    App::setLocale(Request::get('lang'));
    $locale = null;
} else {
    $locale = null;
}

$dir = (App::getLocale() == 'fa') ? 'rtl' : 'ltr';
Config::set('app.dir', $dir);

Route::get('/' . ($locale ? "$locale/" : '') , function() {
    return Lib::callAction('Index', 'Home');
});

Route::get(($locale ? "/$locale/" : '') . 'login', function() {
    return Lib::callAction('', 'Auth');
});

Route::group(array('prefix' => $locale), function () {
    $prefixes = Config::get('app.prefixes');
	foreach ($prefixes as $prefix) {
		Route::group(array('prefix' => $prefix, 'namespace' => studly_case($prefix), 'middleware' => ['auth', 'normalize']), function () use ($prefix) {

            Route::get('/', function() use ($prefix) {
                return Lib::callAction($prefix, 'Home');
            });

            Route::any('{controller?}/{action?}/{args?}', function ($controller, $action = 'index', $args = '') use($prefix) {
                if ($controller == 'logout') 
                    return Lib::callAction('', 'auth', 'logout', $args);

                // $section = in_array(Request::segment(1), Config::get('app.locales')) ? Request::segment(2) : Request::segment(1);
                return Lib::callAction($prefix, $controller, $action, $args);
			})->where(array(
					'controller' => '[^/]+',
					'action'     => '[^/]+',
					'args'       => '[^?$]+'
			));
		});
	}

    //## Index Routing
    Route::any('{controller}/{action?}/{args?}', function ($controller, $action = 'index', $args = '') {
        return Lib::callAction('Index', $controller, $action, $args);
    })->where(array(
            'controller' => '[^/]+',
            'action'     => '[^/]+',
            'args'       => '[^?$]+'
    ));
});