<?php namespace App\Http\Controllers\Customer;

use App\Http\Controllers\IndexController;
use App\Models\Company;
use App\Models\Document;
use App\Models\DocumentType;
use App\Models\FrmChangeAddress;
use App\Models\FrmLiquidation;
use App\Models\FrmRegisterCompanyLtd;
use App\Models\UserGroup;
use App\Models\UserInfo;
use App\Models\User;
use App\Models\UserRole;
use Validator;
use Input;
use Auth;
use Session;


class HomeController extends IndexController
{

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    public function __construct()
    {
        parent::__construct();
        $this->layout = 'customer.layout';
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {
        $title = 'انتخاب نوع اسناد یا صورتجلسه';
        $title_icon = 'fa-th';

//		$faker = \Faker\Factory::create('fa_IR');
//		dd($faker->hexColor);


        $documentTypeList = DocumentType::getResult()->lists('xdocument_type','xdocument_typeid');

        $btnFinishTitle = 'ادامه';
        $this->layout->content = view('customer.home', compact('title','btnFinishTitle', 'title_icon'));
        $this->layout->content->form = view('customer.frm.choose', compact('documentTypeList'));
    }

    public function form()
    {
        $userId = Auth::user()->xuserid;
        $frm = Input::get('frm');
        $documentTypeId = $frm['xdocument_typeid'];

        $title = DocumentType::first($documentTypeId)['xdocument_type'];

        if ($documentTypeId == 26) {

            $document = FrmRegisterCompanyLtd::getResult()
                ->where('d.xuserid',$userId)
                ->where('xfrm_register_company_ltd_finished' , 0)
                ->first();

            $documentId = $document['xdocumentid'];
            $companyId = $document['xcompanyid'];


            if (!$document['xdocumentid']) {
                //Store Document
                $documentId = Document::store([
                    'xuserid' => $userId,
                    'xdocument_typeid' => $documentTypeId,
                ]);

                FrmRegisterCompanyLtd::store([
                    'xdocumentid' => $documentId,
                ]);
            }

            Session::put('documentId',$documentId);
            Session::save();

            $company = $companyId ? Company::first($companyId) : null;

            $title_icon = 'fa-th';
            $type = 'با مسئولیت محدود';
            $step_cnt = 7;
            $url = "/Customer/RegisterCompany";
            $this->layout->content = view('customer.home', compact('title', 'step_cnt', 'title_icon'));
            $this->layout->content->form = view('customer.frm.register_company_limited', compact('type', 'documentId', 'companyId' , 'company' , 'url'));
        }



        if ($documentTypeId == 7) {

            $document = Document::getResult()
                ->where('xuserid',$userId)
                ->whereNull('xdocument_code')
                ->where('xdocument_typeid', $documentTypeId)
                ->first();

            if (!@$document['xdocumentid']) {
                //Store Document
                $documentId = Document::store([
                    'xuserid' => $userId,
                    'xdocument_typeid' => $documentTypeId,
                ]);
            }else{
                $documentId = $document['xdocumentid'];

                $data = FrmChangeAddress::getResult()
                    ->where('xdocumentid' , $documentId)
                    ->first();
            }

            $companyId = @$data['xcompanyid'] ? encryptId($data['xcompanyid']) : 0;
            $frm_dataId    = @$data['xfrm_dataid'] ? encryptId($data['xfrm_dataid']) : 0;
            $frmId    = @$data['xid'] ? encryptId($data['xid']) : 0;
            $data = @$data ? $data : null;

            Session::put('documentId',$documentId);
            Session::save();

            $url = "/Customer/ChangeAddress";
            $title_icon = 'fa-th';
            $step_cnt = 1;
            $this->layout->content = view('customer.home', compact('step_cnt', 'title', 'title_icon'));
            $this->layout->content->form = view('customer.frm.change_address', compact('documentId','url' ,'data' , 'companyId' ,'frm_dataId' ,'frmId'));
        }

        if ($documentTypeId == 1) {

            $document = Document::getResult()
                ->where('xuserid',$userId)
                ->whereNull('xdocument_code')
                ->where('xdocument_typeid', $documentTypeId)
                ->first();

            if(!@$document['xdocumentid']) {
                //Store Document
                $documentId = Document::store([
                    'xuserid' => $userId,
                    'xdocument_typeid' => $documentTypeId,
                ]);
            }else{
                $documentId = $document['xdocumentid'];

                $data = FrmLiquidation::getResult()
                    ->where('xdocumentid' , $documentId)
                    ->first();
            }

            $frm_dataId    = @$data['xfrm_dataid'] ? encryptId($data['xfrm_dataid']) : 0;
            $frmId    = @$data['xid'] ? encryptId($data['xid']) : 0;
            $data = @$data ? $data : null;

            Session::put('documentId',$documentId);
            Session::save();

            $url = "/Customer/ChangeAddress";
            $title_icon = 'fa-th';
            $step_cnt = 1;
            $this->layout->content = view('customer.home', compact('step_cnt', 'title', 'title_icon'));
            $this->layout->content->form = view('customer.frm.liquidation', compact('url' ,'data' , 'companyId' ,'frm_dataId' ,'frmId'));
        }
    }
}
