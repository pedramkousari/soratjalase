<?php namespace App\Http\Controllers\Customer;
use App\Http\Controllers\IndexController;
use App\Models\Company;
use App\Models\Document;
use App\Models\FrmData;
use FarsiLib;
use Input;

class DataController extends IndexController{
    /**
     * Store Company
     * @return int|mixed
     */
    public function storeCompany($document_type){
        $company = Input::get('company');
        $companyId = decryptId(Input::get('companyId'));

        $company['xfrm_type'] = $document_type;

        $company['xcompany_money'] = @$company['xcompany_money'] ? str_replace(',','',$company['xcompany_money']) : null;

        $companyId = Company::store($company , $companyId);
        return $companyId;
    }

    /**
     * @param null $frmid
     * @param null $frmType
     * @param null $companyId
     * @param null $dataid
     *
     * @return nt|mixied
     */
    public function storeData($frmid=null ,$frmType=null, $companyId = null){
        $data = Input::get('data');
        $dataId = decryptId(Input::get('frm_dataId'));

        $data['xcompanyid'] = $companyId ? $companyId : null ;
        $data['xfrm_id'] = $frmid ? $frmid : null ;
        $data['xfrm_type'] = $frmType ? $frmType : null ;

        $data['xfrm_data_date_of_meet'] = $data['xfrm_data_date_of_meet'] ? FarsiLib::j2gDate($data['xfrm_data_date_of_meet']) : '0000-00-00';
        $dataId = FrmData::store($data , $dataId);
        return $dataId;
    }

    public function storeDocumentCode($documentId , $document_typeId , $userId , $formId){

        $years = substr(farsiDate(date('Y/m/d'),'Y' ,false),-2);
        $document_typeId = setStrLength($document_typeId, 2);
        $userId = setStrLength($userId, 5);

        $string = $years . '-' . $document_typeId . '-' .$userId . '-'  . $formId;

        Document::store(['xdocument_code' => $string],$documentId);

        return $string;

    }
}