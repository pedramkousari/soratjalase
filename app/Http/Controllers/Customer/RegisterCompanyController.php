<?php namespace App\Http\Controllers\Customer;

use App\Http\Controllers\IndexController;
use App\Http\Requests;

use App\Models\Activity;
use App\Models\BoardOfDirector;
use App\Models\Branch;
use App\Models\Company;
use App\Models\CompanyName;
use App\Models\Document;
use App\Models\FrmRegisterCompanyLtd;
use App\Models\User;
use App\Models\UserGroup;
use App\Models\UserPartner;
use App\Models\Users;
use Auth;
use Faker\Factory;
use FarsiLib;
use Illuminate\Http\Request;
use Input;
use Session;

class RegisterCompanyController extends IndexController
{

    protected $fake;

    /**
     * RegisterCompanyController constructor.
     */
    public function __construct()
    {
        $this->fake=Factory::create();
//        parent::_construct();
    }

    public function stepLoad1(){

        $documentId = Session::has('documentId') ? Session::get('documentId') : 0;
        $companyId = Session::has('companyId') ? Session::get('companyId') : 0;
        $company = Company::first($companyId);

        $type = 'با مسئولیت محدود';
        return response()
            ->json([
                '.form-step1',
                view('customer.frm.inc.register_company_step1',compact('type', 'step_cnt', 'documentId', 'companyId' , 'company'))->render()
            ]);
    }

    public function stepLoad2(){

        $userPartner = Users::getResult()
            ->join('xxuser_partner as up','up.xuser_id','=','u.xuser_id')
            ->where('up.xuser_partner_type','xxuser')
            ->get();

        $companyPartner = Company::getResult()
            ->join('xxuser_partner as up','up.xuser_id','=','c.xcompanyid')
            ->where('up.xuser_partner_type','xxcompany')
            ->get();

        $userPartner = count($userPartner) ? $userPartner : [1,1];
        $companyPartner = count($companyPartner) ? $companyPartner : [1];

        return response()
            ->json([
                '.form-step2',
                view('customer.frm.inc.register_company_step2',compact('userPartner','companyPartner'))->render()
            ]);
    }

    public function stepLoad3(){

        $userPartner = Users::getResult()
            ->join('xxuser_partner as up','up.xuser_id','=','u.xuser_id')
            ->where('up.xuser_partner_type','xxuser')
            ->where('xcompanyid',Session::get('companyId'))
            ->get();

        $addressList = [];
        foreach ($userPartner as $key => $value) {
            $addressList[$value['xid']] = $value['xuser_address'];
        }

        $json_address = json_encode($addressList);

        return response()
            ->json([
                '.form-step3',
                view('customer.frm.inc.register_company_step3',compact('userPartner','json_address'))->render()
        ]);
    }

    public function stepLoad4(){
        $userPartner = Users::getResult()
            ->join('xxuser_partner as up','up.xuser_id','=','u.xuser_id')
            ->where('up.xuser_partner_type','xxuser')
            ->where('xcompanyid',Session::get('companyId'))
            ->lists('name', 'xid');

        $boardType = BoardOfDirector::fetchEnum('xboard_of_director_type');

        $userBoard = BoardOfDirector::getResult()
            ->where('xcompanyid',Session::get('companyId'))
            ->get();
        $userBoard = $userBoard?$userBoard:[1];


        return response()
            ->json([
                '.form-step4',
                view('customer.frm.inc.register_company_step4',compact('boardType','userPartner','userBoard'))->render()
        ]);
    }

    public function stepLoad5(){
        $companyName = CompanyName::getResult()
            ->where('xcompanyid',Session::get('companyId'))
            ->get();

        $companyName = $companyName?$companyName:[1];
        return response()
            ->json([
                '.form-step5',
                view('customer.frm.inc.register_company_step5',compact('companyName'))->render()
            ]);
    }

    public function stepLoad6(){
        $branch = Branch::getResult()
            ->where('xcompanyid',Session::get('companyId'))
            ->get();

        $branch = $branch?$branch:[1];
        return response()
            ->json([
                '.form-step6',
                view('customer.frm.inc.register_company_step6',compact('branch'))->render()
            ]);
    }

    public function stepLoad7(){
        $activityList = Activity::getResult()->lists('xactivity','xid');
        $userBoard = BoardOfDirector::getResult()
            ->where('xcompanyid',Session::get('companyId'))
            ->lists('name','u.xuser_id');


        $userList = $userBoard?$userBoard:[1];


        return response()
            ->json([
                '.form-step7',
                view('customer.frm.inc.register_company_step7',compact('activityList','userList'))->render()
            ]);
    }

    public function step1()
    {

        $documentId = @Input::get('documentId');
        $companyId = @Input::get('companyId');
        $company = Input::get('company');

        $company["xcompany_money"] = str_replace(',','',$company["xcompany_money"]);
        $company["xcompany_type"] = 'مسئولیت محدود';

        //sample
//        $documentId = $documentId?$documentId:0;
//        $companyId = $companyId?$companyId:0;
//
//
//
//        $company = [
//            "xcompany_money" => $this->fake->numberBetween(100000,99999999999),
//            "xcompany_address" => 'asdsada',
//            "xcompany_postal_code" => $this->fake->postcode,
//            "xcompany_type" => 'مسئولیت محدود',
//        ];
//
//        dump($company);

        $companyId = Company::store($company,$companyId);

        Session::put('companyId', $companyId);
        Session::save();

        Session::put('documentId', $companyId);
        Session::save();

        FrmRegisterCompanyLtd::store(['xcompanyid'=>$companyId],$documentId);

        return $this->stepLoad2();

    }

    public function step2()
    {
        $companyPartner = Input::get('companyPartner',[]);
        $userPartner = Input::get('userPartner',[]);

        foreach($userPartner as $key => $user){
            if(!@$user['xuser_partner_stock']) continue;

            $partner['xuser_partner_stock'] = $user['xuser_partner_stock'];
            unset($user['xuser_partner_stock']);
            $partner['xuser_partner_money'] = str_replace(',','',$user['xuser_partner_money']);
            unset($user['xuser_partner_money']);

            $xuser_id = @$user['xuser_id'] ? $user['xuser_id'] : 0;
            unset($user['xuser_id']);
            $xuser_partnerid = @$user['xuser_partnerid'] ? $user['xuser_partnerid'] : 0;
            unset($user['xuser_partnerid']);

            $user['xuser_birth_date'] = $user['xuser_birth_date']? FarsiLib::j2gDate($user['xuser_birth_date']) : '0000-00-00 00:00:00';
            $userId = Users::store($user , $xuser_id);

            if($userId){
                $partner['xuser_id'] = $userId;
                $partner['xuser_partner_type'] = 'xxuser';
                if (!Session::has('companyId'))
                {
                    return response()
                        ->json([
                            t('msg.companyId not set' , 422)
                        ]);
                }

                $partner['xcompanyid'] = Session::get('companyId');

                UserPartner::store($partner , $xuser_partnerid);
            }
        }

        foreach($companyPartner as $key => $company){
            if(!$company['xuser_partner_stock']) continue;

            $partner['xuser_partner_stock'] = $company['xuser_partner_stock'];
            unset($company['xuser_partner_stock']);
            $partner['xuser_partner_money'] = str_replace(',','',$company['xuser_partner_money']);
            unset($company['xuser_partner_money']);

            $xcompanyid = @$company['xcompanyid'] ? $company['xcompanyid'] : 0;
            unset($company['xcompanyid']);
            $xuser_partnerid = @$company['xuser_partnerid'] ? $company['xuser_partnerid'] : 0;
            unset($company['xuser_partnerid']);

            $companyId = Company::store($company , $xcompanyid);

            if($companyId){
                $partner['xuser_id'] = $companyId;
                $partner['xuser_partner_type'] = 'xxcompany';
                if (!Session::has('companyId'))
                {
                    return response()
                        ->json([
                            t('msg.companyId not set' , 422)
                        ]);
                }

                $partner['xcompanyid'] = Session::get('companyId');

                UserPartner::store($partner , $xuser_partnerid);
            }
        }

//        foreach($companyPartner as $key => $company){
//            if(!$company['xuser_partner_stock']) continue;
//
//            $partner['xuser_partner_stock'] = $company['xuser_partner_stock'];
//            unset($company['xuser_partner_stock']);
//            $partner['xuser_partner_money'] = str_replace(',','',$company['xuser_partner_money']);
//            unset($company['xuser_partner_money']);
//
//            $xcompanyid = @$user['xcompanyid'] ? $user['xcompanyid'] : 0;
//            unset($user['xcompanyid']);
//            $xuser_partnerid = @$user['xuser_partnerid'] ? $user['xuser_partnerid'] : 0;
//            unset($user['xuser_partnerid']);
//
//            $companyId = Company::store($company , $xcompanyid);
//
//            if($companyPartnerId){
//                $partner['xuser_id'] = $companyPartnerId;
//                $partner['xuser_partner_type'] = 'xxcompany';
//                if (!Session::has('companyId'))
//                {
//                    return response()
//                        ->json([
//                            t('msg.companyId not set' , 422)
//                        ]);
//                }
//
//                $partner['xcompanyid'] = Session::get('companyId');
//                UserPartner::store($partner , $xuser_partnerid);
//            }
//        }

        return $this->stepLoad3();


    }

    public function step3()
    {
        $frm = Input::get('frm');
        $xuserid = $frm['xuser_id'];
        unset($frm['xuser_id']);

        if($xuserid){
            Users::store($frm,$xuserid);
        }

        return $this->stepLoad4();
    }

    public function step4()
    {
        $userList = Input::get('user');
        $userBoardList = Input::get('userBoard');

        $companyId = Session::get('companyId');
        if($companyId){
            BoardOfDirector::where('xcompanyId', '=', $companyId)->delete();
        }

        foreach($userBoardList as $key=>$userBoard){
//            $boardId = $userBoard['xboard_of_directorid'];
            unset($userBoard['xboard_of_directorid']);

            if(@$userBoard['xboard_of_director_non_partner']){
                $xuserid = @$userList[$key]['xuser_id'];
                unset($userList[$key]['xuser_id']);

                $userBoard['xuser_id'] = Users::store($userList[$key]);
            }

            $userBoard['xcompanyid'] = $companyId;
            BoardOfDirector::store($userBoard);
        }

        return $this->stepLoad5();
    }

    public function step5()
    {
        $companyNameList = Input::get('companyName');
        $companyId = Session::get('companyId');

//        if($companyId){
//            BoardOfDirector::where('xcompanyId', '=', $companyId)->delete();
//        }

        $cnt = 0;
        foreach($companyNameList as $key => $companyName){

            if($cnt == 0) $companyName['xcompany_name_main'] = 1;

            $companyNameId = $companyName['xcompany_nameid'];
            unset($companyName['xcompany_nameid']);

            $companyName['xcompanyid'] = $companyId;
            CompanyName::store($companyName , $companyNameId);

            $cnt++;
        }

        return $this->stepLoad6();
    }

    public function step6()
    {
        $branchList = Input::get('branch');
        $user = Input::get('user');


        $companyId = Session::get('companyId');

//        if($companyId){
//            BoardOfDirector::where('xcompanyId', '=', $companyId)->delete();
//        }


        foreach($branchList as $key => $branch){
            if($branch['xbranch_postal_code']=='' || $branch['xbranch_address']=='')
                continue;

            $branchId = $branch['xbranchid'];
            unset($branch['xbranch_nameid']);



            $xuserid = @$user[$key]['xuser_id'];
            unset($user[$key]['xuser_id']);
            $user[$key]['xuser_birth_date'] = $user[$key]['xuser_birth_date']? FarsiLib::j2gDate($user[$key]['xuser_birth_date']) : '0000-00-00 00:00:00';
            $branch['xuser_id'] = Users::store($user[$key] , $xuserid);


            $branch['xcompanyid'] = $companyId;
            Branch::store($branch , $branchId);
        }

        return $this->stepLoad7();
    }


    public function finish()
    {
        $company = Input::get('frmCompany');
        $register = Input::get('frmRegister');

        $companyId = Session::get('companyId');
        $documentId = Session::get('documentId');

        if($companyId){
            Company::store($company,$companyId);
        }
        if($documentId){
            $register['xfrm_register_company_ltd_finished'] = 1;
            FrmRegisterCompanyLtd::store($register,$documentId);

            $document['xdocument_code'] =
            Document::store($document , $documentId);
        }else{

        }
    }



    public function deleteUserPartner($id){
        if(@$id)
            UserPartner::remove(@$id);
    }

    public function deleteUserBoard($id){
        if(@$id)
            BoardOfDirector::remove(@$id);
    }

    public function deleteCompanyName($id){
        if(@$id)
            CompanyName::remove(@$id);
    }
    public function deleteBranch($id){
        if(@$id)
            Branch::remove(@$id);
    }

}
