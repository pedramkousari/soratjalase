<?php namespace App\Http\Controllers\Customer;

use App\Models\Document;
use App\Models\DocumentType;
use App\Models\FrmChangeAddress;
use Auth;
use Session;
use Input;

class ChangeAddressController extends DataController
{

    public function finish()
    {

        if(Session::Has('documentId')){
            $documentId = Session::get('documentId');

            $frm = Input::get('frm',[]);
            $frm['xdocumentid'] = $documentId;

            $document_typeId = Document::first($frm['xdocumentid'])['xdocument_typeid'];
            $document_type = documentType($document_typeId);

            $frmid = decryptId(Input::get('frmId'));


            $frmid = FrmChangeAddress::store($frm , $frmid);

            $companyId = $this->storeCompany($document_type);
            $dataId = $this->storeData($frmid , $document_type, $companyId);

            $userId = Auth::id();
            $documentCode = $this->storeDocumentCode($documentId, $document_typeId, $userId, $frmid);
            $title = DocumentType::first($document_typeId)['xdocument_type'];

            return response()
                ->json([
                    '.wizard',
                    view('customer.frm.inc.finish' , compact('documentCode','title'))->render()
                ]);
        }


    }

}
