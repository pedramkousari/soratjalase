<?php namespace App\Http\Controllers;

abstract class IndexController extends Controller {
    public $title;
    public $lang;
    protected $layout = 'index.layout';

    public function __construct()
    { 
        $this->lang = config('custom.site.lang');
    }
    
    protected function setupLayout()
    {   
        if (env('APP_ENV') == 'local') {
            clearCache();
        }
        if ( ! is_null($this->layout)) {
            $this->layout = view($this->layout, array('title' => $this->title));
        }
        if (method_exists($this, 'setCombo')) {
            $this->setCombo();
        }
        $this->startUp();
    }

    public function startUp()
    {
    }

}