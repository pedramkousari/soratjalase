<?php namespace App\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Services\Query;
use Exception;
use DateTime;
use Session;
use Request;
use Config;
use File;
use Input;
use Auth;
use DB;

class BackendController extends Controller {
    public $title;
    public $query;
    public $list;
    public $expand;
    public $view = 'admin';
    public $adminStatus = 'offline';
    public $perPage;
    protected $layout = 'backend.layout';
    public $lang;

    public function __construct()
    { 
        $queryItem = Request::get('q');
        $this->userId = auth()->id();
        $this->uploadPath = config('upload.' . config('app.controller'));
        $this->lang = config('custom.site.lang');

        //## set list limit
        if (@$_COOKIE['perPage']) Config::set('custom.perPage', $_COOKIE['perPage']);
        if (@$queryItem['limit']['count']) {
            Config::set('custom.perPage', $queryItem['limit']['count']);
        }        
        $this->perPage = config('custom.perPage', 10);
        
        //## search
        $this->query = new Query($queryItem);
    }

    /**
     * Count Query Result
     */
    public function queryCount($stmt, $maxLimit = 10000, $addSelect = []) 
    { 
        if (is_array($stmt->columns)) {
            foreach ($stmt->columns as $key => $col) {
                if (trim($col) == '*' or preg_match('/[^\s]+\.\*$/', $col)) {
                    unset($stmt->columns[$key]);
                } 
            }
        }

        if ($addSelect) {
            $stmt->addSelect($addSelect);
        }

        $bindings = $stmt->getBindings();
        $countStmt = $stmt->skip(0)->take($maxLimit);
        $countStmt->orders = null;
        $sql = $countStmt->toSql();

        foreach ($bindings as $val) {
            $sql = preg_replace('/\?/', "'{$val}'", $sql, 1);
        }
        return $stmt->getConnection()->table(DB::raw("($sql) as countStmt"))
            ->addSelect([DB::raw('COUNT(*) as total')]);
    }

    /**
     * Paginage List
     */
    protected function paginate($view = 'backend.list', $params = [], $callBack = '') 
    {   
        $count = 0;
        if (!is_array($this->result) and $this->query) {
            if ($this->query->where and !intval($this->query->where))  {    //## where
                $this->result->whereRaw($this->query->where);
            }  
            if ($this->query->having and !intval($this->query->having))  {  //## having
                $this->result->havingRaw($this->query->having);
            }  
            if ($this->query->group)  {                                       
                $this->result->groupBy(explode(',', $this->query->group));
            }   
            if ($this->query->order AND $this->query->order != 'NULL')  {   //## order by
                $this->result->orders = null;
                $this->result->orderByRaw($this->query->order);
            } 
            
            $this->result = $this->result
                ->skip($this->query->skip)
                ->take($this->query->take);
            $columns = $this->result->columns;
            $orders  = $this->result->orders;

            //## get count of query
            $count = $this->queryCount($this->result)->count();

            //## get query
            $this->result->columns = $columns;
            $this->result->orders = $orders;
            $this->result
                ->skip($this->query->skip)
                ->take($this->query->take);
                
            $this->list = $this->result->get();
        } else { 
            throw new Exception('Deprecated ModelName::getResult($this->query)->get(), please use ModelName::getResult()', 1);            
        }

        //## run clouser after list 
        if (is_callable($view)) {
            $this->list = $view($this->list);
            $view = 'backend.list';
        } 

        if (is_callable($params)) {
            $this->list = $params($this->list);
            $params = [];
        }
        
        if (is_callable($callBack)) {
            $this->list = $callBack($this->list);
        }
      
        $this->list = new Paginator($this->list, $count, $this->perPage, null, [
            'path' => Request::url(),
            'query' => Request::query()
        ]);

        $params = ($params ? $params : [
            'list' => $this->list, 
            'field' => @$this->field,
            'expand' => @$this->expand 
        ]);   

        if (Request::ajax()) {
            $this->layout = view('backend.ajax');
        } 
        if ($view) {
            $this->layout->content = (is_object($view) ? $view : view($view, $params));    
        }
    }
    
    /**
     * Setup layout
     */
    protected function setupLayout()
    {
        $viewPath = base_path() . '/resources/views/';
        $currentSection = camel_case(config('app.section'));
        $currentController = camel_case(config('app.controller'));
        $currentAction = config('app.action');

        if (env('APP_ENV') == 'local') {
            clearCache();
        }
        
        if (!is_null($this->layout)) {
            $views = config('custom.view.' . studly_case($currentSection), config('custom.view.' . studly_case($currentSection)));
            $view = '';
            $searchView = $view;
    
            if (is_array($views)) {
                foreach($views as $sec => $v) {
                    $v = camel_case($v);
                    $viewExist = File::exists($viewPath . $v . '/edit/' . $currentController . '.blade.php') or
                        File::exists($viewPath . $v . '/edit/'  . $currentController . '.php');                        
                    $searchViewExist = File::exists($viewPath . $v . '/search/' . $currentController . '.blade.php') or
                        File::exists($viewPath . $v . '/search/'  . $currentController . '.php');

                    if ((preg_match('/^\w/', $sec) and strtolower(trim($sec)) == strtolower($currentSection)) and $viewExist and !$view) {
                        $view = $v;
                    } elseif (strtolower(trim($v)) == strtolower($currentSection) and $viewExist and !$view) {
                        $view = $v;
                    } elseif ($viewExist and !$view) {
                        $view = $v;
                    }

                    //## search view exist
                    if ((preg_match('/^\w/', $sec) and strtolower(trim($sec)) == strtolower($currentSection)) 
                        and $searchViewExist) {
                        $searchView = $v;
                    } elseif (strtolower(trim($v)) == strtolower($currentSection) and $searchViewExist) {
                        $searchView = $v;
                    } elseif ($searchViewExist) {
                        $searchView = $v;
                    }
                }
                $view = is_array($view) ? $view[0] : camel_case($view);
            }
            $this->view = $view ? camel_case($view) : 'backend';
            
            $this->layout = view($this->layout, [
                'title' => $this->title, 
                'lang' => $this->lang
            ]);

            $searchFile = $viewPath . $this->view . '/search/' . $currentController;
            $this->layout->content = view('backend.default', ['lang' => $this->lang]);

            if (File::exists($searchFile . '.blade.php') or File::exists($searchFile . '.php')) {
                $this->layout->tplsearch = $this->view . '.search.' . $currentController;
            } else {            
                $sView = '';
                if (is_array($searchView) && $searchView) {
                    foreach ($searchView as $ks => $vs) {
                        if (File::exists("{$vs}.search.{$currentController}.blade.php") or File::exists("{$vs}.search.{$currentController}.php")) {
                            $sView = $vs . '.search.' . $currentController;
                        }
                    }
                } else {
                    $sView = $searchView . '.search.' . $currentController;
                }
                $searchFile = $viewPath . $searchView . '/search/' . $currentController;

                if (File::exists("{$searchFile}.blade.php") or File::exists("{$searchFile}.php")) {
                    $this->layout->tplsearch = $sView;
                }
            }

            if (preg_match('/^create\_?|^edit\_?/i', $currentAction)) {
                $this->layout = view('backend.' . (Request::ajax() ? 'edit' : 'layout'));
                if (!Request::ajax()) {
                    $this->layout->custom = true;
                    $this->layout->viewEdit = true;
                }
                $currentView = $viewPath . $this->view . '/edit/' . $currentController;

                if (File::exists($currentView . '.blade.php') or File::exists($currentView . '.php')) {
                    $this->layout->content = view($this->view . '.edit.' . $currentController, ['lang' => $this->lang, 'title' => $this->title]);
                } else {
                    $this->layout->content = view('backend.default');
                }

            }
            if (method_exists($this, 'setCombo')) {
                $this->setCombo();
            }
        }
    }
    
    /**
     * get section title
     * @return mixed
     */
    public function getSectionTitle()
    {
        return '';
    }
}