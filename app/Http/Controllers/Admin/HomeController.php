<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BackendController;
use Input;
use Request;
use stdClass;

class HomeController extends BackendController {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/
 	public $title = 'language.dashboard';

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	
	public function index()
	{
		$this->layout->custom = true;
		$this->layout->content = view('admin.home');
	}
   
}
