<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BackendController;
use App\Models\Company;
use Input;
use File;
use View;

class CompanyController extends BackendController {
    public $title = 'company info';

    /**
     * List
     */
	public function index()
	{
        $this->field[] = ['php' => 'echo \'<div style="text-align: center;"><img src="/pic/company.logo/\' . $item["xid"] . "w80h40c1/". str_random() . ".png?nocache=1" . \'" alt="" /></div>\';', 'name' => trans('language.logo'), 'css' => 'width: 3%'];
		$this->field[] = ['php' => 'echo $item["xcompany"];', 'name' => trans('language.name'), 'css' => 'width: 10%', 'sortable' => 'xcompany'];
        $this->field[] = ['php' => 'echo \'<div style="direction: ltr;">\' . convertDigit($item["xcompany_phone"]) . "</div>";', 'name' => trans('language.phone'), 'css' => 'width: 5%', 'sortable' => 'xcompany_phone'];
		$this->field[] = ['php' => 'echo \'<div style="direction: ltr;">\' . convertDigit($item["xcompany_fax"]) . "</div>";', 'name' => trans('language.fax'), 'css' => 'width: 5%', 'sortable' => 'xcompany_fax'];
		$this->field[] = ['php' => 'echo $item["xcompany_address"];', 'name' => trans('language.address'), 'css' => 'width: 10%'];
        $this->field[] = ['php' => 'echo status($item["xcompany_status"])', 'name' => trans('language.status'), 'css' => 'width: 2%', 'sortable' => 'xcompany_status'];

        $this->result = Company::getResult();
        $this->paginate(); 
	}

    /**
     * Create
     * @param $id
     */
    public function create($id = 0)
    {
    }

    /**
     * Edit
     * @param $id
     */
    public function edit($id)
    {

        $this->layout->content->list = Company::first($id);
        $this->layout->content->logoExist = File::exists($this->uploadPath . "logo/$id");
        $this->layout->content->stampExist = File::exists($this->uploadPath . "stamp/$id");
    }

    /**
     * Store
     * @param $id
     */
    public function store($id)
    {
        $frm = Input::get('frm');

        $id = Company::store($frm, $id);
        $this->saveFile($id);

        die('[[OK]]');
    }

    /**
     * Delete
     * @param $id
     */
    public function delete($id)
    {
        if ($id) {
            $res = Company::remove($id);

            File::delete($this->uploadPath . "stamp/$id");
            File::delete($this->uploadPath . "logo/$id");
            die('[[OK]]');
        } else {
            die('[[Error]]');
        }
    }

    /**
     * Delete Logo and stamp
     * @param $id
     */
    public function deleteLogo($id)
    {
        $type = Input::get('type');

        File::delete($this->uploadPath . $type . "/$id");

        die('[[OK]]');
    }

    /**
     * Set Combo run before all methods
     */
    public function setCombo()
    {   
        //## get status list
        $statusList = Company::fetchEnum('xcompany_status');
        $this->layout->statusList = $this->layout->content->statusList = $statusList;
    }
}