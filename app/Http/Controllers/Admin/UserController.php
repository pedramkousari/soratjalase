<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Programmer\UserController as UC;

class UserController extends UC {

	public function index()
	{
        parent::index();
	}

    public function create($id = 0)
    {
        parent::create($id);
    }

    public function edit($id)
    {
        parent::edit($id);
    }

    public function store($id)
    {
        parent::store($id);
    }

    public function delete($id)
    {
      parent::delete($id);
    }

    public function setCombo()
    {
        parent::setCombo();
        if ($this->layout->content->usergrouplist) {
            foreach ($this->layout->content->usergrouplist as $key => $val) {
                if (trim(strtolower($val['xgroup'])) == 'programmer') unset($this->layout->content->usergrouplist[$key]);
            }
        }
    }
}