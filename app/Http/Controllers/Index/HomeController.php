<?php namespace App\Http\Controllers\Index;

use App\Http\Controllers\IndexController;
use App\Models\UserGroup;
use App\Models\UserInfo;
use App\Models\User;
use App\Models\UserRole;
use League\Flysystem\Exception;
use Validator;
use Input;
use Auth;


class HomeController extends IndexController {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = view('index.index');
	}

	/**
	 * register to application
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function register(){

		$rules = array(														#table	   ,cloumn
			'email' => 'required|unique:xxuser_name,xusername|unique:xxuser_name,email',
			'xmobile' => 'required',
			'captcha' => 'required|captcha',
		);

		$frm =  Input::all();
		$validator = Validator::make($frm , $rules);
		if (@$validator->fails()) {
			return response()
				->json($validator->errors()->all(), 422);
		}

		$res = false;
		try{
			$ug = UserGroup::where('xgroup','Customer')->get()->first();
			$res = $ug->users()->create([
				'xusername'=> $frm['email'],
				'xpassword'=>bcrypt($frm['xmobile']),
				'email'=>@$frm['email']
			])->userinfo()->create([
				'xname' => $frm['xname'],
				'xfamily' => $frm['xfamily'],
				'xmobile' => $frm['xmobile'],
			]);
			$res = true;
		}catch(\Exception $e){
			//throw new Exception("Error");
			return response()
				->json([t('خطایی در ثبت اطلاعات رخ داده است')], 422);
		}

		if($res){
			return response()
				->json([
					t('msg.your information was successfully registered'),
					"نام کاربری شما: {$frm['email']}",
					"کلمه عبور شما : {$frm['xmobile']}",
				]);
		}
		else{
			return response()
				->json([t('error')], 422);
		}

	}

	/**
	 * login
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function login(){

		$rules = array(														#table	   ,cloumn
			'username' => 'required',
			'password' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if (@$validator->fails()) {
			return response()
				->json($validator->errors()->all(), 422);
		}

		$username = Input::get('username');
		$password = Input::get('password');

		if (Auth::attempt(['xusername' => $username, 'password' => $password])) {
			return response()
				->json(['group' => 'Customer']);
		}
		return response()
			->json([t('msg.username or password is incorrect')],422);
	}
}
