<?php namespace App\Http\Controllers;

use Intervention\Image\Facades\Image;
use Response;
use Request;
use Input;
use File;
use App;

class PicController extends Controller {

    /**
     * Show the user profile.
     */
    public function index($folders, $name, $output)
    {
        $folderList = explode('.', $folders);
        preg_match('/(\d+)(?:(?:w(\d+))|(?:h(\d+))|(?:mw(\d+))|(?:mh(\d+))|(?:c(\d+))|(?:r(-?\d+))|(?:q(-?\d+)))*/', $name, $fileMatch);
        preg_match('/[^\.]*\.?(jpg|gif|png)/', $output, $outputMatch);
        $ext = 'jpg';
        if ($outputMatch) {
            $ext = @$outputMatch[1];
        }
        
        $contentType = 'Content-Type: image/' . $ext;
        $noCache = Input::get('nocache', false);
        $quality = 90;

        if ($fileMatch) {
            $fileName = @$fileMatch[1];
            $width = @$fileMatch[2] ? $fileMatch[2] : null;
            $height = @$fileMatch[3] ? $fileMatch[3] : null;
            $maxWidth = @$fileMatch[4] ? $fileMatch[4] : null;
            $maxHeight = @$fileMatch[5] ? $fileMatch[5] : null;
            $crop = @$fileMatch[6];
            $rotate = @$fileMatch[7];
            $quality = @$fileMatch[8];
        }

        if (Request::server('HTTP_CACHE_CONTROL') == 'no-cache') {
            File::deleteDirectory(public_path() . '/pic');
        }

        $filePath = storage_path() . '/777/';
        $cacheFolder = public_path() . '/pic/' . str_finish(implode($folderList, '/'), '/') . $fileName . '/';
        $cachePath = $cacheFolder . "/{$output}";

        if (File::exists($cachePath)) {
            header ($contentType);
            echo File::get($cachePath);
            die;
        }

        if ($folderList) {
            foreach ($folderList as $folder) {
                if (!trim($folder)) continue;
                $filePath .= $folder . '/';
            }
        }

        $filePath .= $fileName;

        if (!File::exists($filePath)) {
            $img = Image::canvas(1, 1); // create null image
        } else {
            $img = Image::make($filePath);
            if ($width or $height or $crop) { //## resize (and/or) crop
                $img = $this->resize($img, $width, $height, $crop);
            }

            if ($maxWidth or $maxHeight) { //## max height & max width             
                $img = $img->resize($maxWidth, $maxHeight, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });  
            }

            if ($rotate) { //## rotate
                $img->rotate($rotate);
            }

            if (!$noCache) {
                if (!is_dir($cacheFolder))
                    File::makeDirectory($cacheFolder, 0755, true);
                $img->save($cachePath, $quality);
            }
        }

        header ($contentType);
        echo  $img->encode($ext);
        die;        
    }

    //## resize image
    public function resize($img, $width, $height, $crop = false)
    {
        if($crop) { //## Crop
            $height = $height ? $height : $img->height();
            $width = $width ? $width : $img->width();
            $img = $img->fit($width, $height);
        } else { //## resize
            if ($width and !$height) {
                // resize the image to a width of 300 and constrain aspect ratio (auto height)
                $img->resize($width, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            } else if (!$width and $height) {
                // resize the image to a height of 200 and constrain aspect ratio (auto width)
                $img->resize(null, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
            } else if ($width and $height) {
                $img->resize($width, $height);
            }
        }
        return $img;
    }

    //## download a file
    public function download($folders, $name, $cid, $output)
    {
        $folderList = explode('.', $folders);
        $path = storage_path() . '/777/';
        $folders = '';
        if ($folderList) {
            $cnt = 1;
            foreach ($folderList as $folder) {
                $path .= $folder . '/';
                $folders .= $folder . ($cnt < count($folderList) ? '.' : '');
                $cnt++;
            }
        }
        $path .= $name;
        $folders .= ('/' . $name);

        if (md5($folders . env('APP_KEY')) != $cid) {
            return abort(403);
        }

        if (File::exists($path)) {
            return Response::download($path, $output);
        } else {
            return abort(404);
        }
    }

}