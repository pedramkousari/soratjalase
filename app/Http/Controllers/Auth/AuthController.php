<?php namespace App\Http\Controllers\Auth;

use Request;
use Config;
use Session;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller {
    public $group;

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		Config::set('app.controller', 'auth');
		$this->auth = $auth;
		$this->registrar = $registrar;
		// $this->middleware('guest', ['except' => 'getLogout']);
	}

	/**
	 * Show the application registration form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getRegister()
	{
		return abort(403);
	}

	/**
	 * Handle a registration request for the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postRegister(Request $request)
	{
		return abort(403);
	}

	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(\Illuminate\Http\Request $request)
	{
      	$rules = [
			'username' => 'required', 
			'password' => 'required',
			'captcha'  => 'required|captcha'
		];
		
		$this->validate($request, $rules);

		$msg = trans('language.your user account is under reviewing for final confirmation');
		$inActiveUser = User::whereRaw("LOWER(xusername) = '" . e($request->get('username')) . "' AND xuser_status = 'inactive'")
			->first();
        if ($inActiveUser) {
        	if (Request::ajax()) {
    			return response()
	    			->json([$msg], 422);
        	} else {
	            return back()
	            	->withInput($request->only('username', 'remember'))
	            	->withErrors($msg);        		
        	}
        }

		$credentials = [
			'xusername' => $request->get('username'), 
			'password' => $request->get('password'), 
			'xuser_status' => 'active'
		];

		if ($this->auth->attempt($credentials, $request->has('remember'))) {

            $this->group = array_flip(User::group($this->auth->user()->xuserid));
			if ($this->auth->user()->xuser_type) {
                $this->group = [$this->auth->user()->xuser_type => true];
            } 
            $groups = array_values($this->group);

            if ($this->group and in_array(studly_case($groups[0]), config('app.prefixes'))) {
				if (Request::ajax()) {
					return response()
						->json(['group' => @$groups[0]]);
				}
                return redirect()->intended(studly_case($groups[0]));
			} else {
				$this->auth->logout();
				$msg = trans('language.user group is not defined!');
				if (Request::ajax()) {
        			return response()
	        			->json([$msg], 422);
				} else {
	                return back()
	        			->withInput($request->only('username', 'remember'))
	        			->withErrors($msg);
				}
        	}

        } 

        $msg = trans('language.' . $this->getFailedLoginMessage());
        if (Request::ajax()) {
        	return response()
	        	->json([$msg], 422);
		}
		return back()
			->withInput($request->only('username', 'remember'))
			->withErrors([
				'username' => $msg,
			]);
	}

	/**
	 * Log the user out of the application.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getLogout()
	{  
		$this->auth->logout();

		return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
	}

}
