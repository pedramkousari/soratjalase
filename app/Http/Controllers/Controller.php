<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

    protected $layout;
    
    protected function setupLayout()
    {   
        if ( ! is_null($this->layout)) {
            $this->layout = view($this->layout, array('title' => $this->title));
        }
    }

    /**
     * Execute an action on the controller.
     *
     * @param  string  $method
     * @param  array   $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callAction($method, $parameters)
    {
        $this->setupLayout();

        $response = call_user_func_array(array($this, $method), $parameters);
        
        // If no response is returned from the controller action and a layout is being
        // used we will assume we want to just return the layout view as any nested
        // views were probably bound on this view during this controller actions.
        if (is_null($response) && ! is_null($this->layout))
        {
            $response = $this->layout;
        }

        return $response;
    }
    
    /**
     * @param $id
     * @param array $gallery
     * @param string $folder / config
     * @param int $chmod
     * @return array
     */
    public function saveFile($id, $gallery = [], $folder = '', $chmod = 0755)
    {
        $return = [];
        $folder = $folder ? $folder : config('app.controller');
        $dirPath = str_finish(config('upload.' . $folder), '/');
        $cachePath = str_replace('/777/', '/pic/', $dirPath);

        if (!$dirPath or $dirPath == '/')  {
            die(trans('language.upload directory is not set in config file'));
        }

        $isGallery = false;
        foreach ($gallery as $key => $value) {
            if (!is_object($value)) {
                $isGallery = true;
            }
        }

        $fileList = (($isGallery or !$gallery) ? Input::file() : $gallery);

        $iniFileMaxSize = ini_get('upload_max_filesize');
        $fileMaxMeassureUnit = substr($iniFileMaxSize, -1, 1);
        $fileMaxSize = intval(substr($iniFileMaxSize, 0, strlen($iniFileMaxSize) - 1));

        switch (strtoupper(trim($fileMaxMeassureUnit))) {
            case 'K':
                $fileMaxSize = $fileMaxSize * 1024;
                break;
            case 'M':
                $fileMaxSize = $fileMaxSize * 1024 * 1024;
                break;
            case 'G':
                $fileMaxSize = $fileMaxSize * 1024 * 1024 * 1024;
                break;
            case 'T':
                $fileMaxSize = $fileMaxSize * 1024 * 1024 * 1024 * 1024;
                break;
        }
      
        if ($fileList) {
            foreach($fileList as $formName => $file) {
                if (is_array($file)) {
                    foreach ($file as $fName => $f) {
                        $fileList[$formName ."[{$fName}]"] = $f;
                    }
                }
            }
            foreach($fileList as $formName => $file) {
                if (!is_object($file)) continue;
                if ($file->isValid()) {
                    $return[$formName]['name'] = $file->getClientOriginalName();
                    $return[$formName]['ext']  = $file->getClientOriginalExtension();
                    $return[$formName]['size'] = round($file->getSize() / 1024);
                    $return[$formName]['mimetype'] = $file->getMimeType();
                    if ($file->getSize() > $fileMaxSize) {
                        die(trans('language.the uploaded file exceeds the upload_max_filesize directive in php.ini'));
                    }
                    preg_match('/(^\/)|(\.\.)|(\[\d*\])/', $formName, $match);
                    if ($match) continue;
                    preg_match('/(\w+)\/*(.*)/', $formName, $file_match);
                    preg_match('/([A-Za-z0-9_\-]*)\[(\w*\d+)\]/', $formName, $gallery_match);

                    if ($formName == $file_match[0] and !$gallery_match) {
                        if(!$dirPath) {
                            die(trans('language.upload directory is not set in config file'));
                        }
                        $destnation = $dirPath . (@$file_match[2] ? $file_match[2] . '/' : '');

                        if (!is_dir($destnation)) {
                            File::makeDirectory($destnation, $chmod, true);
                        }
                        
                        //## remove cache directory
                        File::deleteDirectory($cachePath . (@$file_match[2] ? $file_match[2] . '/' : '') . $id, true, true);
                        $file->move($destnation, $id);
                        $return[$formName]['id'] = $id;
                        $return[$formName]['galleryfile'] = false;
                    } elseif ($gallery_match) {
                        preg_match('/([A-Za-z0-9_\-]*)\[(\w*\d+)\]/i', $formName, $pic);
                        $imgId = @$gallery[$pic[2]];
                        if (!$imgId) continue;
                        $destnation = $dirPath . @$pic[1] . '/';
                        if (!is_dir($destnation)) {
                            File::makeDirectory($destnation, $chmod, true);
                        }

                        //## remove cache directory
                        File::deleteDirectory($cachePath . (@$pic[1] ?  @$pic[1] . '/' : '') . $imgId, true, true);
                        $file->move($destnation, $imgId);
                        $return[$formName]['id'] = $imgId;
                        $return[$formName]['galleryfile'] = true;
                    }
                }
            }
        }
        return $return;
    }

}
