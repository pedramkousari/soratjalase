<?php namespace App\Http\Controllers\Programmer;

use App\Http\Controllers\BackendController;
use Config;
use Request;
use Response;

class HomeController extends BackendController {
    public $title = 'language.home';
	
	public function index()
	{
		$this->layout->custom = true;
        $this->layout->content = view('admin.home');
	}

}