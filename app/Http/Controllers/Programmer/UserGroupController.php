<?php namespace App\Http\Controllers\Programmer;

use App\Http\Controllers\BackendController;
use App\Models\UserGroup;
use Request;
use Input;

class UserGroupController extends BackendController {
    public $title = "language.users group management";

    public function index()
    {
        $this->field[] = ['php' => 'echo trans("language." . $item["xgroup"]);', 'name' => trans('language.group'), 'css' => 'width: 10%', 'sortable' => 'xgroup'];

        $this->result = UserGroup::getResult();
        $this->paginate();

        $this->layout->customToolbarTitle = trans('language.list of users group');
        $this->layout->content->listBtn['hideDelete'] = true;
    }

    public function create($id = 0)
    {
        $this->layout->title = 'language.usergroup';
    }

    public function edit($id)
    {
        $this->layout->content->list = UserGroup::first($id);
        $this->layout->title = 'language.usergroup';

    }

    public function store($id)
    {
        $frm = Input::get('frm');
        $group = Input::get('group', []);
        
        if ($group) {
            foreach ($group as $grp) {
                UserGroup::store(['xgroup' => trim($grp)]);
            }
        } else {
            UserGroup::store($frm, $id);
        }

        die('[[OK]]');
    }

    public function delete($id)
    {
        UserGroup::remove($id);
        die('[[OK]]');
    }

    public function setCombo()
    {
        $userGrops = UserGroup::getResult()->lists('xgroup');
        $newGroups = [];
        if (config('app.prefixes')) {
            foreach (config('app.prefixes') as $key => $group) {
                if (in_array($group, @$userGrops)) continue;
                $newGroups[] = $group;
            }
        }

        $this->layout->toolbar['hideCreate'] = ($newGroups ? false : true);
        $this->layout->content->newGroups = $newGroups;
    }
}