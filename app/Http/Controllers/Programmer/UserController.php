<?php namespace App\Http\Controllers\Programmer;

use App\Http\Controllers\BackendController;
use App\Models\UserGroup;
use App\Models\UserInfo;
use App\Models\User;
use FarsiLib;
use stdClass;
use Request;
use Session;
use Config;
use Input;
use Crypt;
use File;
use Hash;

class UserController extends BackendController {
    public $title = "language.users";


    //## list
	public function index()
	{
        $this->field[] =  ['php' => 'echo \'<div class="text-center"><img src="/pic/user/\' . $item["xuserid"] . "w30h40c1/". rand(1000, 100000) . ".png?nocache=1" . \'" alt="" /></div>\';',
           'name' => trans('language.picture'), 'css' => 'width: 3%'];
		$this->field[] =  ['php' => 'echo transExplode($item["xgroup"]);', 'name' => trans('language.group'), 'css' => 'width: 10%', 'sortable' => 'xgroup'];
		$this->field[] =  ['php' => 'echo \'<div class="ltr">\' . $item["xusername"] . "</div>";', 'name' => trans('language.username'), 'css' => 'width: 10%', 'sortable' => 'xusername'];
        $this->field[] =  ['php' => 'echo $item["xname"];', 'name' => trans('language.name'), 'css' => 'width: 10%', 'sortable' => 'xname'];
        $this->field[] =  ['php' => 'echo $item["xfamily"];', 'name' => trans('language.family'), 'css' => 'width: 10%', 'sortable' => 'xfamily'];
        $this->field[] =  ['php' => 'echo \'<div class="ltr">\' . $item["email"] . "</div>";', 'name' => trans('language.email'), 'css' => 'width: 10%', 'sortable' => 'email'];
        $this->field[] =  ['php' => 'echo \'<div class="ltr">\' . convertDigit($item["xmobile"]) . "</div>";', 'name' => trans('language.mobile'), 'css' => 'width: 10%', 'sortable' => 'xmobile'];
        $this->field[] =  ['php' => 'echo \'<div class="text-center ltr">\' . ($item["xcreation_date"] > 0 ? FarsiLib::g2jDate($item["xcreation_date"], true) : trans("language.unknown")) .
            "</div>";', 'name' => trans('language.register date'), 'css' => 'width: 10%', 'sortable' => 'xcreation_date'];

        $this->field[] =  ['php' => 'echo status($item["xuser_status"])', 'name' => trans('language.status'), 'css' => 'width: 2%', 'sortable' => 'xuser_status'];

        $this->result = User::getResult()
            ->groupBy('u.xuserid');
            
        if (strtolower(config('app.section')) != 'programmer') {
            $this->result->having('xgroup', 'not like', '%Programmer%');
        }   

        $this->paginate();
	}

    //## create
    public function create($id = 0)
    {
        $this->layout->title = 'language.new user';

    }

    //## edit
    public function edit($id)
    {   
        $this->layout->title = 'language.user';
        $userDetail = User::first($id);

        if ($userDetail) {
            $userDetail['xcreation_date'] = $userDetail['xcreation_date'] > 0 ? FarsiLib::g2jDate($userDetail['xcreation_date'], true) : '';
            $userDetail['xbirthday'] = $userDetail['xbirthday'] > 0 ? FarsiLib::g2jDate($userDetail['xbirthday']) : '';

            $userGroupList = User::group($id);
            if ($userGroupList) {
                foreach ($userGroupList as $groupid => $group) {
                    $userDetail['groupList'][$group] = $groupid;
                }
            }
        }

        $this->layout->content->imageExist = File::exists($this->uploadPath . $id);
        $this->layout->content->signExist = File::exists($this->uploadPath . "/signature/$id");

        $this->layout->content->list = $userDetail;

    }

    //## store
    public function store($id)
    {  
        $frm = Input::get('frm');
        $group = Input::get('group');   
        $agentId = intval(Input::get('agentId'));  
            
        if ($frm['xxuser_name']['xusername'] == '' or $frm['xxuser_name']['xpassword'] == '' and !$id) {
            die(trans('language.username and password cannot be empty'));
        } 
        
        $frm['xxuser_info']['xbirthday'] = $frm['xxuser_info']['xbirthday'] > 0 ? FarsiLib::j2gDate($frm['xxuser_info']['xbirthday']) : '';

        $id = User::store($frm, $group, $id);
        
        $this->saveFile($id, [], 'User'); 
        die('[[OK]]');
    }

    //## delete
    public function delete($id)
    {   User::remove($id);

        File::delete($this->uploadPath . "signature/$id");
        File::delete($this->uploadPath . $id);
        die('[[OK]]');
    }

    //## delete image
    public function deleteImg($id)
    {
        $type = Request::get('type');
        $path = $type == 'signature' ? "signature/$id" : $id;
        File::delete($this->uploadPath . $path);
        
        die('[[OK]]');
    }

    //############### set combo
    public function setCombo()
    {   $groupList = UserGroup::getResult()->get();
        if ($groupList) {
            foreach ($groupList as $key => $value) {
                if (strtolower($value['xgroup']) == 'programmer' && strtolower(config('app.section')) != 'programmer')
                    unset($groupList[$key]);
            }
        }
        $this->layout->groupList = $this->layout->content->groupList = $groupList;
        
        //## user status list
        $this->layout->statusList = $this->layout->content->statusList = User::fetchEnum('xuser_status');
        
        //## user gender list
        $this->layout->genderList = $this->layout->content->genderList = UserInfo::fetchEnum('xgender');

        $this->layout->content->nowDateTime = date('Y-m-d H:i');
        $this->layout->content->now = date('Y-m-d');
    }

    //## profile
    public function profile()
    {  
        if (Request::ajax()) {
            $this->layout = view('public.profile.overview');
            $this->layout->content = view('backend.ajax');           
        } else {     
            $this->layout->custom = true;       
            $this->layout->content = view('public.profile'); 
        }

        $list = User::first($this->userId);
        if ($list) {           
            $list['xcreation_date'] = ($list['xcreation_date'] > 0 ? FarsiLib::g2jDate($list['xcreation_date'], true) : '');
            $list['xbirthday'] = ($list['xbirthday'] > 0 ? FarsiLib::g2jDate($list['xbirthday']) : ''); 
        }

        $this->layout->lang = $this->layout->content->lang = $this->lang;
        $this->layout->content->list = $list;
        $this->layout->content->nowDateTime = date('Y-m-d H:i');
        $this->layout->content->imageExist = File::exists($this->uploadPath . $this->userId);
      
        $this->layout->content->signExist = File::exists($this->uploadPath . "/signature/" . $this->userId);
        
        //## user gender list
        $this->layout->genderList = $this->layout->content->genderList = User::fetchEnum('xgender', 'xxuser_info');
    }

    //## update 
    public function updateInfo($id)
    {
        $id = (int) $id ?: $this->userId;
        $frm = Input::get('frm');

        if (!$id) return abort(403);

        if (@$frm['xxuser_info']['xbirthday']) {
            $frm['xxuser_info']['xbirthday'] = FarsiLib::j2gDate(@$frm['xxuser_info']['xbirthday']);
        }

        if ($frm['xxuser_info'] and $id) {
            UserInfo::store($frm['xxuser_info'], $id);
        }

        if (Input::file()) {
            $this->saveFile($id, [], 'User');
        }

        die(json_encode([
            'type' => 'success' , 
            'message' => trans('language.information successfully updated')
        ]));
    }

    //## load images 
    public function loadImage()
    {
        $this->layout = view('public.profile.accountImage');

        $list = User::first($this->userId);
        $this->layout->list = $list;
        $this->layout->imageExist = File::exists($this->uploadPath . $this->userId);
        $this->layout->signExist = File::exists($this->uploadPath . "/signature/" . $this->userId);      
    }

    //## change password
    public function changePassword($token)
    {
        $currentPassword = Input::get('password');
        $newPassword = Input::get('newpassword');
        $retypePassword = Input::get('retypepassword');

        $userRow = User::find($this->userId);
        if (Session::get('_token') == Input::get('_token')) {
            if (!Hash::check($currentPassword, $userRow->xpassword)) {
                die(json_encode(['type' => 'error', 'message' => trans('language.current password is invalid')])); 
            } else if ($newPassword != $retypePassword) {
                die(json_encode(['type' => 'error', 'message' => trans('language.password and confirmation do not match')]));
            } else {              
                $userRow->xpayload = Crypt::encrypt($newPassword);      
                $userRow->xpassword = Hash::make($newPassword);
                $userRow->save();
                die(json_encode(['type' => 'success', 'message' => trans('language.password successfully changed')]));
            }
        } else {
            die(json_encode(['type' => 'error', 'message' => trans('language.token is invalid')]));
        }
    }
}